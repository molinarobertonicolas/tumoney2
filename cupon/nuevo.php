<?
include("../sesion.php");
include("cupon.php");
include("../cliente/cliente.php");
include("../tarjeta/tarjeta.php");
include '../menu.php';

//$objecto = new Tarjeta();
$listaTarjeta = tarjeta::lista();

//$cliente = new Cliente();
//$clientes=$cliente->lista();

$listaCliente = cliente::lista();

$objecto = new Cupon();
if( isset($_POST['selectTarjeta']) && !empty($_POST['selectTarjeta']) )
 {

  $tarjeta_id = $_POST['selectTarjeta'];
  $monto= $_POST['monto'];
  $cuotas = $_POST['cuotas'];
  $fechahora=  date("Y-m-d H:i:s");
  $cliente_id= $_POST['cliente_id'];
  $numero_cupon= $_POST['numero_cupon'];
  $digitos= $_POST['digitos'];

  $todobien = $objecto->nuevo($tarjeta_id,$monto,$cuotas,$fechahora,$cliente_id,$numero_cupon,$digitos);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }     
}
else
{
?>
 <div class="container">
 <h3>Cupon de Tarjeta</h3>
 <script src="../js/jquery-1.10.2.js"></script>
 <script src="../js/util.js"></script>
 <hr>
 <div class="row">

 <div class="col-md-8">
     
 <h4>Agregar Cupon</h4>
 <hr>
 
 

  <div class="col-md-8">
    <label>DNI</label>
    <input id="dni" name="dni" class="form-control" type="text" tabindex="1" required />
   
    <button id="buscarDNI" class="btn btn-primary pull-right"> Buscar</button> <br>
  </div>
 <form method="POST" role="form" action="nuevo.php">

   <input type="hidden" name="cliente_id" id="cliente_id">

   <div class="col-md-8">
    <label>Nombre de Cliente*</label>
    <input id="nombre" name="nombre" class="form-control" type="text" tabindex="1" required  />
  </div>

 <div class="col-md-8">
   <label >Tarjeta de Credito *</label>
   
    <select name="selectTarjeta" class="form-control" required>
      <option>Seleccionar ....</option>
        <?
        foreach($listaTarjeta as $item)
        {
        //while ( $item = mysql_fetch_array($tarjetas)){
        echo "<option value='".$item[id]."'> ". $item[nombre]."</option>";
        }
        ?>
       </select>
    </div>

  <div class="col-md-8">
    <label>Nº Cupon *</label>
    <input name="numero_cupon"  class="form-control" type="text" tabindex="1"   required />
  </div>

  <div class="col-md-8">
    <label>4 ultimos digitos de la Tarjeta</label>
    <input name="digitos"  class="form-control" type="text" tabindex="2" maxlength="4"  required />
  </div>

  <div class="col-md-8">
    <label>Monto del Cupon *</label>
    <input name="monto"  class="form-control" type="number" tabindex="3" maxlength="15" step="any" onkeypress="return soloNumeros(event);" required />
  </div>

  <div class="col-md-8">
    <label>Cantidad de Cuotas *</label>
    <input name="cuotas"  class="form-control" type="number" tabindex="4" maxlength="3" onkeypress="return soloNumerosSd(event);" required />
  </div>
  
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>    


 <script src="../js/jquery-1.10.2.js"></script> 
 <script src="../js/jquery.maskedinput.js" type="text/javascript"></script>
 <script type="text/javascript">
 
 $(document).ready(function()
  {
   
    $("#dni").mask("99999999");
    // 16012017 buscar por dni
    $("#buscarDNI").click(function()
     {
        
        var vdni = $("#dni").val();   
        if(vdni != '')
        {
         $.ajax({
                  data: {"dni" : vdni},
                  type: "POST",
                  dataType: "json",
                  url: "traerDatosCliente.php",
                })
               .done(function( data, textStatus, jqXHR ) {
                   if ( console && console.log ) {
                       console.log( "La solicitud se ha completado correctamente." );
                   }
                   $.each(data, function(i,cliente){
                    document.getElementById("cliente_id").value=cliente.id;
                         //alert(cliente.nombre);
                        document.getElementById("nombre").value=cliente.nombre;
                    });
               })
               .fail(function( jqXHR, textStatus, errorThrown ) {
                   if ( console && console.log ) {
                       console.log( "La solicitud a fallado: " +  textStatus);
                       alert('Dni no existe.')
                   }
              });
           
        }
        else alert('Falta el dni');     
       
      });//fin buscarDNI

     
  }) //fin jquery   
          
</script>

 <?
 }
 ?>             
   