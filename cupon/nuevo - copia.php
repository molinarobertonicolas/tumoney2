<?
include("../sesion.php");
include("cupon.php");
include("../cliente/cliente.php");
include("../tarjeta/tarjeta.php");
include '../menu.php';

//$objecto = new Tarjeta();
$listaTarjeta = tarjeta::lista();

//$cliente = new Cliente();
//$clientes=$cliente->lista();

$listaCliente = cliente::lista();

$objecto = new Cupon();
if( isset($_POST['selectTarjeta']) && !empty($_POST['selectTarjeta']) )
 {

  $tarjeta_id = $_POST['selectTarjeta'];
  $monto= $_POST['monto'];
  $cuotas = $_POST['cuotas'];
  $fechahora= $_POST['fechahora'];
  $cliente_id= $_POST['selectCliente'];
  $numero_cupon= $_POST['numero_cupon'];
  

  $todobien = $objecto->nuevo($tarjeta_id,$monto,$cuotas,$fechahora,$cliente_id,$numero_cupon);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }     
}
else
{
?>
 <div class="container">
 <h3>Cupon de Tarjeta</h3>
 <script src="../js/jquery-1.10.2.js"></script>
 <script src="../js/util.js"></script>
 <hr>
 <div class="row">


 <div class="col-md-8">
     
 <h4>Agregar Cupon</h4>
 <hr>
  <form method="POST" role="form" action="nuevo.php">
  
 <div class="col-md-8">
   <label >Tarjeta *</label>
   
    <select name="selectTarjeta" class="form-control" required>
        <?
        foreach($listaTarjeta as $item)
        {
        //while ( $item = mysql_fetch_array($tarjetas)){
        echo "<option value='".$item[id]."'> ". $item[nombre]."</option>";
        }
        ?>
       </select>
    </div>

  <div class="col-md-8">
    <label>Nº Cupon *</label>
    <input name="numero_cupon"  class="form-control" type="text" tabindex="1"   required />
  </div>

  <div class="col-md-8">
    <label>Monto *</label>
    <input name="monto"  class="form-control" type="text" tabindex="2" maxlength="15" onkeypress="return soloNumeros(event);" required />
  </div>

  <div class="col-md-8">
    <label>Cuotas *</label>
    <input name="cuotas"  class="form-control" type="text" tabindex="3" maxlength="3" onkeypress="return soloNumerosSd(event);" required />
  </div>

   <div class="col-md-8">
    <label >Fecha</label>
    <input type="date" name="fechahora" id="fechahora" tabindex="4" class="form-control" required/>
    </div>
  

  <div class="col-md-8">
   <label >Cliente</label>
   <select name="selectCliente" class="form-control">
        <?
        foreach($listaCliente as $item)
        {
        //while ( $item = mysql_fetch_array($tarjetas)){
        echo "<option value='".$item[id]."'> ". $item[nombre]."</option>";
        }
        ?>
       </select>
  </div>
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>     
 <?
 }
 ?>             
   