<?php
include("../sesion.php");
include("cupon.php");
include '../menu.php';

if(isset($_GET['id']) && !empty($_GET['id']))
{
	$id = (int)$_GET['id'];
  $clientes = cupon::obtenerId($id);
  foreach($clientes as $cli)
	{
   ?>
   <h3>Cancelar Cupon</h3>
   <hr>
   <div class="container">
   <div class="row">
   <div class="col-md-12">
   <hr>
   <form class="form-horizontal" role="form" method="POST" action="eliminar.php">
    <input type="hidden" name="idcupon" value="<?echo $id; ?>" />
    
     <div class="col-md-8">
      <label>Cupon </label>
      <input name="cliente"  class="form-control" type="text" tabindex="1"  value="<?echo utf8_encode($cli['numero_cupon']); ?>" required />
    </div>

    <div class="col-md-8">
    <hr>
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Aceptar</button>
    </div>
  </form>
   <?
  }//fin del while
 }

if( isset($_POST['idcupon']) && !empty($_POST['idcupon']) )
 {
	$id = $_POST['idcupon'];
	//$objecto = new Cliente();
	//$todobien = $objecto->borrar($id);
	$todobien=cupon::cancelarCupon($id);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      //header('Location: listado.php');
      exit;
    }
    else {
    ?>
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo eliminar debido a que tiene un Prestamo asociado ...
         </div>
    <?
    }
  }
?>