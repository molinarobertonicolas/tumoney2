<?
include_once("../bd/conexion.php");
class Cupon
{
  public function obtenerCupones($cupon)
  {
   $consulta="SELECT * FROM cupondetarjeta where numero_cupon='$cupon'";
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
      return $data;
    }else return $rs;
  }
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT
    cupondetarjeta.`id` AS id,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`monto` AS monto,
    cupondetarjeta.`cuotas` AS cuotas,
    cupondetarjeta.`fechahora` AS fechahora,
    cupondetarjeta.`estado` AS estado,
    cliente.`nombre` AS nombre,
    cupondetarjeta.`numero_cupon` AS numero_cupon
    FROM cupondetarjeta,cliente,tarjeta
    where cupondetarjeta.tarjeta_id=tarjeta.id and cupondetarjeta.cliente_id=cliente.id
          order by cupondetarjeta.fechahora desc";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }



   public function nuevo($tarjeta_id,$monto,$cuotas,$fechahora,$cliente_id,$numero_cupon,$digitos)
   	{
  	  $sql="INSERT INTO `cupondetarjeta`
              (`tarjeta_id`,
               `monto`,
               `cuotas`,
               `fechahora`,
               `cliente_id`,
               `numero_cupon`,`digitos`,
               `estado`)
      VALUES ('$tarjeta_id','$monto','$cuotas','$fechahora','$cliente_id','$numero_cupon','$digitos', 'nuevo');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
    	}


	public function editar($id,$dni,$nombre,$domicilio,$departamento,$telefono,$fecha_ingreso)
	{
   	 
      $sql="UPDATE `cliente`
            SET `id` = '$id',
              `dni` = '$dni',
              `nombre` = '$nombre',
              `domicilio` = '$domicilio',
              `departamento` = '$departamento',
              `telefono` = '$telefono',
              `fecha_ingreso` = '$fecha_ingreso'
            WHERE `id` = '$id';";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM cupondetarjeta where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }

  public function eliminar($id)
	{
     //para validar que el cupon no este asociado a un prestamo
     $sql="SELECT * FROM prestamo_tarjeta where cupon_id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      //$cantidad=mysqli_num_rows($rs);
      //exit;
      if ( (mysqli_num_rows($rs)) > 0)
      {
      //tiene un prestamo asociado no se puede eliminar
      return false;
      exit();
      }
       else
           { 
                  //si se puede eliminar porque no tiene prestamos asociados
               $sql="DELETE FROM cupondetarjeta WHERE id ='$id'";
               $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
               return $rs;
           }
    }

     public function cancelarCupon($id)
     {
     //para validar que el cupon no este asociado a un prestamo
     $sql="UPDATE `cupondetarjeta`
            SET `estado` = 'Cancelado'
            WHERE `id` = '$id'";

      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);

      $sql="UPDATE `prestamo_tarjeta`
            SET 
               `estado` = 'Cancelado'
                        WHERE `cupon_id` = '$id'";

      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
    }

}
?>