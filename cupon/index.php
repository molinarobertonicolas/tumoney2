<?php
include("../sesion.php");
include("../menu.php");
include("cupon.php");
?>
 <div class="container">
    <h2>Listado de Cupones</h2>

    <p> <!--a class="btn btn-primary" href="nuevo.php">Adicionar Nuevo</a> </p-->

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>N°</th>
             <th>N-Cupon</th>
             <th>Tarjeta</th>
             <th>Monto</th>
             <th>Cuotas</th>
             <th>FechaHora</th>
             <th>Cliente</th>
             <th>Funciones</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objecto = new Cupon();
          $cupones = cupon::lista();
          foreach($cupones as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['numero_cupon']; ?></td>
              <td><?php echo $item ['tarjeta']; ?></td>
              <td><?php echo $item ['monto']; ?></td>
              <td><?php echo $item ['cuotas']; ?></td>
              <td><?php echo $item ['fechahora']; ?></td>
              <td><?php echo $item ['nombre']; ?></td>
              <td>
                  <? if ($item ['estado']=='Cancelado') {
                   ?>      
                      Cancelado
               <!--a class="btn btn-primary btn-sm" href="../prestamotarjeta/nuevo.php?cuponId=<?php echo $item ['id'];?>" > Generar Prestamo</a-->
                 <? }
                    else{
                         ?>
                          <a class="btn btn-danger btn-sm" href="eliminar.php?id=<?php echo $item ['id'];?>" > Cancelar</a>
                  <?
                    }
                 ?>
              </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>