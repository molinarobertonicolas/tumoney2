<?php
include("../sesion.php");
include("../menu.php");
include("pagos.php");
?>
 <div class="container">
    <h2 class="font-weight-bold">Pagos - Prestamos </h2>
    <p><a class="btn btn-primary" href="nuevo.php">Nuevos Pagos </a></p>

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Fecha Pago </th>
             <th>Monto </th>
             <th>Descripción</th>
             <th></th>
             </tr>
           <thead>
           <tbody>
          <?php
          $doc = new pagos();
          $listados = $doc->listapagos();
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['fecha_pago']; ?></td>
              <td><?php echo $item ['monto']; ?></td>
              <td><?php echo $item ['descripcion']; ?></td>
              <td><a href="imprimir.php?id=<? echo $item ['id']; ?>" target="_blank">Imprimir Detalle</a> </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  
 <script src="../js/jquery-1.10.2.js"></script>
 <script src="../js/bootstrap.min.js" type="text/javascript"></script>
 <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data)
          {
            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin
 });
</script>
</body>
</html>