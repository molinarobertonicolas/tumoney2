<?php
include("../sesion.php");
include("../menu.php");
include("pagos.php");
$objecto = new Pagos();
?>
<style type="text/css" media="print">
@media print {
body { font-size:12px; }
#noimprimir {display:none;}
#parte2 {display:none;}
}
</style>

<div class="container">
  <h2 class="font-weight-bold">Pagos - Prestamos </h2>
  <hr>
  <br>
  <div class="row" id='noimprimir' >
  <div class="col-md-6">
  <h3>Pendientes de Pago</h3>
    <ul class="list-group">
     <?php  
       $doc = new pagos();
       $listados = $doc->listaPendientes();
       foreach($listados as $item)
       {
        ?>
           <li class='list-group-item' id="li<?php echo $item['id'];?>"
             value="<?php echo $item['id'];?>"
             data-pres="<?echo $item['id'];?>"
             data-cliente="<?php echo $item['cliente'];?>"
             data-monto="<?echo $item['monto_prestado'];?>"
             data-fecha="<?php echo date("d/m/Y", strtotime($item['fecha']));?>"
             data-cupon="<?echo $item['numero_cupon'];?>"
             >
             <span>
              <?php echo date("d/m/Y", strtotime($item['fecha']));?>
                             N° <?php echo $item['id'];?>
                             - <?php echo $item['cliente'];?>
                             - $ <?php echo $item['monto_prestado'];?>
             </span>
            <button type='button' onclick="agregar(<?php echo $item['id'];?>);" class='btn btn-primary'>Agregar</button>
           </li>
            <?
          } 
          ?>
        </ul>
  </div>

  <div class="col-md-6">
    <h3>Listado a Rendir:</h3>
    <div class="row" id="listado1" style="display:none;"> 
      <div class="col-md-12">
        <ul id="tabla1" class="list-group">
        </ul>
        <p>
          <label> Sumatoria : $</label>
          <label id="sumatoria1"></label>
        </p> 
        <p> 
          <label>Fecha de Pago: </label>

        <label id="fecha_pago"><?php echo $fechaActual = date( 'd/m/Y ' );?></label>
        </p>
        <p>
        <label>Descripción:</label>
        <input class="form-control" type="text" required name="descripcion" id="descripcion" placeholder="Ej: por transferencia.">
        </p>
        <div class="col-md-12" id='noimprimir' >
              <!--a href="javascript:window.print()" class="btn btn-default" onclick="actualiazarEstado();"> Imprimir</a-->
            <button id="guardar" class="btn btn-default">Guardar Pago</button>
            <button id="cancelar" class="btn btn-default">Cancelar</button>
        </div>
      </div>
    </div>
  </div>

  </div>
  </div>
  
 <script src="../js/jquery-1.10.2.js"></script>
 <script src="../js/bootstrap.min.js" type="text/javascript"></script>
 <script type="text/javascript">

  var listaPrestamos = [];

  var sumatoria=0;

  function imprSelec(nombre)
  {
    var ficha = document.getElementById(nombre);
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write( ficha.innerHTML );
    ventimp.document.close();
    ventimp.print( );
    ventimp.close();
    actualizar();
  }

  function actualiazarEstado()
  {
    $.ajax({
           type: "POST",
           url: "cambiar_estado.php",
           data: {lista: JSON.stringify(listaPrestamos)},
           success: function(datos){
                console.log(datos);
            },
           failure: function(errMsg) {
                alert("Error:"+errMsg);
           }
        }); 
   }

   function agregar(id_prestamo)
   {
    var v_nombre=$('#li'+id_prestamo+'').data('cliente'); 
    var v_monto=$('#li'+id_prestamo+'').data('monto');
    var v_cupon=$('#li'+id_prestamo+'').data('cupon'); 
    var v_fecha=$('#li'+id_prestamo+'').data('fecha');

    listaPrestamos.push(id_prestamo);

    fila="<li id="+ id_prestamo +" data-importe="+v_monto+" class='list-group-item' > N° Pres. "+id_prestamo+" Fecha: "+v_fecha+" Cliente: "+v_nombre+" Monto: "+v_monto+" Cupon: "+v_cupon+"</li>";

    $('#listado1').show();
    $('#tabla1').append(fila);
    sumatoria=parseFloat(sumatoria)+parseFloat(v_monto); 
    $('#sumatoria1').text(Number.parseFloat(sumatoria).toFixed(2));
   }

 $(document).ready(function()
  {
    var sumatoria=0;

    $('#cancelar').click(function()
    {
      location.reload();
    });// fin cancelar

    $('#guardar').click(function()
    {
      var fecha_pago=$('#fecha_pago').text();
      var descripcion=$('#descripcion').val();
      var monto=$('#sumatoria1').text();
      $.ajax({
              type: "POST",
              cache: false,
              async: false,
              url: 'guardar_pago.php',
              data: { fecha_pago: fecha_pago, descripcion: descripcion, monto: monto,lista: JSON.stringify(listaPrestamos)},
              success: function(data)
              {
                if (data)
                  {
                    alert(data);
                    console.log(data);
                    location.reload(true);
                  }
              }
              })//fin ajax
      
      location.reload();
    });// fin cancelar
   
  
  })

</script>

