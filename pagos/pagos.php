<?php
include_once("../bd/conexion.php");
class Pagos
{

  public function listapagos()
  {
    $data=[];
    $consulta="SELECT * FROM `pago_prestamos_motos` order by fecha_pago desc";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT
    prestamo_tarjeta.`estado` AS estado, 
    prestamo_tarjeta.`id` AS id, 
    prestamo_tarjeta.`fechahora` AS fecha,
    cliente.`nombre` AS cliente,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`id` AS id_cupon,
    cupondetarjeta.`numero_cupon` AS numero_cupon,
    cupondetarjeta.`cuotas` AS cuotas,
    cupondetarjeta.`cuotas` AS cuotas,
    prestamo_tarjeta.`montoprestamo` AS monto_prestado,
    prestamo_tarjeta.`monto_apagar` AS apagar,
    prestamo_tarjeta.`pdf_dni` AS documento, 
     prestamo_tarjeta.`documentacion` AS documentacion,
     prestamo_tarjeta.`rendido` AS rendido,
    cupondetarjeta.`numero_cupon` AS numero_cupon 
    FROM
    `prestamo_tarjeta`
    INNER JOIN `cupondetarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
      INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
        left JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`) order by prestamo_tarjeta.`id` desc";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function listaPendientes()
  {
    $data=[];
    $consulta="SELECT
    prestamo_tarjeta.`estado` AS estado, 
    prestamo_tarjeta.`id` AS id, 
    prestamo_tarjeta.`fecha_otorgado` AS fecha,
    cliente.`nombre` AS cliente,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`id` AS id_cupon,
    cupondetarjeta.`numero_cupon` AS numero_cupon,
    cupondetarjeta.`cuotas` AS cuotas,
    cupondetarjeta.`cuotas` AS cuotas,
    prestamo_tarjeta.`montoprestamo` AS monto_prestado,
    prestamo_tarjeta.`monto_apagar` AS apagar,
    prestamo_tarjeta.`pdf_dni` AS documento, 
     prestamo_tarjeta.`documentacion` AS documentacion,
     prestamo_tarjeta.`rendido` AS rendido,
    cupondetarjeta.`numero_cupon` AS numero_cupon
    FROM
    `prestamo_tarjeta`
    INNER JOIN `cupondetarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
      INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
        LEFT JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`) WHERE
         prestamo_tarjeta.`rendido`='NO' AND prestamo_tarjeta.`documentacion`='SI' ORDER BY prestamo_tarjeta.`id` DESC";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  // actualizar el estado de los prestamos ya rendidos 
  public function actualizar_estado_prestamos($lista)
  {
    foreach($lista as $item)
    {
    $id_prestamo=$item;
    $fecha= date("Y-m-d");  
    $sql="UPDATE `prestamo_tarjeta` SET  `rendido` = 'SI',`fecha_cancelado` =$fecha WHERE `id`=".$id_prestamo;
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    }
    return $rs;
  }

  public function cambiarEstadoPrestamo($id)
  {
   $sql="UPDATE `prestamo_tarjeta`
         SET  `rendido` = 'SI',
         `fecha_cancelado` =".date("Y-m-d")."
         WHERE `id`='$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs)>0)
    {
      return 'error actualizar rendicion prestamo'.$id;
    }
    return 1;
  }

  public function nuevo($monto,$fecha_pago,$descripcion)
  {
   $sql="INSERT INTO `pago_prestamos_motos`
            (`monto`,
             `fecha_pago`,
             `descripcion`)
              VALUES ('$monto',
                      '$fecha_pago',
                      '$descripcion');";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs=mysqli_insert_id(conexion::obtenerInstancia());
  }

  public function guardar_detalle($id_pago,$lista)
  {
    foreach($lista as $item)
    {
    $id_prestamo=$item;  
    $sql="INSERT INTO `detalle_pago_pestamos_motos`
            (`id_pago`,
             `id_prestamo`)
          VALUES ('$id_pago',
                  '$id_prestamo');";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    }
    return $rs;
  }

  public function lista_detalle_pagos($id)
  {
    $data=[];
    $consulta="SELECT
    pago_prestamos_motos.`id` AS numero_pago,
    pago_prestamos_motos.`fecha_pago` AS fecha_pago,
    pago_prestamos_motos.`monto` AS monto,
    pago_prestamos_motos.`descripcion` AS descripcion,
    prestamo_tarjeta.`id` AS numero_prestamo,
    prestamo_tarjeta.`montoprestamo` AS monto_prestamo,
    cliente.`dni` AS cliente_dni,
    cliente.`nombre` AS nombre
    FROM
        `pago_prestamos_motos`
        INNER JOIN `detalle_pago_pestamos_motos` 
            ON (`pago_prestamos_motos`.`id` = `detalle_pago_pestamos_motos`.`id_pago`)
        INNER JOIN `prestamo_tarjeta` 
            ON (`detalle_pago_pestamos_motos`.`id_prestamo` = `prestamo_tarjeta`.`id`)
        INNER JOIN `cupondetarjeta` 
            ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
        INNER JOIN `cliente` 
            ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`)
            WHERE pago_prestamos_motos.`id`=$id;";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function lista_pagos_id($id)
  {
    $data=[];
    $consulta="SELECT * FROM `pago_prestamos_motos` where id=".$id;
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }
  
}
?>