<?php
include("../sesion.php");
include("../menu.php");
include("pagos.php");
$objecto = new Pagos();
?>
<style type="text/css" media="print">
@media print {
body { font-size:12px; }
#noimprimir {display:none;}
#parte2 {display:none;}
}
</style>

<div class="container">
  <h2 class="font-weight-bold">Pagos - Prestamos </h2>
  <br>
  <div class="container">
  
   <div class="row" id='noimprimir' >
    <div class="col-md-12">
      <select name="idpres" id="idpres" class="form-control" required>
      <option value="" >Seleccionar Prestamos...</option>
      <?php  
       $doc = new pagos();
          $listados = $doc->listaPendientes();
          foreach($listados as $item)
          {
           ?>
            <option value="<?php echo $item['id'];?>"
             data-pres="<?echo $item['id'];?>"
             data-cliente="<?php echo $item['cliente'];?>"
             data-monto="<?echo $item['monto_prestado'];?>"
             data-fecha="<?php echo $item['fecha'];?>"
             data-cupon="<?echo $item['numero_cupon'];?>"
             >
               <?php echo $item['fecha'];?>
               N° Prestamo <?php echo $item['id'];?>
               Cliente <?php echo $item['cliente'];?>
               Monto <?php echo $item['monto_prestado'];?>
               
            </option>
            <?
          } 
          ?>
    </select> 
    <hr>
  </div>
  </div>  


  <div class="row" id='noimprimir' >
  <div class="col-md-8">
  <h3>Pendientes de Pago</h3>
    <ul>


     <?php  
       $doc = new pagos();
          $listados = $doc->listaPendientes();
          foreach($listados as $item)
          {
           ?>

           <li class='nav-link' id="li<?php echo $item['id'];?>"
             value="<?php echo $item['id'];?>"
             data-pres="<?echo $item['id'];?>"
             data-cliente="<?php echo $item['cliente'];?>"
             data-monto="<?echo $item['monto_prestado'];?>"
             data-fecha="<?php echo $item['fecha'];?>"
             data-cupon="<?echo $item['numero_cupon'];?>"
             >
             <span>
              <?php echo $item['fecha'];?>
                             N° <?php echo $item['id'];?>
                             - <?php echo $item['cliente'];?>
                             - $ <?php echo $item['monto_prestado'];?>
             </span>
               
            <button type='button' onclick="agregar(<?php echo $item['id'];?>);" class='btn btn-primary'>Agregar</button>

              </li>
            <?
          } 
          ?>
        </ul>

  </div>

  <div class="row">

  <div class="col-md-4">
  <h3>Listado a Rendir:</h3>
  <div class="row" id="listado" style="display:none;"> 
    <div class="col-md-12">
        <ul id="tabla" class="list-group">
        </ul>
        <p>
        <label> Sumatoria :</label>
        <input type="text" id="sumatoria" class="form-control">
        </p>  
      
        <div class="col-md-12" id='noimprimir' >
            <a href="javascript:window.print()" class="btn btn-default" onclick="actualiazarEstado();"> Imprimir</a>
            <button id="cancelar" class="btn btn-default">Cancelar</button>
        </div>
    </div>
  </div>
  </div>


   <div class="col-md-4">
  <h3>Listado a Rendir prueba:</h3>
  <div class="row" id="listado1" style="display:none;"> 
    <div class="col-md-12">
        <ul id="tabla1" class="list-group">
        </ul>
        <p>
        <label> Sumatoria :</label>
        
        <label id="sumatoria1"></label>
        </p>  
      
        <div class="col-md-12" id='noimprimir' >
            <a href="javascript:window.print()" class="btn btn-default" onclick="actualiazarEstado();"> Imprimir</a>
            <button id="cancelar" class="btn btn-default">Cancelar</button>
        </div>
    </div>
  </div>
  </div>
  

  </div>
  </div>
  </div>
  </div>
  </div>
   
  
 <script src="../js/jquery-1.10.2.js"></script>
 <script src="../js/bootstrap.min.js" type="text/javascript"></script>
 <script type="text/javascript">

  var listaPrestamos = [];

  var sumatoria=0;

  function imprSelec(nombre) {
    var ficha = document.getElementById(nombre);
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write( ficha.innerHTML );
    ventimp.document.close();
    ventimp.print( );
    ventimp.close();
    actualizar();
  }

   function actualiazarEstado()
   {
    $.ajax({
           type: "POST",
           url: "cambiar_estado.php",
           data: {lista: JSON.stringify(listaPrestamos)},
           success: function(datos){
                console.log(datos);
            },
           failure: function(errMsg) {
                alert("Error:"+errMsg);
           }
        }); 
   }

   function agregar(id_prestamo)
   {
      var v_nombre=$('#li'+id_prestamo+'').data('cliente'); 
      var v_monto=$('#li'+id_prestamo+'').data('monto');
      var v_cupon=$('#li'+id_prestamo+'').data('cupon'); 
      var v_fecha=$('#li'+id_prestamo+'').data('fecha');


      listaPrestamos.push(id_prestamo);

      fila="<li id="+ id_prestamo +" data-importe="+v_monto+" class='list-group-item' > N° Pres. "+id_prestamo+" Fecha: "+v_fecha+" Cliente: "+v_nombre+" Monto: "+v_monto+" Cupon: "+v_cupon+"</li>";



      $('#listado1').show();
      $('#tabla1').append(fila);
      sumatoria=parseFloat(sumatoria)+parseFloat(v_monto); 
      $('#sumatoria1').text(sumatoria);
   }






 $(document).ready(function()
  {
    
    var sumatoria=0;

    $('#idpres').change(function()
    {        
      var id_prestamo = $("#idpres option:selected").val(); 
      var v_nombre=$('option:selected').data('cliente'); 
      var v_monto=$('option:selected').data('monto');
      var v_cupon=$('option:selected').data('cupon'); 
      var v_fecha=$('option:selected').data('fecha');

      listaPrestamos.push(id_prestamo);

      fila="<li id="+ id_prestamo +" data-importe="+v_monto+" class='list-group-item' > N° Pres. "+id_prestamo+" Fecha: "+v_fecha+" Cliente: "+v_nombre+" Monto: "+v_monto+" Cupon: "+v_cupon+"</li>";

      $('#listado').show();
      $('#tabla').append(fila);
      sumatoria=parseFloat(sumatoria)+parseFloat(v_monto); 
      $('#sumatoria').val(sumatoria);
    });//fin change



    $('#cancelar').click(function()
    {
      $('#tabla').empty();
      sumatoria=0;
      $('#listado').hide();
       $("option:selected").removeAttr("selected");
    });// fin cancelar

   


    $('#agregra').click(function()
    {
      $('#tabla').empty();
      sumatoria=0;
      $('#listado').hide();
       $("option:selected").removeAttr("selected");
    });// fin cancelar
  
  })

</script>

