<?
include_once("../bd/conexion.php");
class Cuota_Prestamo_Moto
{

//listar todo los prestamos con y sin documentacion pendiente
  public function prestamos_documentacion($doc_presenta)
  {
     $data=[];
    $consulta="SELECT
                `id`,
                `cupon_id`,
                `fechahora`,
                `montoprestamo`,
                `documentacion`
                 FROM `prestamo_tarjeta` where documentacion = '$doc_presenta'  order by fechahora desc ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

//listar todo los prestamos con pagos pendientes
  public function prestamos_pagos_pendientes()
  {
     $data=[];
    $consulta="SELECT
                `id`,
                `cupon_id`,
                `fechahora`,
                `montoprestamo`,
                `documentacion`
                 FROM `prestamo_tarjeta` where fecha_cancelado = '0000-00-00'  order by fechahora desc ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


//listar todo los prestamos cancelados
  public function prestamos_cancelados()
  {
    $data=[];
    $consulta="SELECT
                `id`,
                `cupon_id`,
                `fechahora`,
                `montoprestamo`,
                `documentacion`,
                `fecha_cancelado`
                 FROM `prestamo_tarjeta` where fecha_cancelado <> '0000-00-00'  order by fechahora desc ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  



}
