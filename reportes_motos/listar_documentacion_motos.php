<?php
include("../sesion.php");
include("cuota_prestamo_moto.php");
$doc_presenta='SI';
?>
 
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Nº Cupon </th>
             <th>Fecha</th>
             <th>Monto Prestamo</th>
             <th>Documentacion</th>
             </tr>
           </thead>
           <tbody>
          <?php
          $listados = cuota_prestamo_moto::prestamos_documentacion($doc_presenta);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['id']; ?></td>
              <td><?php echo $item['cupon_id']; ?></td>
              <td><?php echo $item['fechahora']; ?></td>
              <td><?php echo $item['montoprestamo']; ?></td>
              <td><?php echo $item['documentacion']; ?></td>
           </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
 <?
} else echo 'error';
 ?>