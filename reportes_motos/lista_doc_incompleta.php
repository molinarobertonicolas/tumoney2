<?php
include("../sesion.php");
include("../menu.php");
include("cuota_prestamo_moto.php");
$doc_presenta='NO';
?>
 <div class="container">
    <h2>Reportes</h2>
    <p> 
      <a class="btn btn-primary" href="index.php">Con Documentacion</a>
      <a class="btn btn-primary" href="lista_doc_incompleta.php">Documentacion Incompleta</a> 
      <a class="btn btn-primary" href="lista_pagos_pendientes.php">Prestamos Pagos Pendientes</a>
      <a class="btn btn-primary" href="lista_prestamos_cancelados.php">Prestamos Cancelados</a>
    </p>
    <h3>Prestamos con documentacion incompleta</h3>
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Nº Cupon </th>
             <th>Fecha</th>
             <th>Monto Prestamo</th>
             <th>Documentacion</th>
             </tr>
           </thead>
           <tbody>
          <?php
          $listados = cuota_prestamo_moto::prestamos_documentacion($doc_presenta);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['id']; ?></td>
              <td><?php echo $item['cupon_id']; ?></td>
              <td><?php echo $item['fechahora']; ?></td>
              <td><?php echo $item['montoprestamo']; ?></td>
              <td><?php echo $item['documentacion']; ?></td>
           </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>