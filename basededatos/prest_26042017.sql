/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - nuevo2017
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nuevo2017` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nuevo2017`;

/*Table structure for table `caja` */

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` double(8,2) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `operacion` varchar(7) DEFAULT NULL,
  `usuario_id` int(2) DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `caja` */

insert  into `caja`(`id`,`monto`,`fechahora`,`operacion`,`usuario_id`,`observacion`) values (1,50000.00,'2017-01-26 11:39:35','Ingreso',1,'En la maÃ±ana'),(2,654.00,'2017-02-12 12:43:24','Egreso',1,'prestamo 569');

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `cuit` varchar(15) DEFAULT NULL,
  `domicilio` varchar(45) DEFAULT NULL,
  `departamento` varchar(25) DEFAULT NULL,
  `telefono` varchar(35) DEFAULT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `cliente` */

insert  into `cliente`(`id`,`dni`,`nombre`,`cuit`,`domicilio`,`departamento`,`telefono`,`fecha_ingreso`) values (1,'12345678','Raul Primero','123456789','DOMICILIO','ALBARDON','46464646','2017-01-10 00:00:00'),(3,'22222222','SEGUNDO','333333333333333','DOMICILIO','VALLE FERTIL','EEEE','2017-02-24 04:03:12');

/*Table structure for table `cobranza` */

DROP TABLE IF EXISTS `cobranza`;

CREATE TABLE `cobranza` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `prestamo_id` int(6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `aranceltarjeta` decimal(8,2) DEFAULT NULL,
  `ivaarancel` decimal(8,2) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL,
  `ivaintereses` decimal(8,2) DEFAULT NULL,
  `gastostarj1` decimal(8,2) DEFAULT NULL,
  `ivagtos1` decimal(8,2) DEFAULT NULL,
  `gastostarj2` decimal(8,2) DEFAULT NULL,
  `ivagtos2` decimal(8,2) DEFAULT NULL,
  `retiibb` decimal(8,2) DEFAULT NULL,
  `retlh` decimal(8,2) DEFAULT NULL,
  `retiva` decimal(8,2) DEFAULT NULL,
  `retgan` decimal(8,2) DEFAULT NULL,
  `acobrar` decimal(8,2) DEFAULT NULL,
  `sellos` decimal(8,2) DEFAULT NULL,
  `lotehogar` decimal(8,2) DEFAULT NULL,
  `neto` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `cobranza` */

insert  into `cobranza`(`id`,`prestamo_id`,`totalcupon`,`aranceltarjeta`,`ivaarancel`,`subtotal`,`interesestarjeta`,`ivaintereses`,`gastostarj1`,`ivagtos1`,`gastostarj2`,`ivagtos2`,`retiibb`,`retlh`,`retiva`,`retgan`,`acobrar`,`sellos`,`lotehogar`,`neto`) values (32,24,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(33,25,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(34,26,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(35,27,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(36,28,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(37,29,1551.50,46.55,9.78,1185.20,366.30,76.92,3.00,3.00,3.00,3.00,37.38,7.48,44.86,14.95,944.28,14.16,2.83,927.29),(38,30,1551.50,46.55,9.78,1185.20,366.30,76.92,3.00,3.00,3.00,3.00,37.38,7.48,44.86,14.95,944.28,14.16,2.83,927.29),(39,31,1551.50,46.55,9.78,1185.20,366.30,76.92,0.00,0.00,0.00,0.00,37.38,7.48,44.86,14.95,947.28,14.21,2.84,930.23),(40,32,163.02,4.89,1.03,124.52,38.50,8.09,0.00,0.00,0.00,0.00,3.93,0.79,4.71,1.57,99.51,1.49,0.30,97.72),(41,33,1630.05,48.90,10.27,1245.20,384.85,80.82,0.00,0.00,0.00,0.00,39.27,7.85,47.13,15.71,995.25,14.93,2.99,977.33);

/*Table structure for table `cupondetarjeta` */

DROP TABLE IF EXISTS `cupondetarjeta`;

CREATE TABLE `cupondetarjeta` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tarjeta_id` int(3) DEFAULT NULL,
  `monto` decimal(8,2) DEFAULT NULL,
  `cuotas` int(3) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `cliente_id` int(7) DEFAULT NULL,
  `numero_cupon` varchar(15) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL COMMENT 'nuevo, con prestamo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `cupondetarjeta` */

insert  into `cupondetarjeta`(`id`,`tarjeta_id`,`monto`,`cuotas`,`fechahora`,`cliente_id`,`numero_cupon`,`estado`) values (12,2,1551.00,12,'2017-03-31 22:48:19',3,'1','completo'),(13,2,1550.00,12,'2017-03-31 23:07:55',3,'2','completo'),(14,2,1000.00,12,'2017-04-24 23:42:52',1,'1231','completo'),(15,2,1551.49,12,'2017-04-25 00:08:46',1,'26','completo'),(16,2,1551.50,12,'2017-04-25 00:20:01',1,'3','completo'),(17,2,1630.03,12,'2017-04-25 00:28:55',1,'22222','completo');

/*Table structure for table `detalleprestamo` */

DROP TABLE IF EXISTS `detalleprestamo`;

CREATE TABLE `detalleprestamo` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `prestamo_id` int(6) DEFAULT NULL,
  `prestamo` decimal(8,2) DEFAULT NULL COMMENT 'monto a pagar por el cliente',
  `intereses` decimal(8,2) DEFAULT NULL,
  `iva` decimal(8,2) DEFAULT NULL,
  `plus` decimal(8,2) DEFAULT NULL,
  `subtotal1` decimal(8,2) DEFAULT NULL,
  `gtosadmin` decimal(8,2) DEFAULT NULL,
  `ivagtos` decimal(8,2) DEFAULT NULL,
  `subtotal2` decimal(8,2) DEFAULT NULL,
  `cuotas` decimal(8,2) DEFAULT NULL,
  `coeficientetarjeta` decimal(8,6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `detalleprestamo` */

insert  into `detalleprestamo`(`id`,`prestamo_id`,`prestamo`,`intereses`,`iva`,`plus`,`subtotal1`,`gtosadmin`,`ivagtos`,`subtotal2`,`cuotas`,`coeficientetarjeta`,`totalcupon`,`interesestarjeta`) values (19,24,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(20,25,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(21,26,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(22,27,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(23,28,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(24,29,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(25,30,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(26,31,1551.50,120.00,25.20,40.00,1185.20,35.56,7.47,1228.23,12.00,1.263200,1551.50,366.30),(27,32,163.02,12.00,2.52,10.00,124.52,3.74,0.79,129.05,12.00,1.263200,163.02,38.50),(28,33,1630.05,120.00,25.20,100.00,1245.20,37.36,7.85,1290.41,12.00,1.263200,1630.05,384.85);

/*Table structure for table `gastos_pres_tarj` */

DROP TABLE IF EXISTS `gastos_pres_tarj`;

CREATE TABLE `gastos_pres_tarj` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `item` varchar(25) DEFAULT NULL,
  `valor` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `gastos_pres_tarj` */

insert  into `gastos_pres_tarj`(`id`,`item`,`valor`) values (1,'interes',12.00),(2,'iva',21.00),(3,'plus',10.00),(4,'Gastos Administrativos',3.00),(5,'Sellos',1.50),(6,'Retenciones iibb',2.50),(7,'Retenciones Lote Hogar',20.00),(8,'Retenciones iva',3.00),(9,'Retenciones Ganancias',1.00),(10,'sellos',1.50),(11,'Adicional Lote Hogar',20.00);

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `usuario_id` int(2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login` */

/*Table structure for table `mastercard` */

DROP TABLE IF EXISTS `mastercard`;

CREATE TABLE `mastercard` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `mastercard` */

insert  into `mastercard`(`id`,`cuota`,`coeficiente`) values (1,2,1.0504),(2,3,1.0684),(3,4,1.0865),(4,5,1.1049),(5,6,1.1235),(6,7,1.1562),(7,8,1.1772),(8,9,1.1983),(9,10,1.2197),(10,11,1.2413),(11,12,1.2632),(12,13,1.2853),(13,14,1.3076),(14,15,1.3302),(15,16,1.3530),(16,17,1.3760),(17,18,1.3992),(18,19,1.4227),(19,20,1.4464),(20,21,1.4704),(21,22,1.4945),(22,23,1.5189),(23,24,1.5435);

/*Table structure for table `prestamo_tarjeta` */

DROP TABLE IF EXISTS `prestamo_tarjeta`;

CREATE TABLE `prestamo_tarjeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cupon_id` int(10) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `pdf_dni` varchar(70) DEFAULT NULL,
  `montoprestamo` decimal(8,2) DEFAULT NULL COMMENT 'monto prestado',
  `monto_apagar` decimal(8,2) DEFAULT NULL COMMENT 'monto a devolver por el cliente',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `prestamo_tarjeta` */

insert  into `prestamo_tarjeta`(`id`,`cupon_id`,`fechahora`,`pdf_dni`,`montoprestamo`,`monto_apagar`) values (24,12,'2017-03-31 22:49:15','dni_manualpre.pdf',1000.00,930.23),(25,13,'2017-03-31 23:10:19','dni_dni_manualpre.pdf',1000.00,930.23),(26,14,'2017-04-24 23:55:37','dni_tumoney_1.pdf',1000.00,930.23),(27,14,'2017-04-24 23:55:57','dni_tumoney_1.pdf',1000.00,930.23),(28,14,'2017-04-24 23:56:12','dni_INSTRUCTIVO-POSTULANTES.pdf',1000.00,930.23),(29,14,'2017-04-25 00:06:54','dni_INSTRUCTIVO-POSTULANTES.pdf',1000.00,927.29),(30,15,'2017-04-25 00:09:00','dni_PagoNevada_Abril_2017_Marcela.pdf',1000.00,927.29),(31,16,'2017-04-25 00:20:12','dni_IEE-Prof.-soporte-informÃ¡tico(1).pdf',1000.00,930.23),(32,16,'2017-04-25 00:21:01','dni_INSTRUCTIVO-POSTULANTES.pdf',100.00,97.72),(33,17,'2017-04-25 00:29:07','dni_IEE-Prof.-soporte-informÃ¡tico(1).pdf',1000.00,977.33);

/*Table structure for table `tarjeta` */

DROP TABLE IF EXISTS `tarjeta`;

CREATE TABLE `tarjeta` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `coeficiente` varchar(50) DEFAULT NULL COMMENT 'nombre de la tabla',
  `arancel` decimal(5,2) DEFAULT NULL,
  `gastos1` decimal(6,2) DEFAULT NULL,
  `gastos2` decimal(6,2) DEFAULT NULL,
  `gastos3` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tarjeta` */

insert  into `tarjeta`(`id`,`nombre`,`coeficiente`,`arancel`,`gastos1`,`gastos2`,`gastos3`) values (2,'Nevada Visa','visa',3.00,0.00,0.00,0.00),(3,'Mastercard','mastercard',3.00,0.00,0.00,0.00);

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` varchar(150) CHARACTER SET latin1 NOT NULL,
  `nombrereal` varchar(25) CHARACTER SET latin1 NOT NULL,
  `nivel` int(2) NOT NULL COMMENT '1-administrador 2-empleado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `usuario` */

insert  into `usuario`(`id`,`usuario`,`password`,`nombrereal`,`nivel`) values (1,'admin','202cb962ac59075b964b07152d234b70','Administrador',1),(3,'qaz','202cb962ac59075b964b07152d234b70','qaz',2),(4,'wqrqwr','202cb962ac59075b964b07152d234b70','wqrqwrwqr',2);

/*Table structure for table `visa` */

DROP TABLE IF EXISTS `visa`;

CREATE TABLE `visa` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL COMMENT 'coefiente segun cuota',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `visa` */

insert  into `visa`(`id`,`cuota`,`coeficiente`) values (1,2,1.0504),(2,3,1.0684),(3,4,1.0865),(4,5,1.1049),(5,6,1.1235),(6,7,1.1562),(7,8,1.1772),(8,9,1.1983),(9,10,1.2197),(10,11,1.2413),(11,12,1.2632),(12,13,1.2853),(13,14,1.3076),(14,15,1.3302),(15,16,1.3530),(16,17,1.3760),(17,18,1.3992),(18,19,1.4227),(19,20,1.4464),(20,21,1.4704),(21,22,1.4945),(22,23,1.5189),(23,24,1.5435),(24,25,1.5684);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
