/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - nuevo2017
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nuevo2017` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nuevo2017`;

/*Table structure for table `cajadetalle` */

DROP TABLE IF EXISTS `cajadetalle`;

CREATE TABLE `cajadetalle` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `cajanueva_id` int(8) DEFAULT NULL,
  `operacion` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `monto` decimal(8,2) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `detalle` varchar(60) DEFAULT NULL,
  `usuario_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `cajadetalle` */

insert  into `cajadetalle`(`id`,`cajanueva_id`,`operacion`,`tipo`,`monto`,`fechahora`,`detalle`,`usuario_id`) values (10,1,'ingreso','Ingreso',15000.00,'2017-06-14 01:02:10','factura',1),(11,1,'Egreso','Egreso',10.00,'2017-06-14 01:09:35','566',1),(12,1,'Egreso','Egreso',10.00,'2017-06-14 01:42:01','resta',1),(13,1,'Ingreso','Egreso',10.00,'2017-06-14 03:23:34','233223',1),(14,1,'Ingreso','Ingreso',30.00,'2017-06-14 03:46:44','1313',1),(15,1,'Egreso','Egreso',100.00,'2017-06-14 03:48:01','ghhhh',1),(16,1,'Gastos Administrativos','Egreso',2.00,'2017-06-14 04:09:21','tttt',1),(17,1,'Egreso','Egreso',20.00,'2017-06-14 04:11:13','y',1),(18,0,'Egreso','Egreso',5.00,'2017-06-14 04:13:13','414654',1),(19,0,'Egreso','Egreso',1.00,'2017-06-14 04:14:24','6',1),(20,0,'Gastos Administrativos','Egreso',4.00,'2017-06-14 04:16:24','4',1),(21,1,'Egreso','Egreso',4.00,'2017-06-14 04:17:11','4',1),(22,1,'Gastos Administrativos','Egreso',6.00,'2017-06-14 04:20:47','6',1),(23,1,'Egreso','Egreso',3000.00,'2017-06-14 04:29:04','64',1);

/*Table structure for table `cajanueva` */

DROP TABLE IF EXISTS `cajanueva`;

CREATE TABLE `cajanueva` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `inicio` decimal(8,2) DEFAULT NULL,
  `fechaapertura` datetime DEFAULT NULL,
  `cierre` decimal(8,2) DEFAULT NULL,
  `fechacierre` datetime DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL COMMENT 'Abierto o Cerrado',
  `saldo` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `cajanueva` */

insert  into `cajanueva`(`id`,`inicio`,`fechaapertura`,`cierre`,`fechacierre`,`estado`,`saldo`) values (1,15000.00,'2017-06-13 22:39:01',0.00,'2001-01-01 00:00:00','Abierto',0.00);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
