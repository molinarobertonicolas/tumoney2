SELECT
cliente.`nombre` AS nombre,
cliente.`cuit` AS cuit,
cliente.`domicilio` AS calle,
cliente.`departamento` AS departamento,
prestamo_tarjeta.`montoprestamo` AS montoprestamo,
detalleprestamo.`plus` AS plus,
detalleprestamo.`gtosadmin` AS gastosadministrativos,
detalleprestamo.`interesestarjeta` AS interestarjeta, 
tarjeta.`nombre` AS nombretarjeta,
cupondetarjeta.`cuotas` AS cuotas,
cupondetarjeta.`monto` AS totalcupon,
cupondetarjeta.`numero_cupon` AS numeroCupon
FROM
    `cliente`
    INNER JOIN `cupondetarjeta` 
        ON (`cliente`.`id` = `cupondetarjeta`.`cliente_id`)
    INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
    INNER JOIN `prestamo_tarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
    INNER JOIN `detalleprestamo` 
        ON (`prestamo_tarjeta`.`id` = `detalleprestamo`.`prestamo_id`)
        WHERE cupondetarjeta.`id`=8;