-- phpMyAdmin SQL Dump
-- version 4.0.10.15
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-05-2017 a las 19:19:53
-- Versión del servidor: 5.6.31-log
-- Versión de PHP: 5.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `c0250456_tumoney`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE IF NOT EXISTS `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` double(8,2) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `operacion` varchar(7) DEFAULT NULL,
  `usuario_id` int(2) DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `monto`, `fechahora`, `operacion`, `usuario_id`, `observacion`) VALUES
(3, 10000.00, '2017-04-23 23:58:54', 'Ingreso', 1, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `cuit` varchar(15) DEFAULT NULL,
  `domicilio` varchar(45) DEFAULT NULL,
  `departamento` varchar(25) DEFAULT NULL,
  `telefono` varchar(35) DEFAULT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `dni`, `nombre`, `cuit`, `domicilio`, `departamento`, `telefono`, `fecha_ingreso`) VALUES
(1, '12345678', 'Raul Primero', '123456789', 'DOMICILIO', 'ALBARDON', '46464646', '2017-01-10 00:00:00'),
(10, '28877834', 'toselli', '23288778344', '122', 'ALBARDON', '122', '2017-04-24 09:55:47'),
(11, '28844877', 'mecha', '2328844877', '222', 'ALBARDON', '222', '2017-04-24 10:46:24'),
(12, '48571322', 'vil', '23485713224', '2222', 'ALBARDON', '222232', '2017-04-24 11:18:32'),
(13, '44571321', '2525', '525', '2536', 'ALBARDON', '2536', '2017-04-24 11:48:32'),
(14, '12833877', 'eess', '2012833877', '2222', 'ALBARDON', '12121', '2017-04-24 12:07:48'),
(15, '48571333', '5252', '20485713334', '25252', 'ALBARDON', '25656', '2017-04-24 12:22:44'),
(16, '48888888', '2536', '253698', '1254', 'ALBARDON', '362514', '2017-04-24 18:34:35'),
(17, '48555555', '253636', '25363656', '25366', 'ALBARDON', '256656', '2017-04-24 19:28:57'),
(18, '45555555', '253699', '999999', '125666', 'ALBARDON', '122233', '2017-04-24 20:03:57'),
(19, '45666333', 'maria', '2564', '2564', 'ALBARDON', '3366', '2017-04-24 20:05:15'),
(20, '22222333', 'juan perez', '20222223332', 'dsad', 'ALBARDON', '1234156', '2017-04-24 20:39:50'),
(21, '48834877', 'mar', '20488348774', 'alem', 'ALBARDON', '155445544', '2017-04-25 09:12:12'),
(22, '47834877', 'ma', '2047834877', 'mol', 'ALBARDON', '15544558', '2017-04-25 09:15:12'),
(23, '24480733', 'Figueroa MariaRrosa', '27-24480733-5', 'Costa canal y Caseros Los Medanos', 'CAUCETE', '2644717252', '2017-04-28 11:48:44'),
(24, '21520563', 'Vazquez Fernando Jose', '20-21520563-4', 'Ramon y Cajal 122 norte', 'CAPITAL', '2644111277', '2017-04-28 12:07:26'),
(25, '36033516', 'Carrizo Diego Emanuel', '20-36033516-0', 'Hugo Wast 3988 Va. Don Arturo ', 'SANTA LUCIA', '2644254669', '2017-05-02 20:04:53'),
(26, '35049781', 'Mingolla Flores Yohana El', '27-35049781-7', 'Larrain 1907 sur Bo. Las ViÃ±as', 'SANTA LUCIA', '2645575460', '2017-05-03 11:28:29'),
(27, '04188253', 'Cortez Sofia Norma', 'F 4188253', 'Etados Unidos 1066 sur', 'CAPITAL', '2645047380', '2017-05-03 20:13:23'),
(28, '10348915', 'Gallardo Violeta del Carm', '27-10348915-1', 'Las Heras 642 norte Vo. FERROVIARIO', 'CAPITAL', '2644222993', '2017-05-04 18:31:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranza`
--

CREATE TABLE IF NOT EXISTS `cobranza` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `prestamo_id` int(6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `aranceltarjeta` decimal(8,2) DEFAULT NULL,
  `ivaarancel` decimal(8,2) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL,
  `ivaintereses` decimal(8,2) DEFAULT NULL,
  `gastostarj1` decimal(8,2) DEFAULT NULL,
  `ivagtos1` decimal(8,2) DEFAULT NULL,
  `gastostarj2` decimal(8,2) DEFAULT NULL,
  `ivagtos2` decimal(8,2) DEFAULT NULL,
  `retiibb` decimal(8,2) DEFAULT NULL,
  `retlh` decimal(8,2) DEFAULT NULL,
  `retiva` decimal(8,2) DEFAULT NULL,
  `retgan` decimal(8,2) DEFAULT NULL,
  `acobrar` decimal(8,2) DEFAULT NULL,
  `sellos` decimal(8,2) DEFAULT NULL,
  `lotehogar` decimal(8,2) DEFAULT NULL,
  `neto` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Volcado de datos para la tabla `cobranza`
--

INSERT INTO `cobranza` (`id`, `prestamo_id`, `totalcupon`, `aranceltarjeta`, `ivaarancel`, `subtotal`, `interesestarjeta`, `ivaintereses`, `gastostarj1`, `ivagtos1`, `gastostarj2`, `ivagtos2`, `retiibb`, `retlh`, `retiva`, `retgan`, `acobrar`, `sellos`, `lotehogar`, `neto`) VALUES
(42, 34, 1551.50, 46.55, 9.78, 1185.20, 366.30, 76.92, 0.00, 0.00, 0.00, 0.00, 37.38, 7.48, 44.86, 14.95, 947.28, 14.21, 2.84, 930.23),
(43, 35, 2407.12, 72.21, 15.16, 1838.83, 568.29, 119.34, 0.00, 0.00, 0.00, 0.00, 57.99, 11.60, 69.59, 23.20, 1469.74, 22.05, 4.41, 1443.28),
(44, 36, 8132.63, 243.98, 51.24, 7471.20, 661.43, 138.90, 0.00, 0.00, 0.00, 0.00, 195.94, 39.19, 235.12, 78.37, 6488.46, 97.33, 19.47, 6371.66),
(45, 37, 8698.60, 260.96, 54.80, 7471.20, 1227.40, 257.75, 0.00, 0.00, 0.00, 0.00, 209.57, 41.91, 251.49, 83.83, 6310.89, 94.66, 18.93, 6197.30),
(46, 38, 8698.60, 260.96, 54.80, 7471.20, 1227.40, 257.75, 0.00, 0.00, 0.00, 0.00, 209.57, 41.91, 251.49, 83.83, 6310.89, 94.66, 18.93, 6197.30),
(47, 39, 7248.82, 217.46, 45.67, 6226.00, 1022.82, 214.79, 0.00, 0.00, 0.00, 0.00, 174.64, 34.93, 209.57, 69.86, 5259.08, 78.89, 15.78, 5164.41),
(48, 40, 6893.32, 206.80, 43.43, 6226.00, 667.32, 140.14, 0.00, 0.00, 0.00, 0.00, 166.08, 33.22, 199.29, 66.43, 5370.61, 80.56, 16.11, 5273.94),
(49, 41, 16300.35, 489.01, 102.69, 12452.00, 3848.35, 808.15, 0.00, 0.00, 0.00, 0.00, 392.72, 78.54, 471.26, 157.09, 9952.54, 149.29, 29.86, 9773.39),
(50, 42, 1378.67, 41.36, 8.69, 1245.20, 133.47, 28.03, 0.00, 0.00, 0.00, 0.00, 33.22, 6.64, 39.86, 13.29, 1074.11, 16.11, 3.22, 1054.78),
(51, 43, 7248.82, 217.46, 45.67, 6226.00, 1022.82, 214.79, 0.00, 0.00, 0.00, 0.00, 174.64, 34.93, 209.57, 69.86, 5259.08, 78.89, 15.78, 5164.41),
(52, 44, 1630.05, 48.90, 10.27, 1245.20, 384.85, 80.82, 0.00, 0.00, 0.00, 0.00, 39.27, 7.85, 47.13, 15.71, 995.25, 14.93, 2.99, 977.33),
(53, 45, 1630.05, 48.90, 10.27, 1245.20, 384.85, 80.82, 0.00, 0.00, 0.00, 0.00, 39.27, 7.85, 47.13, 15.71, 995.25, 14.93, 2.99, 977.33),
(54, 46, 8698.60, 260.96, 54.80, 7471.20, 1227.40, 257.75, 0.00, 0.00, 0.00, 0.00, 209.57, 41.91, 251.49, 83.83, 6310.89, 94.66, 18.93, 6197.30),
(55, 47, 11410.23, 342.31, 71.89, 8716.40, 2693.83, 565.70, 0.00, 0.00, 0.00, 0.00, 274.90, 54.98, 329.88, 109.96, 6966.78, 104.50, 20.90, 6841.38),
(56, 48, 11017.31, 330.52, 69.41, 8716.40, 2300.91, 483.19, 0.00, 0.00, 0.00, 0.00, 265.43, 53.09, 318.52, 106.17, 7090.07, 106.35, 21.27, 6962.45),
(57, 49, 11017.31, 330.52, 69.41, 8716.40, 2300.91, 483.19, 0.00, 0.00, 0.00, 0.00, 265.43, 53.09, 318.52, 106.17, 7090.07, 106.35, 21.27, 6962.45),
(58, 50, 1630.05, 48.90, 10.27, 1245.20, 384.85, 80.82, 0.00, 0.00, 0.00, 0.00, 39.27, 7.85, 47.13, 15.71, 995.25, 14.93, 2.99, 977.33),
(59, 51, 2899.53, 86.99, 18.27, 2490.40, 409.13, 85.92, 0.00, 0.00, 0.00, 0.00, 69.86, 13.97, 83.83, 27.94, 2103.62, 42.07, 8.41, 2053.14),
(60, 52, 1449.78, 43.49, 9.13, 1245.20, 204.58, 42.96, 0.00, 0.00, 0.00, 0.00, 34.93, 6.99, 41.91, 13.97, 1051.82, 21.04, 4.21, 1026.57),
(61, 53, 1449.78, 43.49, 9.13, 1245.20, 204.58, 42.96, 0.00, 0.00, 0.00, 0.00, 34.93, 6.99, 41.91, 13.97, 1051.82, 21.04, 4.21, 1026.57),
(62, 54, 2899.53, 86.99, 18.27, 2490.40, 409.13, 85.92, 0.00, 0.00, 0.00, 0.00, 69.86, 13.97, 83.83, 27.94, 2103.62, 42.07, 8.41, 2053.14),
(63, 55, 5074.19, 152.23, 31.97, 4358.20, 715.99, 150.36, 0.00, 0.00, 0.00, 0.00, 122.25, 24.45, 146.70, 48.90, 3681.34, 73.63, 14.73, 3592.98),
(64, 56, 9780.21, 293.41, 61.62, 9425.18, 2309.01, 484.89, 0.00, 0.00, 0.00, 0.00, 235.63, 47.13, 282.76, 94.25, 5971.51, 119.43, 23.89, 5828.19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupondetarjeta`
--

CREATE TABLE IF NOT EXISTS `cupondetarjeta` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tarjeta_id` int(3) DEFAULT NULL,
  `monto` decimal(8,2) DEFAULT NULL,
  `cuotas` int(3) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `cliente_id` int(7) DEFAULT NULL,
  `numero_cupon` varchar(15) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL COMMENT 'nuevo, con prestamo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Volcado de datos para la tabla `cupondetarjeta`
--

INSERT INTO `cupondetarjeta` (`id`, `tarjeta_id`, `monto`, `cuotas`, `fechahora`, `cliente_id`, `numero_cupon`, `estado`) VALUES
(42, 2, 2899.53, 6, '2017-04-28 11:49:44', 23, '19087106', 'completo'),
(43, 3, 1449.78, 6, '2017-04-28 12:09:27', 24, '390412', 'completo'),
(44, 3, 1449.78, 6, '2017-05-02 20:06:19', 25, '629284', 'completo'),
(45, 2, 2899.53, 6, '2017-05-03 11:31:20', 26, '737668', 'completo'),
(46, 2, 5074.19, 6, '2017-05-03 20:14:04', 27, '910515', 'completo'),
(47, 2, 9780.21, 12, '2017-05-04 18:32:20', 28, '913041', 'completo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleprestamo`
--

CREATE TABLE IF NOT EXISTS `detalleprestamo` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `prestamo_id` int(6) DEFAULT NULL,
  `prestamo` decimal(8,2) DEFAULT NULL COMMENT 'monto a pagar por el cliente',
  `intereses` decimal(8,2) DEFAULT NULL,
  `iva` decimal(8,2) DEFAULT NULL,
  `plus` decimal(8,2) DEFAULT NULL,
  `subtotal1` decimal(8,2) DEFAULT NULL,
  `gtosadmin` decimal(8,2) DEFAULT NULL,
  `ivagtos` decimal(8,2) DEFAULT NULL,
  `subtotal2` decimal(8,2) DEFAULT NULL,
  `cuotas` decimal(8,2) DEFAULT NULL,
  `coeficientetarjeta` decimal(8,6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `detalleprestamo`
--

INSERT INTO `detalleprestamo` (`id`, `prestamo_id`, `prestamo`, `intereses`, `iva`, `plus`, `subtotal1`, `gtosadmin`, `ivagtos`, `subtotal2`, `cuotas`, `coeficientetarjeta`, `totalcupon`, `interesestarjeta`) VALUES
(22, 27, 33254.88, 2760.00, 579.60, 920.00, 27259.60, 817.79, 171.74, 28249.13, 8.00, 1.177200, 33254.88, 5995.28),
(29, 34, 1551.50, 120.00, 25.20, 40.00, 1185.20, 35.56, 7.47, 1228.23, 12.00, 1.263200, 1551.50, 366.30),
(30, 35, 2407.12, 186.18, 39.10, 62.06, 1838.83, 55.16, 11.58, 1905.57, 12.00, 1.263200, 2407.12, 568.29),
(31, 36, 8132.63, 720.00, 151.20, 600.00, 7471.20, 224.14, 47.07, 7742.41, 2.00, 1.050400, 8132.63, 661.43),
(32, 37, 8698.60, 720.00, 151.20, 600.00, 7471.20, 224.14, 47.07, 7742.41, 6.00, 1.123500, 8698.60, 1227.40),
(33, 38, 8698.60, 720.00, 151.20, 600.00, 7471.20, 224.14, 47.07, 7742.41, 6.00, 1.123500, 8698.60, 1227.40),
(34, 39, 7248.82, 600.00, 126.00, 500.00, 6226.00, 186.78, 39.22, 6452.00, 6.00, 1.123500, 7248.82, 1022.82),
(35, 40, 6893.32, 600.00, 126.00, 500.00, 6226.00, 186.78, 39.22, 6452.00, 3.00, 1.068400, 6893.32, 667.32),
(36, 41, 16300.35, 1200.00, 252.00, 1000.00, 12452.00, 373.56, 78.45, 12904.01, 12.00, 1.263200, 16300.35, 3848.35),
(37, 42, 1378.67, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 3.00, 1.068400, 1378.67, 133.47),
(38, 43, 7248.82, 600.00, 126.00, 500.00, 6226.00, 186.78, 39.22, 6452.00, 6.00, 1.123500, 7248.82, 1022.82),
(39, 44, 1630.05, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 12.00, 1.263200, 1630.05, 384.85),
(40, 45, 1630.05, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 12.00, 1.263200, 1630.05, 384.85),
(41, 46, 8698.60, 720.00, 151.20, 600.00, 7471.20, 224.14, 47.07, 7742.41, 6.00, 1.123500, 8698.60, 1227.40),
(42, 47, 11410.23, 840.00, 176.40, 700.00, 8716.40, 261.49, 54.91, 9032.80, 12.00, 1.263200, 11410.23, 2693.83),
(43, 48, 11017.31, 840.00, 176.40, 700.00, 8716.40, 261.49, 54.91, 9032.80, 10.00, 1.219700, 11017.31, 2300.91),
(44, 49, 11017.31, 840.00, 176.40, 700.00, 8716.40, 261.49, 54.91, 9032.80, 10.00, 1.219700, 11017.31, 2300.91),
(45, 50, 1630.05, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 12.00, 1.263200, 1630.05, 384.85),
(46, 51, 2899.53, 240.00, 50.40, 200.00, 2490.40, 74.71, 15.69, 2580.80, 6.00, 1.123500, 2899.53, 409.13),
(47, 52, 1449.78, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 6.00, 1.123500, 1449.78, 204.58),
(48, 53, 1449.78, 120.00, 25.20, 100.00, 1245.20, 37.36, 7.85, 1290.41, 6.00, 1.123500, 1449.78, 204.58),
(49, 54, 2899.53, 240.00, 50.40, 200.00, 2490.40, 74.71, 15.69, 2580.80, 6.00, 1.123500, 2899.53, 409.13),
(50, 55, 5074.19, 420.00, 88.20, 350.00, 4358.20, 130.75, 27.46, 4516.41, 6.00, 1.123500, 5074.19, 715.99),
(51, 56, 9780.21, 720.00, 151.20, 600.00, 7471.20, 224.14, 47.07, 7742.41, 12.00, 1.263200, 9780.21, 2309.01);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_pres_tarj`
--

CREATE TABLE IF NOT EXISTS `gastos_pres_tarj` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `item` varchar(25) DEFAULT NULL,
  `valor` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `gastos_pres_tarj`
--

INSERT INTO `gastos_pres_tarj` (`id`, `item`, `valor`) VALUES
(1, 'interes', 12.00),
(2, 'iva', 21.00),
(3, 'plus', 10.00),
(4, 'Gastos Administrativos', 3.00),
(5, 'Sellos', 1.50),
(6, 'Retenciones iibb', 2.50),
(7, 'Retenciones Lote Hogar', 20.00),
(8, 'Retenciones iva', 3.00),
(9, 'Retenciones Ganancias', 1.00),
(10, 'sellos', 2.00),
(11, 'Adicional Lote Hogar', 20.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL,
  `usuario_id` int(2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mastercard`
--

CREATE TABLE IF NOT EXISTS `mastercard` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `mastercard`
--

INSERT INTO `mastercard` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, 1.0504),
(2, 3, 1.0684),
(3, 4, 1.0865),
(4, 5, 1.1049),
(5, 6, 1.1235),
(6, 7, 1.1562),
(7, 8, 1.1772),
(8, 9, 1.1983),
(9, 10, 1.2197),
(10, 11, 1.2413),
(11, 12, 1.2632),
(12, 13, 1.2853),
(13, 14, 1.3076),
(14, 15, 1.3302),
(15, 16, 1.3530),
(16, 17, 1.3760),
(17, 18, 1.3992),
(18, 19, 1.4227),
(19, 20, 1.4464),
(20, 21, 1.4704),
(21, 22, 1.4945),
(22, 23, 1.5189),
(23, 24, 1.5435),
(24, 25, 1.5684),
(25, 26, 1.5934),
(26, 27, 1.6187),
(27, 28, 1.6442),
(28, 29, 1.6699),
(29, 30, 1.6958),
(30, 31, 1.7219),
(31, 32, 1.7482),
(32, 33, 1.7748),
(33, 34, 1.8015),
(34, 35, 1.8285),
(35, 36, 1.8556),
(36, 37, 1.8830),
(37, 38, 1.9105),
(38, 39, 1.9383),
(39, 40, 1.9662),
(40, 41, 1.9944),
(41, 42, 2.0227),
(42, 43, 2.0512),
(43, 44, 2.0799),
(44, 45, 2.1088),
(45, 46, 2.1379),
(46, 47, 2.1671),
(47, 48, 2.1965),
(48, 49, 2.2261),
(49, 50, 2.2559);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo_tarjeta`
--

CREATE TABLE IF NOT EXISTS `prestamo_tarjeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cupon_id` int(10) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `pdf_dni` varchar(70) DEFAULT NULL,
  `montoprestamo` decimal(8,2) DEFAULT NULL COMMENT 'monto prestado',
  `monto_apagar` decimal(8,2) DEFAULT NULL COMMENT 'monto a devolver por el cliente',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Volcado de datos para la tabla `prestamo_tarjeta`
--

INSERT INTO `prestamo_tarjeta` (`id`, `cupon_id`, `fechahora`, `pdf_dni`, `montoprestamo`, `monto_apagar`) VALUES
(51, 42, '2017-04-28 11:51:20', 'dni_24480733.jpg', 2000.00, 2053.14),
(52, 43, '2017-04-28 12:13:46', 'dni_21520563.jpg', 1000.00, 1026.57),
(53, 44, '2017-05-02 20:07:37', 'dni_36033516.jpg', 1000.00, 1026.57),
(54, 45, '2017-05-03 11:39:23', 'dni_35049781.bmp', 2000.00, 2053.14),
(55, 46, '2017-05-03 20:18:07', 'dni_04188253.bmp', 3500.00, 3592.98),
(56, 47, '2017-05-04 18:36:49', 'dni_10348915.bmp', 6000.00, 5828.19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE IF NOT EXISTS `tarjeta` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `coeficiente` varchar(50) DEFAULT NULL COMMENT 'nombre de la tabla',
  `arancel` decimal(5,2) DEFAULT NULL,
  `gastos1` decimal(6,2) DEFAULT NULL,
  `gastos2` decimal(6,2) DEFAULT NULL,
  `gastos3` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tarjeta`
--

INSERT INTO `tarjeta` (`id`, `nombre`, `coeficiente`, `arancel`, `gastos1`, `gastos2`, `gastos3`) VALUES
(2, 'Tarjeta Data', 'visa', 3.00, 0.00, 0.00, 0.00),
(3, 'Mastercard', 'mastercard', 3.00, 0.00, 0.00, 0.00),
(4, 'Visa', 'visa', 3.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` varchar(150) CHARACTER SET latin1 NOT NULL,
  `nombrereal` varchar(25) CHARACTER SET latin1 NOT NULL,
  `nivel` int(2) NOT NULL COMMENT '1-administrador 2-empleado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `nombrereal`, `nivel`) VALUES
(1, 'rmolina', '202cb962ac59075b964b07152d234b70', 'Administrador', 1),
(5, 'jorge2017', '202cb962ac59075b964b07152d234b70', 'Jorge Adm.', 1),
(6, 'Operador', '202cb962ac59075b964b07152d234b70', 'Operador1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visa`
--

CREATE TABLE IF NOT EXISTS `visa` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL COMMENT 'coefiente segun cuota',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `visa`
--

INSERT INTO `visa` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, 1.0504),
(2, 3, 1.0684),
(3, 4, 1.0865),
(4, 5, 1.1049),
(5, 6, 1.1235),
(6, 7, 1.1562),
(7, 8, 1.1772),
(8, 9, 1.1983),
(9, 10, 1.2197),
(10, 11, 1.2413),
(11, 12, 1.2632),
(12, 13, 1.2853),
(13, 14, 1.3076),
(14, 15, 1.3302),
(15, 16, 1.3530),
(16, 17, 1.3760),
(17, 18, 1.3992),
(18, 19, 1.4227),
(19, 20, 1.4464),
(20, 21, 1.4704),
(21, 22, 1.4945),
(22, 23, 1.5189),
(23, 24, 1.5435),
(24, 25, 1.5684),
(25, 26, 1.5934),
(26, 27, 1.6187),
(27, 28, 1.6442),
(28, 29, 1.6699),
(29, 30, 1.6958),
(30, 31, 1.7219),
(31, 32, 1.7482),
(32, 33, 1.7748),
(33, 34, 1.8015),
(34, 35, 1.8285),
(35, 36, 1.8556),
(36, 37, 1.8830),
(37, 38, 1.9105),
(38, 39, 1.9383),
(39, 40, 1.9662),
(40, 41, 1.9944),
(41, 42, 2.0227),
(42, 43, 2.0512),
(43, 44, 2.0799),
(44, 45, 2.1088),
(45, 46, 2.1379),
(46, 47, 2.1671),
(47, 48, 2.1956),
(48, 49, 2.2261),
(49, 50, 2.2559);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
