/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - nuevo2017
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nuevo2017` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nuevo2017`;

/*Table structure for table `facturab` */

DROP TABLE IF EXISTS `facturab`;

CREATE TABLE `facturab` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `prestamo_id` int(8) DEFAULT NULL,
  `monto_solicitado` decimal(10,2) DEFAULT NULL,
  `plus` decimal(10,2) DEFAULT NULL,
  `monto_prestamo_facturado` decimal(10,2) DEFAULT NULL,
  `interes` decimal(10,2) DEFAULT NULL,
  `iva_interes` decimal(10,2) DEFAULT NULL,
  `interes_mas_iva` decimal(10,2) DEFAULT NULL,
  `gastos_administrativos` decimal(10,2) DEFAULT NULL,
  `interes_tarjeta` decimal(10,2) DEFAULT NULL,
  `gastos_adm_mas_interes` decimal(10,2) DEFAULT NULL,
  `iva_gastos_adm` decimal(10,2) DEFAULT NULL,
  `gastos_adm_con_iva` decimal(10,2) DEFAULT NULL,
  `total_factura_b` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `facturab` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
