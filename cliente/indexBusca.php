<?php
include("../sesion.php");
include("../menu.php");
include("cliente.php");
?>
 <div class="container">
    <h2>Listado de Clientes</h2>

    <p> <a class="btn btn-primary" href="nuevo.php">Adicionar Nuevo</a> </p>
    <strong class="text-primary" for="buscar_producto">Buscar Cliente &nbsp</strong>
    <input id="buscar_cliente"  type="text" name="buscar_cliente" placeholder="nombre o DNI del cliente">

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" style="display: none" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>DNI</th>
             <th>Nombre</th>
             <th>CUIT</th>
             <th>Domicilio</th>
             <th>Departamento</th>
             <th>Telefono</th>
              <th>Email</th>
             <th>Fecha de Ingreso</th>
			       <th>Fecha Nacimiento</th>
              <th>Prestamos</th>
             <th>Funciones</th>
             </tr>
           <thead>
           <tbody id="listadoClientes">

          </tbody>
         </table>


    <!-- ventana modal-->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Formulario Agregar Cliente</h4>
          </div>
          <div  class="modal-body">
            
          </div>
          <div class="modal-footer">
            <!--button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button-->
          </div>
        </div>
      </div>
    </div>
    <!-- fin-->
           </div>
         </div>
      </div>
    </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });

    function obtener_registros(clientes)
    {
        $.ajax({
            url : 'listarcliente.php',
            type : 'POST',
            dataType : 'html',
            data : { clientes: clientes },
            })

        .done(function(resultado){
            $("#listadoClientes").html(resultado);
        })
    }


    $(document).on('keyup', '#buscar_cliente', function()
    {
        var valorBusqueda=$(this).val();
         if (valorBusqueda.length>2)
         {
            if (valorBusqueda!="")
            {
                document.getElementById("listado").style.display="";
                obtener_registros(valorBusqueda);
            }
            else
                {
                    document.getElementById("listado").style.display="none";
                    obtener_registros();
                }
        }    
    });


</script>
</body>
</html>