<?php
include("../sesion.php");
include("../menu.php");
include("cliente.php");
if( isset($_GET['id']) && !empty($_GET['id']) )
 {
  $id = $_GET['id'];
?>
 <div class="container">
    <h2>Listado de Cuotas</h2>

      <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Cliente</th>
             <th>Prestamo</th>
             <th>Cuota</th>
             <th>Fecha</th>
             <th>Monto</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objecto = new Cliente();
          $clientes = cliente::prestamos_cuotas($id);
          foreach($clientes as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['cliente']; ?></td>
              <td><?php echo $item ['prestamo']; ?></td>
              <td><?php echo $item ['cuota']; ?></td>
               <td><?php echo $item ['fecha']; ?></td>
              <td><?php echo $item ['monto']; ?></td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  
 <?php
           }
          ?>
