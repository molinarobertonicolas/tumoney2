<?php
include_once("../bd/conexion.php");
class Cliente
{
  public function obtenerCliente($cliente)
  {
   $consulta="SELECT * FROM cliente where cliente='$cliente'";
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
      return $data;
    }else return $rs;
  }
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT * FROM cliente";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }



   public function nuevo($dni,$nombre,$cuit,$domicilio,$departamento,$telefono,$fecha_ingreso,$email,$fecha_nacimiento,$sucursal)
   	{
  	  $sql="INSERT INTO `cliente`
              (`dni`,
               `nombre`,
               `cuit`,
               `domicilio`,
               `departamento`,
               `telefono`,
               `fecha_ingreso`,
               `email`,`fecha_nacimiento`,`sucursal_id`)
      VALUES ('$dni',
              '$nombre',
              '$cuit',
              '$domicilio',
              '$departamento',
              '$telefono',
              '$fecha_ingreso',
              '$email','$fecha_nacimiento','$sucursal');";
              
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
    	}


	public function editar($id,$dni,$nombre,$domicilio,$departamento,$telefono,$fecha_ingreso,$fecha_nacimiento)
	{
      $sql="UPDATE `cliente`
            SET `id` = '$id',
              `dni` = '$dni',
              `nombre` = '$nombre',
              `domicilio` = '$domicilio',
              `departamento` = '$departamento',
              `telefono` = '$telefono',
              `fecha_ingreso` = '$fecha_ingreso',
			  `fecha_nacimiento` = '$fecha_nacimiento' 
            WHERE `id` = '$id';";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM cliente where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }

    public function eliminar($id)
	  {
     $sql="DELETE FROM cliente WHERE id ='$id'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
     return $rs;
    }
    
   //10-01-2017 devuelve si esta repetido un dni
   public function validarDNI($dni)
   {
      $sql="SELECT * FROM cliente WHERE dni ='$dni'";
      
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      /* determinar el número de filas del resultado por procedimiento */
      $cantidad = mysqli_num_rows($rs);
      return $cantidad;
    }  
   
   //27-07-2018 devuelve si esta repetido un dni
   public function buscarDNI($dni)
   {
      $data=array();   
      $sql="SELECT id, nombre, domicilio FROM cliente WHERE dni ='$dni'";
      
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
    }


  //23-11-2018 cuotas clientes
  public function cuotas_cliente($cliente_id)
  {
   $consulta="SELECT
    cliente.`id` AS cliente_id,
    cliente.`nombre` AS cliente,
    cuota_prestamo.`prestamo_id` AS prestamo,
    cuota_prestamo.`numero_cuota` AS cuota,
    cuota_prestamo.`fecha_liquidacion` AS fecha,
    cuota_prestamo.`monto_prestamo` AS monto
    FROM
        `cliente`
        INNER JOIN `cuota_prestamo` 
            ON (`cliente`.`id` = `cuota_prestamo`.`cliente_id`) WHERE cliente_id='$cliente_id' ORDER BY prestamo DESC, cuota DESC;";
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
      return $data;
    }else return $rs;
  } 
  
  // 15-09-2019 buscar todos los prestamos de un cliente
  public function cliente_prestamos($cliente_id)
  {
   $consulta="SELECT
              cliente.`nombre`,
              cliente.`dni`,
              cliente.`id`,
              prestamo_tarjeta.`id` AS prestamo
              FROM
                  `cliente`
                  INNER JOIN `cupondetarjeta` 
                      ON (`cliente`.`id` = `cupondetarjeta`.`cliente_id`)
                  INNER JOIN `prestamo_tarjeta` 
                      ON (`cupondetarjeta`.`id` = `prestamo_tarjeta`.`cupon_id`)
                      WHERE cliente.`id`=$cliente_id
                      ORDER BY prestamo DESC;";
                    
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
      return $data;
    }else return $rs;
  } 

  public function prestamos_cuotas($prestamo_id)
  {
   $consulta="SELECT
                  cliente.`id` AS cliente_id,
                  cliente.`nombre` AS cliente,
                  cuota_prestamo.`prestamo_id` AS prestamo,
                  cuota_prestamo.`numero_cuota` AS cuota,
                  cuota_prestamo.`fecha_liquidacion` AS fecha,
                  cuota_prestamo.`monto_prestamo` AS monto
                  FROM
                  `cuota_prestamo`
                  INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                  INNER JOIN `cliente` 
                      ON (`cliente`.`id` = `cuota_prestamo`.`cliente_id`)
                      WHERE prestamo_tarjeta.`id`=$prestamo_id
                      ORDER BY cuota DESC";
                    
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
      return $data;
    }else return $rs;
  } 












}
?>