<?

// 08052017 generar prestamo desde la calculadora con la modificacion

include("../prestamotarjeta/prestamotarjeta.php");

function porcentaje($cantidad,$porciento)
{
 //sin separador de miles y dos decimales separados por el .
 return number_format($cantidad*$porciento/100 ,2,".", "");
}

if( isset($_POST['tarjeta_id']) && !empty($_POST['tarjeta_id']) )
{
   $tarjeta_id=$_POST['tarjeta_id'];
   $montoprestamo=$_POST['montoprestamo'];
   $coefTarjeta=$_POST['coeficiente'];

   //buscar datos coeficiente por cuotas segun tarjeta de credito
   // son en Generar Prestamo $coefTarjeta = prestamotarjeta::buscarCoeficiente($tarjeta_id,$cuotas);
  
   // buscar gastos administrativos
    $listados = prestamotarjeta::traerGastosPrestamo();
    foreach($listados as $item)
    {
    //$gastosPrestamo[ ] = array('nombre'=>$item['item'], 'valor'=>$item['valor']);
     switch ($item['id']) 
            {
            case 1:
                $interes=$item ['valor'];
            case 2:
               $iva=$item ['valor'];
            case 3:
               $plus=$item ['valor'];
            case 4:
               $gastosAdm=$item ['valor'];
            case 5:
               $sello=$item ['valor'];  
            case 6:
                  $retenciones_iibb=$item ['valor'];
            case 7:
                  $retenciones_lh=$item ['valor'];
            case 8:
                  $retenciones_iva=$item ['valor'];
            case 9:
                  $retenciones_ganancia=$item ['valor'];
            case 10:
                  $sellos=$item ['valor'];  
            case 11:
                  $adicional_lote_hogar=$item ['valor'];                           
            }
      }//fin foreach            
  
  //calcular los gastos del prestamo
   
 //  '-intereses-'.
  $c_intereses=porcentaje($montoprestamo,$interes);
  //  '-iva-'.
  $c_iva=porcentaje($c_intereses,$iva);
  //  '-plus-'.
  $c_plus=porcentaje($montoprestamo,$plus);
  // '<br>';
  // subtotal 1
  //  'subtotal 1 :'.
  $subtotal1=round(($montoprestamo+$c_intereses+$c_plus+$c_iva),2,PHP_ROUND_HALF_UP);
  //  '-gastos adm-'.
  $c_gastosAdm=porcentaje($subtotal1,$gastosAdm);//
  //  '-iva gastos adm2 -'.
  $c_ivaGastosAdm=porcentaje($c_gastosAdm,$iva).'--';

  //  'subtotal 2 :'.
  $subtotal2=round(($subtotal1+$c_gastosAdm+$c_ivaGastosAdm),2);

  //  '--Sub total cupon   modificacion del 08-05-2017--'. 
   //echo '</br>';
  $subtotalCupon=round(($coefTarjeta*$subtotal2),2,PHP_ROUND_HALF_UP);
 //echo '</br>';
  //  '--interes tarjeta--'. 
   $interesTarjeta=$subtotalCupon-$subtotal1;
  // 'IVA INTERES TARJETA'
  // echo '</br>';
   $IVA_interesTarjeta=porcentaje($interesTarjeta,$iva);
  // '-- total cupon'
 //  echo '</br>';
  $totalCupon=($subtotal2+$interesTarjeta+$IVA_interesTarjeta); 

  // array para el total del cupon
  $datos[] = array('totalCupon'=>$totalCupon);

}
else echo 'Faltan datos.';

 header('Content-type: application/json; charset=utf-8');
 $json_string = json_encode($datos);
  echo $json_string;
?>