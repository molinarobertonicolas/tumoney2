<?
  include("../tarjeta/tarjeta.php");
  $tabla=$_GET['tabla'];
  $items = tarjeta::listaCuotas($tabla);
  $datos = array(); //creamos un array
  foreach($items as $item)
  {
  $cuota=$item['cuota'];
  $coeficiente=floatval(number_format(($item['coeficiente']), 6, '.', ''));
  $datos[] = array('cuota'=> $cuota, 'coeficiente'=> $coeficiente);
  }
  //Creamos el JSON
  header('Content-type: application/json; charset=utf-8');
  $json_string = json_encode($datos);
  echo $json_string;
?>