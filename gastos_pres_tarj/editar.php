<?php
include("../sesion.php");
include '../menu.php';
include("gastos_prest_tarjeta.php");

if( isset($_GET['id']) && !empty($_GET['id']) )
 {
  $id=(int)$_GET['id'];
  $registros=gastos_prest_tarjeta::obtenerId($id);
   foreach($registros as $veh)
  {
    $id = $veh['id'];
    $item = $veh['item'];
    $valor = $veh['valor'];
  ?>

 <div class="container">
 <h3>Gastos Administrativos</h3>
 <script src="../js/jquery.js"></script>
 <hr>
 <div class="row">
  
 <div class="col-md-8">
 <h4>Editar </h4> 
 <hr>
 <form class="form-horizontal" role="form" method="POST" action="editar.php">
  <input type="hidden" name="idGasto" value="<?echo $id; ?>" />
  
  <div class="col-md-8">
    <label>Gasto</label>
    <input name="item"  class="form-control" type="text" tabindex="1" disabled value="<?echo $veh['item']; ?>" />
  </div>

  <div class="col-md-8">
    <label >Valor</label>
    <input name="valor"  class="form-control" type="text" tabindex="2"  value="<?echo $veh['valor']; ?>" maxlength="25" required />
  </div>

  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>   
</div>
<?
}//fin del while
}// fin del if
if( isset($_POST['idGasto']) && !empty($_POST['idGasto']) )
 {
   $id= $_POST['idGasto'];
   $valor= $_POST['valor'];

  $registros=gastos_prest_tarjeta::editar($id,$valor);

  if($registros){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      //header('Location: listado.php');
      exit;
    }
    else {
    ?>
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div>
    <?
    }
    }
?>