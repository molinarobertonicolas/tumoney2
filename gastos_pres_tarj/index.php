<?php
include("../sesion.php");
include("../menu.php");
include("gastos_prest_tarjeta.php");
?>
 <div class="container">
    <h2>Parametros</h2>
    <p> 
      <a class="btn btn-primary" href="index.php">Gasto Administrativos</a>
      <a class="btn btn-primary" href="../visa/index.php">Coeficiente Visa</a> 
      <a class="btn btn-primary" href="../mastercard/index.php">Coeficiente Mastercard</a>
      <a class="btn btn-primary" href="../americanExpress/index.php">Coef. American Express</a>
      <a class="btn btn-primary" href="../tp/index.php">Coef. TP</a>
       <a class="btn btn-primary" href="../naranja/index.php">Coef. Naranja</a>
    </p>
    <h3>Gastos Administrativos</h3>
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Costos</th>
             <th>Valor (en %) </th>
             <th>Funciones</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $clientes = gastos_prest_tarjeta::lista();
          foreach($clientes as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['item']; ?></td>
              <td><?php echo $item ['valor']; ?></td>
                          
              <td>
                  <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>
                  <!--a class="btn btn-danger btn-sm" href="eliminar.php?id=<?php echo $item ['id'];?>" > Borrar</a-->
              </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>