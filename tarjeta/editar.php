<?
include("../sesion.php");
include("tarjeta.php");
include '../menu.php';

if( isset($_GET['id']) && !empty($_GET['id']) )
 {
  $id=(int)$_GET['id'];
  $registros=tarjeta::obtenerId($id);
  foreach($registros as $veh)
  {
    $id = $veh['id'];
   ?>

 <div class="container">
 <h3>Tarjetas</h3>
 <script src="../js/jquery.js"></script>
 <hr>
 <div class="row">
  
 <div class="col-md-8">
 <h4>Editar Tarjeta</h4> 
 <hr>
 <form class="form-horizontal" role="form" method="POST" action="editar.php">
  <input type="hidden" name="idTarjeta" value="<?echo $id; ?>" />

  <div class="col-md-8">
          <label>Nombre *</label>
          <input name="nombre"  class="form-control" type="text" tabindex="1" value="<?echo utf8_encode($veh['nombre']); ?>" required autofocus/>
        </div>

        <div class="col-md-8">
          <label >Coeficiente *</label>
          <input name="coeficiente" class="form-control" type="text" tabindex="2" maxlength="6" value="<?echo utf8_encode($veh['coeficiente']); ?>" required />
        </div>

        <div class="col-md-8">
          <label >Arancel % *</label>
         <input type="text" class="form-control" id="arancel" name="arancel" tabindex="2" maxlength="8" value="<?echo utf8_encode($veh['arancel']); ?>" required>
      </div>

        <div class="col-md-8">
          <label >Gastos 1</label>
         <input type="text" class="form-control" id="gastos1" name="gastos1" maxlength="8" value="<?echo utf8_encode($veh['gastos1']); ?>" required>
      </div>

      <div class="col-md-8">
          <label >Gastos 2</label>
         <input type="text" class="form-control" id="gastos2" name="gastos2" maxlength="8" value="<?echo utf8_encode($veh['gastos2']); ?>" required>
      </div>

      <div class="col-md-8">
          <label >Gastos 3</label>
         <input type="text" class="form-control" id="gasto3" name="gastos3" maxlength="8" value="<?echo utf8_encode($veh['gastos3']); ?>" required>
      </div>


  

   <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i>Guardar</button>
  </div>
</form>   



</div>



<?
}//fin del while
}// fin del if
if( isset($_POST['idTarjeta']) && !empty($_POST['idTarjeta']) )
 {
  $id= $_POST['idTarjeta'];
  $nombre = $_POST['nombre'];
  $coeficiente= $_POST['coeficiente'];
  $arancel=$_POST['arancel'];
  $gastos1=$_POST['gastos1'];
  $gastos2=$_POST['gastos2'];
  $gastos3=$_POST['gastos3'];

  $registros=tarjeta::editar($id,$nombre,$coeficiente,$arancel,$gastos1,$gastos2,$gastos3);

  if($registros){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      //header('Location: listado.php');
      exit;
    }
    else {
    ?>
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div>
    <?
    }
    }
?>