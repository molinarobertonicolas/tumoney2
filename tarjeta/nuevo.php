<?
include("../sesion.php");
include("tarjeta.php");
include '../menu.php';
$objecto = new Tarjeta();
if( isset($_POST['nombre']) && !empty($_POST['nombre']) )
 {

  $nombre = $_POST['nombre'];
  $coeficiente= $_POST['coeficiente'];
  $arancel=$_POST['arancel'];
  $gastos1=$_POST['gastos1'];
  $gastos2=$_POST['gastos2'];
  $gastos3=$_POST['gastos3'];

  $todobien = $objecto->nuevo($nombre,$coeficiente,$arancel,$gastos1,$gastos2,$gastos3);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }     
}
else
{
?>
 <div class="container">
 <h3>Tarjetas</h3>
 <script src="../js/jquery.js"></script>
 <hr>
 <div class="row">

   <div class="col-md-6">
        <h4>Agregar Tarjeta</h4>
        <hr>
        <form method="POST" role="form" action="nuevo.php">
        
        <div class="col-md-8">
          <label>Nombre *</label>
          <input name="nombre"  class="form-control" type="text" tabindex="1" required autofocus/>
        </div>

        <div class="col-md-8">
          <label >Coeficiente *</label>
             <select class="form-control" name="coeficiente">
          <option value="2">Visa</option>
          <option value="3">Mastercard</option>
          <option value="4">American Express</option>
          <option value="5">TP</option>
          </select>
        </div>

        <div class="col-md-8">
          <label >Arancel % *</label>
         <input type="text" class="form-control" id="arancel" name="arancel" tabindex="2" maxlength="8" required>
      </div>

        <div class="col-md-8">
          <label >Gastos 1</label>
         <input type="text" class="form-control" id="gastos1" name="gastos1" maxlength="8" required>
      </div>

      <div class="col-md-8">
          <label >Gastos 2</label>
         <input type="text" class="form-control" id="gastos2" name="gastos2" maxlength="8" required>
      </div>

      <div class="col-md-8">
          <label >Gastos 3</label>
         <input type="text" class="form-control" id="gasto3" name="gastos3" maxlength="8" required>
      </div>
   
          
      <div class="col-md-8">
        <hr>
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
        </div>
      </form>     
 <?
 }
 ?>             
   