<?
include_once("../bd/conexion.php");
class Tarjeta
{
  public function lista()
  {
    $consulta="SELECT * FROM tarjeta";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


  public function listaCuotas($tabla)
  {
    $consulta="SELECT * FROM ".$tabla." where activo='SI' order by cuota";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function nuevo($nombre,$coeficiente,$arancel,$gastos1,$gastos2,$gastos3)
 	{
	 $sql="INSERT INTO `tarjeta`
            (`nombre`,
             `coeficiente`,
             `arancel`,
             `gastos1`,
             `gastos2`,
             `gastos3`)
              VALUES ('$nombre',
                      '$coeficiente',
                      '$arancel',
                      '$gastos1',
                      '$gastos2',
                      '$gastos3');";
   $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
   return $rs;
	}

	public function editar($id,$nombre,$coeficiente,$arancel,$gastos1,$gastos2,$gastos3)
	{
    $sql="UPDATE `tarjeta`
          SET `id` = '$id',
            `nombre` = '$nombre',
            `coeficiente` = '$coeficiente',
            `arancel` = '$arancel',
            `gastos1` = '$gastos1',
            `gastos2` = '$gastos2',
            `gastos3` = '$gastos3'
             WHERE `id` = '$id';";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
   }

  public function obtenerId($id)
	{
	  $sql="SELECT * FROM tarjeta where id='$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
    }

  public function eliminar($id)
	{
    $sql="DELETE FROM tarjeta WHERE id ='$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

  public function nombreTarjeta($tarjeta)
  {
    $consulta="SELECT nombre FROM tarjeta where id=".$tarjeta;
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  }
?>