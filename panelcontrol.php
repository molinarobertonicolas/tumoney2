<?php
include("sesion.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>Tumoney.net</title>
    <!-- Bootstrap Styles-->
    <!--link href="css/bootstrapFlaty.css" rel="stylesheet" /-->
    <link href="css/bootstrapYeti.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-inverse" role="navigation">
  <div class="container-fluid">

		<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Navegación Toggle</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">AdminSis-Motos</a>
		</div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		  <?
        if ($permiso=='1')
        {
        ?>
          <li>
             <a href="panelcontrol.php">Calculadora</a>
           </li>
         
          <li>
             <a href="cupon/index.php">Cupones de Tarjetas</a>
           </li>
           <li>
             <a href="prestamotarjeta/index.php">Prestamo Tarjeta de Credito</a>
           </li>

          <li>
             <a href="documentacion/index.php">Documentacion</a>
          </li>
          
          <li>
             <a href="pagos/index.php">Pagos</a>
          </li>

            <li>
             <a href="facturab/index.php">Factura B</a>
          </li>
             <li>
             <a href="cliente/index.php">Clientes</a>
           </li>
           <li>
             <a href="tarjeta/index.php">Tajetas de Credito</a>
           </li>
            <!--li>
             <a href="caja/index.php">Caja</a>
           </li-->
             <li>
             <a href="cajanueva/index.php">Caja</a>
           </li> 
           <li>
             <a href="cajadetalle/index.php">Movimientos</a>
           </li> 
           <li>
             <a href="gastos/index.php">Gastos</a>
          </li>
          <li>
             <a href="ventas/index.php">Ventas</a>
          </li>
           <li>
             <a href="gastos_pres_tarj/index.php">Parametros</a>
           </li>
           <li>
             <a href="estadisticas/comollego.php">Encuestas</a>
          </li>
          <li>
             <a href="reportes_motos/index.php">Reportes</a>
          </li>
           <li>
             <a href="usuario/index.php">Usuarios</a>
           </li>
             <li>
             <a href="respaldo/respaldo.php">Copia de Base de Datos</a>
          </li>
                    
            <?
            }
            // permisos del operador
            if ($permiso=='2'  )
            {
            ?>
             <li>
             <a href="panelcontrol.php">Calculadora</a>
           </li>
           
          <!--li>
             <a href="cupon/index.php">Cupones de Tarjetas</a>
           </li-->
          <li>
             <a href="prestamotarjeta/index.php">Prestamo Tarjeta de Credito</a>
           </li>

           <li>
             <a href="cliente/index.php">Clientes</a>
           </li>

           <li>
             <a href="reportes_motos/index.php">Reportes</a>
          </li>
           
            <? }?>
            <li>
             <a href="salir.php">Salir</a>
            </li>
           </ul>

            <ul class="nav navbar-nav navbar-right">
             <li>
               <a href="#">Usuario : <? echo $nombre;?></a>
             </li>
           </ul>
       </div>
     </nav>
  
              
<div class="container">
	<div class="row">
 
   <h3>  Calculadora de Prestamo</h3>			
	 <form action="prestamotarjeta/guardardatos.php" method="POST" id="form_account"> 

      <input type="hidden" name="id_cliente_nuevo" value="" >

      <div class="col-md-8">
         <label >Tarjeta de Credito</label>
         <select id="tarjeta_id" name="tarjeta_id" class="form-control" tabindex="1" autofocus required>
          <option>Seleccionar T...</option>
         </select>
      </div>

      <div class="col-md-8">
         <label >Monto Solicitado</label>
         <input type="number" class="form-control" name="montoprestamo" id="montoprestamo" maxlength="8" tabindex="2" required>
      </div>

       <div class="col-md-8">
          <label >Cantidad de Cuotas</label>
         <select id="cuotas" class="form-control" tabindex="3"  required>
          <option>Seleccionar T...</option>
        </select>
       </div>
   
      <div class="col-md-8">
         <label >Monto total a Pagar </label>
         <input type="text" class="form-control" id="monto_apagar" name="monto_apagar" tabindex="4">
      </div>

      <div class="col-md-8">
          <label > Monto por Cuotas</label>
         <input type="text" class="form-control" id="monto_cuota" name="monto_cuota" tabindex="5">
      </div>

      <div class="col-md-8">
        <label>Nº Cupon *</label>
        <input name="numero_cupon" class="form-control" type="text" tabindex="6" required />
      </div>

   <!--div class="col-md-8">
    <label>Ingresar Nº de Cuotas </label>
    <input name="cant_cuotas" id="cant_cuotas" min="1" max="50" class="form-control" type="hidden" tabindex="7" maxlength="2" required />
  </div-->


  <input name="cant_cuotas" id="cant_cuotas" type="hidden" />

   <div class="col-md-8">
    <label>4 ultimos digitos de la Tarjeta</label>
    <input name="digitos"  class="form-control" type="text" tabindex="6" maxlength="4"  tabindex="8" required />
  </div>


  <div class="col-md-8">
    <label>D.N.I *</label>
    <input name="dni" id="dni"  class="form-control" type="text" tabindex="7" maxlength="8" tabindex="9" required />
    <br>
    <button id="buscar_dni">Buscar D.N.I.</button>
    <div id="resultadoBusqueda"></div>
  </div>
  

 <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      
      <button type="submit" class="btn btn-primary pull-right" tabindex="10" ><i class="fa fa-floppy-o"></i> Guardar Cupon </button>
  </div>
</form>

</div>
</div>

 <!-- ventana modal-->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Formulario Agregar Cliente</h4>
          </div>
          <div  class="modal-body">
            <form id="form_cliente" method="post">

            <div class="col-md-8">
              <label>D.N.I *</label>
              <input name="dni2" id="dni2"  class="form-control" type="text"  maxlength="8" required autofocus />
            </div>
            
            <div class="col-md-8">
              <label>Nombre *</label>
              <input name="nombre" id="nombre"  class="form-control" type="text" maxlength="80" required />
            </div>

             <div class="col-md-8">
              <label>CUIT *</label>
              <input name="cuit" id="cuit" class="form-control" type="text"  maxlength="15" required />
            </div>

            <div class="col-md-8">
              <label>Domicilio *</label>
              <input name="domicilio" id="domicilio" class="form-control" type="text"  maxlength="90" required />
            </div>
            
             <div class="col-md-8">
              <label >Departamento *</label>
              
                  <select class="form-control" name="departamento" id="departamento" required>
                    <option value="ALBARDON">ALBARDON</option>
                    <option value="ANGACO">ANGACO</option>
                    <option value="CALINGASTA">CALINGASTA</option>
                    <option value="CAPITAL">CAPITAL</option>
                   <option value="CAUCETE">CAUCETE</option>
                   <option value="CHIMBAS">CHIMBAS</option>
                   <option value="IGLESIA">IGLESIA</option>
                   <option value="JACHAL">JACHAL</option>
                   <option value="9 DE JULIO">9 DE JULIO</option>
                    <option value="POCITO">POCITO</option>
                     <option value="RAWSON">RAWSON</option>
                      <option value="RIVADAVIA">RIVADAVIA</option>
                       <option value="SAN MARTIN">SAN MARTIN</option>
                        <option value="SANTA LUCIA">SANTA LUCIA</option>
                         <option value="SARMIENTO">SARMIENTO</option>
                          <option value="ULLUM">ULLUM</option>
                           <option value="VALLE FERTIL">VALLE FERTIL</option>
                           <option value="25 DE MAYO">25 DE MAYO</option>
                           <option value="ZONDA">ZONDA</option>
                         <option value="Otra Provincia">Otra Provincia</option>
                  </select>
           
               
            </div>

             <div class="col-md-8">
              <label >Email </label>
              <input type="text" class="form-control" name="email" id="email" maxlength="35"  >
           </div>

            <div class="col-md-8">
              <label >Telefono *</label>
              <input type="text" class="form-control" name="telefono" id="telefono" maxlength="15"  required>
           </div>
            
            <div class="col-md-8">
            
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='panelcontrol.php';"><i class="fa fa-times"></i> Cancelar</button>
                <button id='botonGuardarCliente' class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar Cliente</button>
            </div>
          </form> 
          </div>
          <div class="modal-footer">
            <!--button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button-->
          </div>
        </div>

      </div>
    </div>
    <!-- fin-->

<script src="js/jquery-1.10.2.js"></script> 
<script type="text/javascript">
 $(document).ready(function()
  {
    
     listadoTarjetas();
        
    $("#tarjeta_id").change(function()
     {
        var vtabla = $("#tarjeta_id").val();   
        //alert(vtabla);
        listadoCuotas(vtabla);
     })//fin

   
    $("#cuotas").change(function()
     {
        var vtarjeta_id = $("#tarjeta_id").val();   
        //alert(vtarjeta_id);
        var vmontoprestamo = $("#montoprestamo").val();  
        var vcoe = $("#cuotas").val(); 
        var vcantidad = $("#cuotas option:selected").html();
       // alert(vcantidad);    
        $.ajax({
          data: {"tarjeta_id" :vtarjeta_id ,"montoprestamo":vmontoprestamo ,"coeficiente":vcoe},
          type: "POST",
          url: "calculadora/calcularPrestamo.php",
        })
       .done(function( data, textStatus, jqXHR ) {
           if ( console && console.log ) {
               console.log( "Generacion es correcta." );
           }
           $.each(data, function(i,datos){
                   document.getElementById("monto_apagar").value=datos.totalCupon;
                   document.getElementById("monto_cuota").value=redondeo((datos.totalCupon/vcantidad),2);

                    document.getElementById("cant_cuotas").value=vcantidad;
                   
                    //alert(datos.totalCupon);     
                    });
       })
       .fail(function( jqXHR, textStatus, errorThrown ) {
           if ( console && console.log ) {
               console.log( "La solicitud a fallado de Generar: " +  textStatus);
           }
      });
      });//fin cuotas

    
    // buscar por dni 09/08/2018
      $('#buscar_dni').click(function(){
          event.preventDefault();
          var vdni = $("#dni").val();
          //alert(vdni);
          console.log(vdni);
          if (vdni != "") {
              $.post("cliente/buscar_dni.php", {dni: vdni}, function(mensaje) {
                  $("#resultadoBusqueda").html(mensaje);
              }); 
          } else { 
                 ("#resultadoBusqueda").html('sin Datos.');
                 }
      });// fin buscar

      // guardar cliente 31/07/2018
      $('#botonGuardarCliente').click(function(){
           event.preventDefault();
          var vdni = $("#dni2").val();
          //alert(vdni);
          var vnombre = $("#nombre").val();
          var vcuit = $("#cuit").val();
          var vdomicilio = $("#domicilio").val();
          var vdepartamento = $("#departamento").val();
          var vemail = $("#email").val();
          var vtelefono = $("#telefono").val();
          $.post("cliente/insertar_cliente.php", {dni: vdni, nombre: vnombre,cuit: vcuit, domicilio: vdomicilio, departamento: vdepartamento,email:vemail, telefono:vtelefono }, function(mensaje) {
                  //alert(mensaje);
                  $("#id_cliente_nuevo").val(mensaje);
                   $.post("cliente/buscar_dni.php", {dni: vdni}, function(mensaje)
                   {
                      $("#resultadoBusqueda").html(mensaje);
                      $("#myModal").hide();
                      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
                      $("#buscar_dni").hide(); //ocultamos el boton buscar dni
                   }); 
              });
       });// fin buscar
 
  }) //fin jquery   

  //17012017 listado de tarjetas para el select recibe un json
  function listadoTarjetas(){
    $.getJSON('calculadora/listaTarjetas.php',function(data){
          $.each(data, function(i,tarjeta){
            $("#tarjeta_id").append('<option value="'+tarjeta.coeficiente+'">'+tarjeta.nombre+'</option>');
          });
    });
   };

   function listadoCuotas(vtabla){
    $.getJSON('calculadora/listaCuotas.php',{ tabla: vtabla},function(data){
          $.each(data, function(i,cuotas){
            $("#cuotas").append('<option value="'+cuotas.coeficiente+'">'+cuotas.cuota+'</option>');
          });
    });
   };

   function redondeo(numero, decimales)
    {
    var flotante = parseFloat(numero);
    var resultado = Math.round(flotante*Math.pow(10,decimales))/Math.pow(10,decimales);
    return resultado;
    };
//16062017 listado de tarjetas para el select recibe un json
  function mostrarSaldo(){
   $.ajax({
            url: 'cajadetalle/saldo.php',
            success: function(data) {
                $('#saldo').html(data);
            }
        });
   };
</script>
</body>
</html>