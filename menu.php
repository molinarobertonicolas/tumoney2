<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>Tumoney.net</title>
    <!-- Bootstrap Styles-->
    <!--link href="css/bootstrapFlaty.css" rel="stylesheet" /-->
    <link href="../css/bootstrapYeti.css" rel="stylesheet" />

    <!-- FontAwesome Styles-->
    <link href="../css/font-awesome.css" rel="stylesheet" />
    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
</head>
<body>
<nav class="navbar navbar-default navbar-inverse" role="navigation">
  <div class="container-fluid">

		<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Navegación Toggle</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="../panelcontrol.php">AdminSis-Motos</a>
		</div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		  <?
        if ($permiso=='1')
        {
        ?>
           <li>
             <a href="../panelcontrol.php">Calculadora</a>
           </li>
           
          <li>
             <a href="../cupon/index.php">Cupones de Tarjetas</a>
          </li>
          <li>
             <a href="../prestamotarjeta/index.php">Prestamo Tarjeta de Credito</a>
          </li>
          <li>
             <a href="../documentacion/index.php">Documentacion</a>
          </li>
          <li>
             <a href="../pagos/index.php">Pagos</a>
          </li>
          <li>
             <a href="../facturab/index.php">Factura B</a>
          </li>
          <li>
             <a href="../cliente/index.php">Clientes</a>
          </li>
          <li>
             <a href="../tarjeta/index.php">Tajetas de Credito</a>
          </li>
            <!--li>
             <a href="../caja/index.php">Caja</a>
           </li-->
          <li>
             <a href="../cajanueva/index.php">Caja</a>
          </li> 

          <li>
             <a href="../cajadetalle/index.php">Movimientos</a>
          </li> 

           <li>
             <a href="../gastos/index.php">Gastos</a>
          </li> 
           <li>
             <a href="../ventas/index.php">Ventas</a>
          </li> 

          <li>
             <a href="../gastos_pres_tarj/index.php">Parametros</a>
          </li>
           <li>
             <a href="../estadisticas/comollego.php">Encuestas</a>
          </li> 
          <li>
             <a href="../reportes_motos/index.php">Reportes</a>
          </li>
          <li>
             <a href="../usuario/index.php">Usuarios</a>
          </li>
          <li>
             <a href="../respaldo/respaldo.php">Copia de Base de Datos</a>
          </li>
                    
            <?
            }
            // permisos del operador
            if ($permiso=='2'  )
            {
            ?>
            <li>
             <a href="../panelcontrol.php">Calculadora</a>
           </li>
            
            <!--li>
             <a href="../cupon/index.php">Cupones de Tarjetas</a>
            </li-->
            <li>
             <a href="../prestamotarjeta/index.php">Prestamo Tarjeta de Credito</a>
            </li>

            <li>
             <a href="../cliente/index.php">Clientes</a>
          </li>


                       
            <li>
             <a href="../reportes_motos/index.php">Reportes</a>
          </li>
           
           <?
            }
            ?>
           <li>
             <a href="../salir.php">Salir</a>
           </li>
           </ul>

            <ul class="nav navbar-nav navbar-right">
             <li>
               <a href="#">Usuario : <? echo $nombre;?></a>
             </li>
            </ul>
       </div>
     </nav>