<?php
include("../sesion.php");
include("../menu.php");
include("naranja.php");
?>
 <div class="container">
    <h2>Parametros</h2>

  <p> 
      <a class="btn btn-primary" href="../gastos_pres_tarj/index.php">Gasto Administrativos</a>
      <a class="btn btn-primary" href="../visa/index.php">Coeficiente Visa</a> 
      <a class="btn btn-primary" href="index.php">Coeficiente Mastercard</a>
      <a class="btn btn-primary" href="../americanExpress/index.php">coef. American Express</a>
      <a class="btn btn-primary" href="../tp/index.php">Coef. TP</a>
      <a class="btn btn-primary" href="../naranja/index.php">Coef. Naranja</a>
    </p>
        <h3>Coeficientes Naranja</h3>

        <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Id</th>
             <th>Cuota</th>
             <th>Coeficiente</th>
              <th>Funciones</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objeto = new Naranja();
          $usuarios = $objeto->lista();
          foreach($usuarios as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['cuota']; ?></td>
              <td><?php echo $item ['coeficiente']; ?></td>
              <td>
                  <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>
             </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin
 });
</script>
</body>
</html>