<?
include("../sesion.php");
include("../menu.php");
include("prestamotarjeta.php");
if( isset($_POST['cliente_id']) && !empty($_POST['cliente_id']) )
 {

  $cliente_id = $_POST['cliente_id'];
  $tarjeta_id = $_POST['tarjeta_id'];
  $cupon_id = $_POST['cupon_id'];
  $fechahora = date("Y-m-d H:i:s");
  $pdf_dni =0;// $_POST['pdf_dni'];
  $pdf_tarjeta =0;// $_POST['pdf_tarjeta'];
  $pdf_recibosueldo =0;// $_POST['pdf_recibosueldo'];
  $montoprestamo = $_POST['montoprestamo'];
  $monto_apagar = $_POST['monto_apagar'];
$objecto = new PrestamoTarjeta();
  $todobien = $objecto->nuevo($cliente_id,$tarjeta_id,$cupon_id,$fechahora,$pdf_dni,$pdf_tarjeta,
                      $pdf_recibosueldo,$montoprestamo,$monto_apagar);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }     
}
else
{
?>
 <div class="container">
 <h3>Prestamos Tarjeta de Credito</h3>
 <hr>
 <div class="row">
 <div class="col-md-6">
  
 <h4>Generar Prestamo</h4>
 <hr>

  <div class="col-md-8">
    <label>DNI</label>
    <input id="dni" name="dni" class="form-control" type="text" tabindex="1" required />
   
    <button id="buscarDNI" class="btn btn-primary pull-right"> Buscar</button> <br>
  </div>


   <input type="hidden" name="cliente_id" id="cliente_id">

  <div class="col-md-8">
    <label>Nombre de Cliente*</label>
    <input id="nombre" name="nombre" class="form-control" type="text" tabindex="1" required  />
  </div>

  <div class="col-md-8">
    <label >Tarjeta de Credito</label>
   <select id="tarjeta_id" name="tarjeta_id" class="form-control" required>
    <option>Seleccionar T...</option>
  </select>
  </div>

  <div class="col-md-8">
    <label >Monto Solicitado</label>
   <input type="number" class="form-control" name="montoprestamo" id="montoprestamo" maxlength="8" required>
</div>

<div class="col-md-8">
     <button id="generarPrestamo" class="btn btn-primary pull-right">Generar </button>
</div>

<div class="col-md-8">
   <label >Monto a Pagar </label>
   <input type="number" class="form-control" id="monto_apagar" name="monto_apagar">
</div>

<div class="col-md-8">
    <label >Nº de Cupon</label>
   <input type="number" class="form-control" id="cupon_id" name="cupon_id" maxlength="8" >
</div>
    
<div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="button" id="botonConfirmar" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Confirmar  Prestamo</button>
  </div>
</div><!--row-->
</div>   
 <?
 }
 ?>             
 
 <script src="../js/jquery-1.10.2.js"></script> 
 <script src="../js/jquery.maskedinput.js" type="text/javascript"></script>
 <script type="text/javascript">
 
 $(document).ready(function()
  {
   
    listadoTarjetas();

    $("#dni").mask("99999999");
    // 16012017 buscar por dni
    $("#buscarDNI").click(function()
     {
        var vdni = $("#dni").val();   
        if(vdni != '')
        {
         $.ajax({
                  data: {"dni" : vdni},
                  type: "POST",
                  dataType: "json",
                  url: "traerDatosCliente.php",
                })
               .done(function( data, textStatus, jqXHR ) {
                   if ( console && console.log ) {
                       console.log( "La solicitud se ha completado correctamente." );
                   }
                   $.each(data, function(i,cliente){
                    document.getElementById("cliente_id").value=cliente.id;
                         //alert(cliente.nombre);
                        document.getElementById("nombre").value=cliente.nombre;
                    });
               })
               .fail(function( jqXHR, textStatus, errorThrown ) {
                   if ( console && console.log ) {
                       console.log( "La solicitud a fallado: " +  textStatus);
                       alert('Dni no existe.')
                   }
              });
           
        }
        else alert('Falta el dni');     
       
      });//fin buscarDNI

    

     $("#generarPrestamo").click(function()
     {
        var vtarjeta_id = $("#tarjeta_id").val();   
        //alert(vtarjeta_id);
        var vmontoprestamo = $("#montoprestamo").val();   
        //alert(vmontoprestamo);    
        $.ajax({
          data: {"tarjeta_id" :vtarjeta_id ,"montoprestamo":vmontoprestamo},
          type: "POST",
          url: "generarPrestamo.php",
        })
       .done(function( data, textStatus, jqXHR ) {
           if ( console && console.log ) {
               console.log( "Generacion es correcta." );
           }
           $.each(data, function(i,datos){
                    document.getElementById("monto_apagar").value=datos.totalCupon;
                    //alert(datos.totalCupon);     
                    });
      

       })
       .fail(function( jqXHR, textStatus, errorThrown ) {
           if ( console && console.log ) {
               console.log( "La solicitud a fallado de Generar: " +  textStatus);
           }
      });
      });//fin buscarDNI

     //boton confirmar el prestamo inserta en la base de datos
     // 16012017 buscar por dni
    $("#botonConfirmar").click(function()
     {
        var vcliente_id = $("#cliente_id").val(); 
        var vtarjeta_id = $("#tarjeta_id").val();
        var vmontoprestamo = parseFloat( $("#montoprestamo").val());
        var vmonto_apagar = parseFloat($("#monto_apagar").val());
        var vcupon_id = $("#cupon_id").val();
    
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'nuevo.php',
          data: { cliente_id:vcliente_id, tarjeta_id:vtarjeta_id,montoprestamo:vmontoprestamo,monto_apagar:vmonto_apagar,cupon_id:vcupon_id},
          success: function(data){

            if (data)
            {
             alert('Operacion Terminada');
             window.location.href = "index.php";
            }
        }
        });//fin ajax    
       
      });//fin buscarDNI
 
  }) //fin jquery   

  //17012017 listado de tarjetas para el select recibe un json
  function listadoTarjetas(){
    $.getJSON('listaTarjetas.php',function(data){
          $.each(data, function(i,tarjeta){
      $("#tarjeta_id").append('<option value="'+tarjeta.id+'">'+tarjeta.nombre+'</option>');
        });
    });
   };
           
</script>
</body>
</html>