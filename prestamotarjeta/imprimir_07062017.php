<?php
include("../sesion.php");
include("../menu.php");
include("prestamotarjeta.php");

if( isset($_GET['cupon_id']) && !empty($_GET['cupon_id']) )
{
 //gastos generaloes y administrativode los prestamos
   $listados = prestamotarjeta::traerGastosPrestamo();
    foreach($listados as $item)
    {
  	//$gastosPrestamo[ ] = array('nombre'=>$item['item'], 'valor'=>$item['valor']);
     switch ($item['id']) 
            {
            case 1:
                $interes=$item ['valor'];
            case 2:
               $iva=$item ['valor'];
            case 3:
               $plus=$item ['valor'];
            case 4:
               $gastosAdm=$item ['valor'];
            case 5:
               $sello=$item ['valor'];   
            }
    }        

 //datos pretamo 
  $cupon_id=$_GET['cupon_id'];
  $datosPrestamos = prestamotarjeta::datosPrestamos($cupon_id);
  foreach($datosPrestamos as $item)
  {
  
?>

<style type="text/css" media="print">

@media print {

  body { 
         font-size:20px;
         }

         table {
    font-size: 14px;
}

#noimprimir {display:none;}

#parte2 {display:none;}

}

</style>

<div id="seleccion">


 <div class="container">
 
 <h3 class="text-right">
 SAMY S.R.L.
</h3>
<p class="text-right">
(Servicio Financiero-Fondos Propios)
</p>
</br>

<p class="text-left">
SOLICITUD DE PRESTAMO – PAGO CON TARJETA.-
</p>
</br>

<p class="text-right">
Lugar y Fecha San Juan <? echo date("d-m-Y");?>
</p>
</br>
</br>
<p class="text-left">
Señores SAMY S.R.L.
</p>
</br>
</br>
<p class="text-justify">
Por la presente, El/La Sr./a: <b><?php echo $item ['nombre']; ?></b>
CUIT: <b><?php echo $item ['cuit']; ?></b>,
 domiciliado en <b><?php echo $item ['calle'].' '.$item ['departamento']; ?></b>(el “cliente”) solicita a SAMY S.R.L (“prestadora” ), la suma de <b>($ <?php echo $SUMA=$item ['montoprestamo']+$item ['plus']; ?>)</b>.
  2)El préstamo se encuentra sujeto a las siguientes condiciones, que el cliente acepta y autoriza en este acto: Comisión, Intereses, Gastos e Impuestos, a razón de: CFT (50%) TNA(48%), cualquier otro impuesto y tasa que grave este tipo de operaciones correrán a cargo del Cliente.  3) El Cliente abona el crédito solicitado con tarjeta de Credito:<b><?php echo $item ['nombretarjeta']; ?></b>, en <b><?php echo $item ['cuotas']; ?></b> cuotas, cupón N° <b> <?php echo $item ['numeroCupon']; ?></b>, por un Importe total de pesos (<b>$ <?php echo $item ['totalcupon']; ?></b>).- 4)En caso que la tarjeta de crédito no efectué el pago deberá hacerlo el Cliente. 5) Fondos Propios: Se deja expresamente aclarado que el presente se efectúa con fondos propios de la prestadora. 6) A todos los efectos derivados de la presente, el Cliente constituye domicilio especial el denunciado en la presente y se somete a la jurisdicción de los Tribunales Ordinarios de la Ciudad de San Juan, o la que corresponda al domicilio del Cliente, a exclusivo criterio del prestador, con exclusión de todo otro fuero o jurisdicción.Se imprime un ejemplar para cada parte.
</p>
</br>
</br>
<p class="text-left">
RECIBE conforme el dinero solicitado: _______________________________________________________ .-
</p>
</br>
</br>
</br>
</br>
</br>
<hr>
</br>
</br>
</br>
</br>
<p class="text-right">
Son __________________ .-
</p>
</br>
<p class="text-right">
Lugar y Fecha: San Juan, _____ de _________ del año 201__.-
</p>
</br>
<p class="text-justify">
El día _____ de ________del año 201__, pagaré sin protesto a ___________________, la cantidad de pesos ___________, por igual valor recibido a mi/nuestra satisfacción en ________. De conformidad con lo dispuesto por los artículos 36 y concordantes del Decreto-Ley 5965/63, se deja ampliado el plazo de presentación de este pagaré por un plazo de TRES (3) años computados a partir del día de la fecha (fecha de emisión). A partir del vencimiento, el importe del presente, devengara un interés del  0,33 % diario a favor de su portador. Pagadero en calle ________________,_________, Ciudad, San Juan.- 
</p>
</br>
</br>
<table class="table">
  <tr>
    <th>Firma</th>
    <th>Aclaración</th>
    <th>Tipo/Nº Doc.</th>
    <th>Domicilio</th>
  </tr>
</table>

<!--p>
_________ _______________ _______________ __________________

_________              _______________                _______________                    __________________
   Firma:                          Aclaración:                          Tipo/Nº Doc.:                                 Domicilio:        

    </p-->
<?
}
}
?>
    <div id='noimprimir'>
<p>
    
    <a class="btn btn-primary" href="index.php">Cerrar</a>
    <a href="javascript:window.print()" class="btn btn-primary"> Imprimir</a>
    </p>
    </div>
</div>

</div>