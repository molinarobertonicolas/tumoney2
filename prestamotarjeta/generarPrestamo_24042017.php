<?
/*26/03/2017  agregar el subir archivo pdf*/
include("prestamotarjeta.php");

function porcentaje($cantidad,$porciento)
{
 //sin separador de miles y dos decimales separados por el .
 return number_format($cantidad*$porciento/100 ,2,".", "");
}



if( isset($_POST['cupon_id']) && !empty($_POST['cupon_id']) )
{
    //26-03-2017 subir archivo
      $status = "";
      if ($_POST["action"] == "upload")
      {
         // obtenemos los datos del archivo
         $tamano = $_FILES["archivo"]['size'];
         $tipo = $_FILES["archivo"]['type'];
         $archivo = $_FILES["archivo"]['name'];

         if ($archivo != "")
         {
           // guardamos el archivo a la carpeta files
           $destino =  "../dni/".$archivo;

           if (copy($_FILES['archivo']['tmp_name'],$destino))
           {
           
          // nombre modificado del archivo
          $nuevo_name='dni_'.$archivo;
          rename("../dni/".$archivo,"../dni/".$nuevo_name);
          //fin del renombrar
             $status = "Archivo subido: <b>".$nuevo_name."</b>";
         }
         else {
                  $status  = "Error al Copiar el Archivo";
                  echo '<script> console.log("Error al Copiar el Archivo") </script>';
                          }
        }//fin del if del subir archivo
        else
        {
           $status  = "Achivo esta vacio";
              echo '<script> console.log("Archivo esta vacio.") </script>';
                }

      }//fin del subir archivo
    //echo $status;
    //exit();
            
 
    //fin subir archivo
    $cupon_id=$_POST['cupon_id'];
    $montoprestamo=$_POST['montoprestamo'];
    $pdf_dni=$nuevo_name;
    $fechahora=date("Y-m-d H:i:s");

    //modificar el estado de los cupones
     $estadoCupon = prestamotarjeta::estadoCupon($cupon_id);

    //buscar datos cupon
    $datosCupon = prestamotarjeta::buscarCupon($cupon_id);
    foreach($datosCupon as $item)
    {
       $tarjeta_id=$item ['tarjeta_id'];
       $cuotas=$item ['cuotas'];
       $cliente_id=$item ['cliente_id'];
       $numero_cupon=$item ['numero_cupon'];
    }

    //buscar datos coeficiente por cuotas segun tarjeta de credito
    $coefTarjeta = prestamotarjeta::buscarCoeficiente($tarjeta_id,$cuotas);
  
   // buscar gastos administrativos
    $listados = prestamotarjeta::traerGastosPrestamo();
    foreach($listados as $item)
    {
  	//$gastosPrestamo[ ] = array('nombre'=>$item['item'], 'valor'=>$item['valor']);
     switch ($item['id']) 
            {
            case 1:
                $interes=$item ['valor'];
            case 2:
               $iva=$item ['valor'];
            case 3:
               $plus=$item ['valor'];
            case 4:
               $gastosAdm=$item ['valor'];
            case 5:
               $sello=$item ['valor'];   
            }
    }        
  //calcular los gastos del prestamo
  //  $interes.'--';
  //  $iva.'--';
  //  $plus.'--';
  //  $gastosAdm.'--';
  //  $sello.'--';
  //  $montoprestamo.'---';
  // '<br>';
  //porcentaje($cantidad,$porciento)
  //  '-intereses-'.
  $c_intereses=porcentaje($montoprestamo,$interes);
  //  '-iva-'.
  $c_iva=porcentaje($c_intereses,$iva);
  //  '-plus-'.
  $c_plus=porcentaje($montoprestamo,$plus);
  // '<br>';
  // subtotal 1
  //  'subtotal 1 :'.
  $subtotal1=round(($montoprestamo+$c_intereses+$c_plus+$c_iva),2,PHP_ROUND_HALF_UP);
  //  '-gastos adm-'.
  $c_gastosAdm=porcentaje($subtotal1,$gastosAdm);//
  //  '-iva gastos adm2 -'.
  $c_ivaGastosAdm=porcentaje($c_gastosAdm,$iva).'--';

  //  'subtotal 2 :'.
  $subtotal2=round(($subtotal1+$c_gastosAdm+$c_ivaGastosAdm),2);

  //  '--cuotas --'.
  //$cuotas=12;

  //  '--coef tarjeta-- '.  $coefTarjeta=1.2999;

  //  '--total cupon--'. 
 $totalCupon=round(($coefTarjeta*$subtotal2),2,PHP_ROUND_HALF_UP);

  //  '--interes tarjeta--'. 
  $interesTarjeta=$totalCupon-$subtotal1;
  
  // '<br>';
  // '---Cobranza--';
  // '<br>';
  //  '--arancel tarjeta--'. 
  $arancelTarjeta=3;
  //  '--arancel tarjeta--'. 
  $c_arancelTarjeta=porcentaje($totalCupon,$arancelTarjeta);
  //  '-iva arancel Targeta -'. 
  $c_ivaArancelTarjeta=porcentaje($c_arancelTarjeta,$iva);
  //  'subtotal 3 
  $subtotal3=($totalCupon-$c_arancelTarjeta-$c_ivaArancelTarjeta);
  // '<br>';
  //  '--interes tarjeta--'.$interesTarjeta;
  //  '-iva Interes Tarjeta -'. 
  $c_ivaInteresTarjeta=porcentaje($interesTarjeta,$iva).'--';
  //  '--Gastos tarjeta--'. 
  $gastosTarjeta=0;
  //  '--retenciones ii bb--'. 
 $retenciones_iibb=2.5;
  //  '--retenciones_iibb--'. 
  $c_retenciones_iibb=porcentaje($subtotal3,$retenciones_iibb);
  //  '--retenciones lote hogar--'. 
  $retenciones_lh=20;
  //  '--retenciones_lh--'. 
  $c_retenciones_lh=porcentaje($c_retenciones_iibb,$retenciones_lh);
  //  '--retenciones iva--'. 
  $retenciones_iva=3;
  //  '--retenciones_iva--'. 
  $c_retenciones_iva=porcentaje($subtotal3,$retenciones_iva);
  //  '--retenciones ganancias--'. 
  $retenciones_ganancia=1;
  //  '--retenciones_ganancias--'. 
  $c_retenciones_ganancia=porcentaje($subtotal3,$retenciones_ganancia);
  // '<br>';
  //  '-- a cobrar--'. 
  $a_cobrar=($subtotal3-$interesTarjeta-$c_ivaInteresTarjeta-$gastosTarjeta-$c_retenciones_iibb-$c_retenciones_lh-$c_retenciones_iva-$c_retenciones_ganancia);
 //  '--sello--'. 
  $sellos=1.50;
 //  '--Sellos --'. 
  $c_sellos=porcentaje($a_cobrar,$sellos);
 //  '--Adicional lote hogar--'. 
  $adicional_lote_hogar=20;
  //  '--retenciones_iva--'. 
  $c_adicional_lote_hogar=porcentaje($c_sellos,$adicional_lote_hogar);
  // '<br>';
//  '-- total Neto--'. 
  $total_neto=($a_cobrar-$c_sellos-$c_adicional_lote_hogar);
//  echo $total_neto;
  //$datos[] = array('totalCupon'=> $totalCupon);

  //insertar el prestamo
$objecto = new PrestamoTarjeta();
  $todobien = $objecto->nuevo($cupon_id,$fechahora,$pdf_dni,$montoprestamo,$total_neto);
  if($todobien){
      echo 'se inserto';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar Cobranza ...
         </div> 
    <?
    }
$objecto = new PrestamoTarjeta();
  $prestamo_id = $objecto->ultimoid();
  if($prestamo_id){
      echo 'ultimoid '.$prestamo_id;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ultimo id ...
         </div> 
    <?
    }

  //inserta en la tabla cobranza
        //variable que faltan
        //$prestamo_id=1;
  $objecto = new PrestamoTarjeta();
  $todobien = $objecto->nuevoCobranza($prestamo_id, 
  $totalCupon, 
  $c_arancelTarjeta, 
  $c_ivaArancelTarjeta, 
  $subtotal1, 
  $interesTarjeta, 
  $c_ivaInteresTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $c_retenciones_iibb, 
  $c_retenciones_lh, 
  $c_retenciones_iva, 
  $c_retenciones_ganancia, 
  $a_cobrar, 
  $c_sellos, 
  $c_adicional_lote_hogar, 
  $total_neto);
  if($todobien){
      echo 'se inserto';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar Cobranza ...
         </div> 
    <?
    }
  
 $plus=$c_plus;

$objecto = new PrestamoTarjeta();
  $todobien = $objecto->nuevoDetallePrestamo($prestamo_id, 
  $totalCupon, 
  $c_intereses, 
  $c_iva, 
  $plus, 
  $subtotal1, 
  $c_gastosAdm, 
  $c_ivaGastosAdm, 
  $subtotal2, 
  $cuotas, 
  $coefTarjeta, 
  $totalCupon, 
  $interesTarjeta);
  if($todobien){
      echo 'se inserto';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar  Detalle...
         </div> 
    <?
    }

/*
 header('Content-type: application/json; charset=utf-8');
 $json_string = json_encode($datos);
 echo $json_string;
*/

 /*modificar la caja*/

 
echo'<script type="text/javascript">
window.location="imprimir.php?cupon_id='.$cupon_id.'"</script>';

    

}
  else echo'Faltan datos.';


?>