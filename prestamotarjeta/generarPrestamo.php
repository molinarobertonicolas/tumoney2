﻿<?php
/**24-02-2020 se modifica el monto total sin el plus en las cuotas **/
/**28102018 ultima modificacion**/
/**** 11/05/2017 se agrega la factura b****/
/**** 14/08/2018 se agrega la generacion de cuotas del prestamo****/
/**** 28/10/2018 se agrega la generacion de cuotas del prestamo sacando los datos de la factura b ****/
/** 6/8/2019 se agrega el codigo para los nombres de los dni **/
include("../sesion.php");
include("prestamotarjeta.php");
include("../reportes/cuota_prestamo.php");
function porcentaje($cantidad,$porciento)
{
 //sin separador de miles y dos decimales separados por el .
 return number_format($cantidad*$porciento/100 ,2,".", "");
}

if( isset($_POST['cupon_id']) && !empty($_POST['cupon_id']) )
{
    //26-03-2017 subir archivo
      $status = "";
      if ($_POST["action"] == "upload")
      {
         // obtenemos los datos del archivo
         $tamano = $_FILES["archivo"]['size'];
         $tipo = $_FILES["archivo"]['type'];
         $archivo = $_FILES["archivo"]['name'];

         if ($archivo != "")
         {
           // guardamos el archivo a la carpeta files
           $destino =  "../dni/".$archivo;

           if (copy($_FILES['archivo']['tmp_name'],$destino))
           {

             // renombrar archivo subido
                $long = 8; // la longitud del rand 
                $nuevo_rand = ''; 
                for($i=0;$i<=$long;$i++){ 
                $nuevo_rand .= rand(0,9); 
                }
                //subcadena
                $subcadena = substr ($archivo,-5);
            
           
          // nombre modificado del archivo
          $nuevo_name='dni_'.$nuevo_rand.$subcadena; 
         
          rename("../dni/".$archivo,"../dni/".$nuevo_name);
          //fin del renombrar
             $status = "Archivo subido: <b>".$nuevo_name."</b>";
         }
         else {
                  $status  = "Error al Copiar el Archivo";
                  echo '<script> console.log("Error al Copiar el Archivo") </script>';
                  exit();
                          }
        }//fin del if del subir archivo
        else
        {
           $status  = "Achivo esta vacio";
              echo '<script> console.log("Archivo esta vacio.") </script>';
              exit();
                }

      }//fin del subir archivo
              
 
    //fin subir archivo
    $cupon_id=$_POST['cupon_id'];
    $montoprestamo=$_POST['montoprestamo'];
    $pdf_dni=$nuevo_name;
    $fechahora=date("Y-m-d H:i:s");

    //modificar el estado de los cupones
     $estadoCupon = prestamotarjeta::estadoCupon($cupon_id);

    //buscar datos cupon
    $datosCupon = prestamotarjeta::buscarCupon($cupon_id);
    foreach($datosCupon as $item)
    {
       $tarjeta_id=$item ['tarjeta_id'];
       $cuotas=$item ['cuotas'];
       $cliente_id=$item ['cliente_id'];
       $numero_cupon=$item ['numero_cupon'];
    }

    //buscar datos coeficiente por cuotas segun tarjeta de credito
    $coefTarjeta = prestamotarjeta::buscarCoeficiente($tarjeta_id,$cuotas);
  
   // buscar gastos administrativos
    $listados = prestamotarjeta::traerGastosPrestamo();
    foreach($listados as $item)
    {
  	//$gastosPrestamo[ ] = array('nombre'=>$item['item'], 'valor'=>$item['valor']);
     switch ($item['id']) 
            {
            case 1:
                $interes=$item ['valor'];
            case 2:
               $iva=$item ['valor'];
            case 3:
               $plus=$item ['valor'];
            case 4:
               $gastosAdm=$item ['valor'];
            case 5:
               $sello=$item ['valor'];  
            case 6:
                  $retenciones_iibb=$item ['valor'];
            case 7:
                  $retenciones_lh=$item ['valor'];
            case 8:
                  $retenciones_iva=$item ['valor'];
            case 9:
                  $retenciones_ganancia=$item ['valor'];
            case 10:
                  $sellos=$item ['valor'];  
            case 11:
                  $adicional_lote_hogar=$item ['valor'];  
            case 12:
                  $cft=$item ['valor'];  
            case 13:
                  $tna=$item ['valor'];                            
            }
    }        
  
  //  '-intereses-'.
  $c_intereses=porcentaje($montoprestamo,$interes);
  //  '-iva-'.
  $c_iva=porcentaje($c_intereses,$iva);
  //  '-plus-'.
  $c_plus=porcentaje($montoprestamo,$plus);
  // '<br>';
  // subtotal 1
  //  'subtotal 1 :'.
  $subtotal1=round(($montoprestamo+$c_intereses+$c_plus+$c_iva),2,PHP_ROUND_HALF_UP);
  //  '-gastos adm-'.
  $c_gastosAdm=porcentaje($subtotal1,$gastosAdm);//
  //  '-iva gastos adm2 -'.
  $c_ivaGastosAdm=porcentaje($c_gastosAdm,$iva);//.'--';

  //  'subtotal 2 :'.
  $subtotal2=round(($subtotal1+$c_gastosAdm+$c_ivaGastosAdm),2);

  /**** NUEVO CALCULO 09/05/2017****/
      $subtotalCupon=round(($coefTarjeta*$subtotal2),2,PHP_ROUND_HALF_UP);

      //  '--interes tarjeta--'. 
      $interesTarjeta=$subtotalCupon-$subtotal1;
      // 'IVA INTERES TARJETA'
      $IVA_interesTarjeta=porcentaje($interesTarjeta,$iva);
      // '-- total cupon'
      $totalCupon=($subtotal2+$interesTarjeta+$IVA_interesTarjeta); 

  /*** FIN ***/

   //  '--coef tarjeta-- '.  $coefTarjeta=1.2999;
  //  '--total cupon--'. 
 //$totalCupon=round(($coefTarjeta*$subtotal2),2,PHP_ROUND_HALF_UP);

  //  '--interes tarjeta--'. 
  //$interesTarjeta=$totalCupon-$subtotal1;
  // '<br>';
  // '---Cobranza--';
  // '<br>';
  //  '--arancel tarjeta--'. 
 // antes $arancelTarjeta=3;
  $arancelTarjeta = prestamotarjeta::arancelTarjeta($tarjeta_id);
  //  '--arancel tarjeta--'. 
  $c_arancelTarjeta=porcentaje($subtotalCupon,$arancelTarjeta);
  //  '-iva arancel Targeta -'. 
  $c_ivaArancelTarjeta=porcentaje($c_arancelTarjeta,$iva);
  //  'subtotal 3 
  $subtotal3=($subtotalCupon-$c_arancelTarjeta-$c_ivaArancelTarjeta);
  // '<br>';
  //  '--interes tarjeta--'.$interesTarjeta;
  //  '-iva Interes Tarjeta -'. 
  $c_ivaInteresTarjeta=porcentaje($interesTarjeta,$iva);//.'--';
  
  //  '--Gastos tarjeta-- es la sumatoria de los gastos'. 
  //  $gastosTarjeta=0;
  $gastosTarjeta = prestamotarjeta::gastosTarjeta($tarjeta_id);
  //  '--retenciones ii bb--'. 
  //$retenciones_iibb=2.5;
  //  '--retenciones_iibb--'. 
  $c_retenciones_iibb=porcentaje($subtotal3,$retenciones_iibb);
  //  '--retenciones lote hogar--'. 
  //$retenciones_lh=20;
  //  '--retenciones_lh--'. 
  $c_retenciones_lh=porcentaje($c_retenciones_iibb,$retenciones_lh);
  //  '--retenciones iva--'. 
  //$retenciones_iva=3;
  //  '--retenciones_iva--'. 
  $c_retenciones_iva=porcentaje($subtotal3,$retenciones_iva);
  //  '--retenciones ganancias--'. 
  //$retenciones_ganancia=1;
  //  '--retenciones_ganancias--'. 
  $c_retenciones_ganancia=porcentaje($subtotal3,$retenciones_ganancia);
  // '<br>';
  //  '-- a cobrar--'. 
  $a_cobrar=($subtotal3-$interesTarjeta-$c_ivaInteresTarjeta-$gastosTarjeta-$c_retenciones_iibb-$c_retenciones_lh-$c_retenciones_iva-$c_retenciones_ganancia);
 //  '--sello--'. 
  //$sellos=1.50;
 //  '--Sellos --'. 
  $c_sellos=porcentaje($a_cobrar,$sellos);
 //  '--Adicional lote hogar--'. 
  //$adicional_lote_hogar=20;
  //  '--retenciones_iva--'. 
  $c_adicional_lote_hogar=porcentaje($c_sellos,$adicional_lote_hogar);
  // '<br>';
//  '-- total Neto--'. 
  $total_neto=($a_cobrar-$c_sellos-$c_adicional_lote_hogar);
//  echo $total_neto;
  //$datos[] = array('totalCupon'=> $totalCupon);

  //insertar el prestamo 

  $sucursal_id='0';
  $rendido='NO';
  $fecha_otorgado=date("Y-m-d");
  $fechahora=date("Y-m-d H:i:s");

  $fecha_cancelado='';
  $documentacion='NO';
  $objecto = new PrestamoTarjeta();
  $todobien = $objecto->nuevo($cupon_id,$fechahora,$pdf_dni,$montoprestamo,$monto_apagar,$cft,$tna,$estado,$sucursal_id,$rendido,$fecha_otorgado,$fecha_cancelado,$documentacion);
  if($todobien){
     // echo 'se inserto';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar Cobranza ...
         </div> 
    <?
    }
  $objecto = new PrestamoTarjeta();
  $prestamo_id = $objecto->ultimoid();
  if($prestamo_id){
     // echo 'el ultimo id: '.$prestamo_id;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ultimo id ...
         </div> 
    <?
    }

    /*** insertar factura b *********/
    /*1 datos para factura*/
    $monto_solicitado= $montoprestamo;
    $plus=$c_plus;
    //echo '--'.
    $monto_prestamo_facturado=($monto_solicitado+$plus);
    //echo '--'.
    $interes=$c_intereses;
    //echo '--'.
    $iva_interes=porcentaje($interes,$iva);
    //echo '--'.
    $interes_mas_iva=($interes+$iva_interes);
    //echo '--'.
    $gastos_administrativos=$c_gastosAdm;
    //echo '--'.
    $interes_tarjeta=$interesTarjeta;
    //echo '--'.
    $gastos_adm_mas_interes=($gastos_administrativos+$interes_tarjeta);
     //echo '--'.
     $iva_gastos_adm=porcentaje($gastos_adm_mas_interes,$iva);
    //echo '--'.
    $gastos_adm_con_iva=($gastos_adm_mas_interes+$iva_gastos_adm);
    //echo '--'.
    $total_factura_b=($gastos_adm_con_iva+$interes_mas_iva+$monto_prestamo_facturado);
    
    $guardar_factura_b = prestamotarjeta::insertar_factura_b($prestamo_id,$monto_solicitado,$plus,$monto_prestamo_facturado,$interes,$iva_interes,
        $interes_mas_iva,
        $gastos_administrativos,
        $interes_tarjeta,
        $gastos_adm_mas_interes,
        $iva_gastos_adm,
        $gastos_adm_con_iva,
        $total_factura_b);
    //exit();

   /*** FIN ***/

  //inserta en la tabla cobranza
        //variable que faltan
        //$prestamo_id=1;
  $objecto = new PrestamoTarjeta();
  $todobien1 = $objecto->nuevoCobranza($prestamo_id, 
  $subtotalCupon, 
  $c_arancelTarjeta, 
  $c_ivaArancelTarjeta, 
  $subtotal3, 
  $interesTarjeta, 
  $c_ivaInteresTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $gastosTarjeta, 
  $c_retenciones_iibb, 
  $c_retenciones_lh, 
  $c_retenciones_iva, 
  $c_retenciones_ganancia, 
  $a_cobrar, 
  $c_sellos, 
  $c_adicional_lote_hogar, 
  $total_neto);
  if($todobien1){
      //echo 'se inserto nuevoCobranza';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar Cobranza ...
         </div> 
    <?
    }
  
 $plus=$c_plus;

$objecto = new PrestamoTarjeta();
  $todobien2 = $objecto->nuevoDetallePrestamo($prestamo_id, 
  $totalCupon, 
  $c_intereses, 
  $c_iva, 
  $plus, 
  $subtotal1, 
  $c_gastosAdm, 
  $c_ivaGastosAdm, 
  $subtotal2, 
  $cuotas, 
  $coefTarjeta, 
  $totalCupon, 
  $interesTarjeta);
  if($todobien2){
     // echo 'se inserto nuevoDetallePrestamo';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar  Detalle...
         </div> 
    <?
    }

  $cajanueva_id=$objecto->buscarCajaActiva();
  //$todobien3 = $objecto->guardarPrestamoCaja($cajanueva_id,$operacion,$tipo,$monto,$fechahora,$detalle,$usuario_id);
   $todobien3 = $objecto->guardarPrestamoCaja($cajanueva_id,'Prestamo','Egreso',$montoprestamo,$fechahora,'Prestamo',$ID);


  if($todobien3){
     // echo 'Se Inserto el Prestamo en la Caja.';
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar en la caja.  
         </div> 
    <?
    }

    /* 
    //14-08-2018 generar cuotas prestamo e insertar en la tabla de cuotas_prestamo

      // a- traer cupon
     // a- traer cupon
      $datos_cupon=cuota_prestamo::traer_cupon($cupon_id);
      foreach($datos_cupon as $item1)
      {
        $monto=$item1 ['monto'];
        $fecha=$item1 ['fechahora'];
        $cuotas=$item1 ['cuotas'];
        $tarjeta_id=$item1 ['tarjeta_id'];
        $cliente_id=$item1 ['cliente_id'];
      } 
      $dias_tarjeta=cuota_prestamo::traer_dias_tarjeta($tarjeta_id);
      $x=0;
      foreach($dias_tarjeta as $item2)
      {
       $x = $item2['cantidad_dias']; //dias a sumar
      } 
      $fecha_liquidacion= date("Y-m-d", strtotime("$fecha + ". $x ." days")); //se suman los $x dias
         
      // 2 - generar las cuotas de cada prestamo
      
      $i=1;
      $monto_cuota=($monto/$cuotas);
      $fecha_liquidacion= date("Y-m-d", strtotime("$fecha + ". $x ." days")); //se suman los $x dias
      while ( $i<= $cuotas)
      {
        $numero_cuota=$i;

        $fecha_liquidacion= date("Y-m-d", strtotime("$fecha_liquidacion + ". $x ." days")); //se suman los $x dias
             $insertar_cuota=cuota_prestamo::insertar_cuota($prestamo_id,$tarjeta_id,$cupon_id,$cliente_id,$monto_cuota,$fecha_liquidacion,$numero_cuota);
        $i++; 
       
       }

    // Fin del generar cuotas del prestamo */
 
   /* con datos factura b*/

     $facturab_id=cuota_prestamo::traer_ultima_facturab();
      
     $dias_tarjeta=cuota_prestamo::traer_dias_tarjeta($tarjeta_id);

      $x=0;
      foreach($dias_tarjeta as $item2)
      {
       $x = $item2['cantidad_dias']; //dias a sumar
      } 
      
      // sacar las fechas de liquidacion cuando la tarjeta paga esta cuota 
     
     $fecha= date("Y-m-d");
     $fecha_liquidacion= date("Y-m-d", strtotime("$fecha + ". $x ." days")); //se suman los $x dias
     // 2 - generar las cuotas de cada prestamo
     
      $i=1;
      //sacar los montos divididos en las cuotas
        $total_facturado=(($interes_mas_iva+$gastos_adm_con_iva+$monto_prestamo_facturado)/$cuotas);
      $interes_mas_iva=($interes_mas_iva/$cuotas);
      $gastos_adm_con_iva=($gastos_adm_con_iva/$cuotas);
      $monto_prestamo=($monto_prestamo_facturado/$cuotas);
      // antes 25/02/2020 $total_facturado=($total_factura_b/$cuotas);
        
      while ( $i<= $cuotas)
      {
        $numero_cuota=$i;
        $insertar_cuota=cuota_prestamo::insertar_cuota($prestamo_id,$fecha,$tarjeta_id,$cupon_id,$cliente_id,$facturab_id,$numero_cuota,$fecha_liquidacion,$monto_prestamo,$interes_mas_iva,$gastos_adm_con_iva,$total_facturado);
        $fecha_liquidacion= date("Y-m-d", strtotime("$fecha_liquidacion + ".$x." days")); //se suman los $x dias
        $i++;
      }
       
    // fin

echo'<script type="text/javascript">
window.location="imprimir.php?cupon_id='.$cupon_id.'"</script>';
}
 else echo'Faltan datos.';
?>