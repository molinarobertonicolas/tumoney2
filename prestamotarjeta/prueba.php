<!DOCTYPE html>
<html lang="es-ES">
<head>
  <meta charset="utf-8">
  <script src="//code.jquery.com/jquery-latest.js"></script>
  
</head>
<body>
  <input type="text" name="dni" id="dni">

  <button id="buscarDNI" > buscarDNI</button>

  <input type="hidden" name="cliente_id" id="cliente_id">
  
  <input type="text" name="nombre" id="nombre">
  
  <select id="tarjeta_id">
  	<option>Seleccionar T...</option>
  </select>

  <input type="text" name="montoprestamo" id="montoprestamo">
  
  <button id="generarPrestamo" > Generar </button>
  

 <script src="../js/util.js"></script> 
 <script src="../js/jquery-1.10.2.js"></script> 
 <script src="../js/jquery.maskedinput.js" type="text/javascript"></script>      
 <script type="text/javascript">

 $(document).ready(function()
  {
    $("#dni").mask("99999999");
    // 16012017 buscar por dni
    $("#buscarDNI").click(function()
     {
        var vdni = $("#dni").val();   
        alert(vdni);     
        $.ajax({
			    data: {"dni" : vdni},
			    type: "POST",
			    dataType: "json",
			    url: "traerDatosCliente.php",
		  	})
		   .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			         console.log( "La solicitud se ha completado correctamente." );
			     }
			     $.each(data, function(i,cliente){
						document.getElementById("cliente_id").value=cliente.id;
    				     //alert(cliente.nombre);
    				    document.getElementById("nombre").value=cliente.nombre;
    				 });
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "La solicitud a fallado: " +  textStatus);
			     }
			});
      });//fin buscarDNI

     listadoTarjetas();  

     $("#generarPrestamo").click(function()
     {
        var vtarjeta_id = $("#tarjeta_id").val();   
        //alert(vtarjeta_id);
        var vmontoprestamo = $("#montoprestamo").val();   
        //alert(vmontoprestamo);    

        $.ajax({
			    data: {"tarjeta_id" :vtarjeta_id ,"montoprestamo":vmontoprestamo},
			    type: "POST",
			    url: "generarPrestamo.php",
		  	})
		   .done(function( data, textStatus, jqXHR ) {
			     if ( console && console.log ) {
			         console.log( "La solicitud es correcta." );
			     }
			     alert(data);
			 })
			 .fail(function( jqXHR, textStatus, errorThrown ) {
			     if ( console && console.log ) {
			         console.log( "La solicitud a fallado de Generar: " +  textStatus);
			     }
			});
      });//fin buscarDNI

    
     
  }) //fin jquery   

  //17012017 listado de tarjetas para el select recibe un json
  function listadoTarjetas(){
  	$.getJSON('listaTarjetas.php',function(data){
  		    $.each(data, function(i,tarjeta){
			$("#tarjeta_id").append('<option value="'+tarjeta.id+'">'+tarjeta.nombre+'</option>');
		    });
		});
   };
           
</script>
</body>
</html>