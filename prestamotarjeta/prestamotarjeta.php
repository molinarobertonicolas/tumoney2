<?php
include_once("../bd/conexion.php");
class PrestamoTarjeta
{
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT
    prestamo_tarjeta.`estado` AS estado, 
    prestamo_tarjeta.`id` AS id, 
    prestamo_tarjeta.`fechahora` AS fecha,
    cliente.`nombre` AS cliente,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`id` AS id_cupon,
    cupondetarjeta.`numero_cupon` AS numero_cupon,
     cupondetarjeta.`cuotas` AS cuotas,
    prestamo_tarjeta.`montoprestamo` AS monto_prestado,
    prestamo_tarjeta.`monto_apagar` AS apagar,
    prestamo_tarjeta.`pdf_dni` AS documento 
    FROM
    `prestamo_tarjeta`
    INNER JOIN `cupondetarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
      INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
        left JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`)
         order by prestamo_tarjeta.`id` desc";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


   public function nuevo($cupon_id,$fechahora,$pdf_dni,$montoprestamo,$monto_apagar,$cft,$tna,$estado,$sucursal_id,$rendido,$fecha_otorgado,$fecha_cancelado,$documentacion)
  {
    $sql="INSERT INTO `prestamo_tarjeta`
            (`cupon_id`,
             `fechahora`,
             `pdf_dni`,
             `montoprestamo`,
             `monto_apagar`,
             `cft`,
             `tna`,
             `estado`,
             `sucursal_id`,
             `rendido`,
             `fecha_otorgado`,
             `fecha_cancelado`,
             `documentacion`
             )
            VALUES ('$cupon_id',
                    '$fechahora',
                    '$pdf_dni',
                    '$montoprestamo',
                    '$monto_apagar',
                    '$cft',
                    '$tna',
                    '$estado',
                    '$sucursal_id',
                    '$rendido',
                    '$fecha_otorgado',
                    '$fecha_cancelado',
                    '$documentacion'
                  );";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

	
    public function buscarDNI($dni)
    {
    $sql="SELECT id,nombre FROM cliente WHERE dni ='$dni'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  //buscar datos cupon
  public function buscarCupon($id)
  {
      $data[]=0;
      $sql="SELECT * FROM cupondetarjeta WHERE id ='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $data[] = $fila;
        }
      }
      return $data;
  }

  //buscar coeficiente de la tarjeta 
  public function buscarCoeficiente($tarjeta_id,$cuotas)
  {
      $data[]=0;
      $sql="SELECT coeficiente FROM tarjeta WHERE id ='$tarjeta_id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        { 
          $nombreTabla= $fila['coeficiente'];
        }
      }
     
      $sql="SELECT coeficiente FROM ".$nombreTabla." WHERE cuota ='$cuotas'";
    
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $coeficiente= $fila['coeficiente'];
        }
      }
      //retorno el coeficiente segun las cuotas
      return $coeficiente;
  }

  //traer los gasto asociados al prestamo
  public function traerGastosPrestamo()
  {
    $sql="SELECT * FROM gastos_pres_tarj";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  //buscar por numero de cupon
   public function buscarCuponNumero($numero_cupon)
   {
      $sql="SELECT * FROM cupondetarjeta WHERE numero_cupon ='$numero_cupon'";
      
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function ultimoid()
   {
     $rs = mysqli_insert_id(conexion::obtenerInstancia());
     return $rs;
  }

public function nuevoCobranza($prestamo_id, 
  $totalcupon, 
  $aranceltarjeta, 
  $ivaarancel, 
  $subtotal, 
  $interesestarjeta, 
  $ivaintereses, 
  $gastostarj1, 
  $ivagtos1, 
  $gastostarj2, 
  $ivagtos2, 
  $retiibb, 
  $retlh, 
  $retiva, 
  $retgan, 
  $acobrar, 
  $sellos, 
  $lotehogar, 
  $neto)
  {
    
     $sql="INSERT INTO `cobranza` 
  (`prestamo_id`, 
  `totalcupon`, 
  `aranceltarjeta`, 
  `ivaarancel`, 
  `subtotal`, 
  `interesestarjeta`, 
  `ivaintereses`, 
  `gastostarj1`, 
  `ivagtos1`, 
  `gastostarj2`, 
  `ivagtos2`, 
  `retiibb`, 
  `retlh`, 
  `retiva`, 
  `retgan`, 
  `acobrar`, 
  `sellos`, 
  `lotehogar`, 
  `neto`
  )
  VALUES
  ('$prestamo_id', 
  '$totalcupon', 
  '$aranceltarjeta', 
  '$ivaarancel', 
  '$subtotal', 
  '$interesestarjeta', 
  '$ivaintereses', 
  '$gastostarj1', 
  '$ivagtos1', 
  '$gastostarj2', 
  '$ivagtos2', 
  '$retiibb', 
  '$retlh', 
  '$retiva', 
  '$retgan', 
  '$acobrar', 
  '$sellos', 
  '$lotehogar', 
  '$neto'
  );";
                     
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

public function nuevoDetallePrestamo($prestamo_id, 
  $prestamo, 
  $intereses, 
  $iva, 
  $plus, 
  $subtotal1, 
  $gtosadmin, 
  $ivagtos, 
  $subtotal2, 
  $cuotas, 
  $coeficientetarjeta, 
  $totalcupon, 
  $interesestarjeta)
  {
    
     $sql="INSERT INTO `detalleprestamo` 
  (`prestamo_id`, 
  `prestamo`, 
  `intereses`, 
  `iva`, 
  `plus`, 
  `subtotal1`, 
  `gtosadmin`, 
  `ivagtos`, 
  `subtotal2`, 
  `cuotas`, 
  `coeficientetarjeta`, 
  `totalcupon`, 
  `interesestarjeta`
  )
  VALUES
  ('$prestamo_id', 
  '$prestamo', 
  '$intereses', 
  '$iva', 
  '$plus', 
  '$subtotal1', 
  '$gtosadmin', 
  '$ivagtos', 
  '$subtotal2', 
  '$cuotas', 
  '$coeficientetarjeta', 
  '$totalcupon', 
  '$interesestarjeta'
  );";
                     
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

  //21-02-2017 modificar el estado de los cupones
  public function estadoCupon($idcupon)
  {
    $sql="UPDATE `cupondetarjeta`
          SET  `estado` = 'completo'
          WHERE `id`='$idcupon'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs)>0)
    {
      return 'error actualizar'.$idcupon;
    }
    return 1;
  }

  //24022017
  public function datosPrestamos($idcupon)
  {
   
    $consulta="SELECT DISTINCT
      cliente.`nombre` AS nombre,
      cliente.`cuit` AS cuit,
      cliente.`domicilio` AS calle,
      cliente.`departamento` AS departamento,
      prestamo_tarjeta.`montoprestamo` AS montoprestamo,
      prestamo_tarjeta.`cft` AS cft,
      prestamo_tarjeta.`tna` AS tna,
      DATE_FORMAT(prestamo_tarjeta.`fechahora`, '%d/%m/%Y') AS fecha,
      detalleprestamo.`plus` AS plus,
      detalleprestamo.`gtosadmin` AS gastosadministrativos,
      detalleprestamo.`interesestarjeta` AS interestarjeta, 
      tarjeta.`nombre` AS nombretarjeta,
      cupondetarjeta.`cuotas` AS cuotas,
      cupondetarjeta.`monto` AS totalcupon,
      cupondetarjeta.`numero_cupon` AS numeroCupon
      FROM
          `cliente`
          INNER JOIN `cupondetarjeta` 
              ON (`cliente`.`id` = `cupondetarjeta`.`cliente_id`)
          INNER JOIN `tarjeta` 
              ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
          INNER JOIN `prestamo_tarjeta` 
              ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
          INNER JOIN `detalleprestamo` 
              ON (`prestamo_tarjeta`.`id` = `detalleprestamo`.`prestamo_id`)
              WHERE cupondetarjeta.`id`=".$idcupon;

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


  // 24-04-2017 trear el arancel de la Tarjeta
  public function arancelTarjeta($id)
  {
    $sql="SELECT arancel FROM tarjeta where id=$id";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $arancel= $fila['arancel'];
        }
      }
      //retorno el arancel de la tarjeta
      return $arancel;
  }

  // 24-04-2017 trear los gastos de la Tarjeta
  public function gastosTarjeta($id)
  {
    $sql="SELECT gastos1,gastos2,gastos3 FROM tarjeta where id=$id";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $gastos= $fila['gastos1'] + $fila['gastos2'] + $fila['gastos3'];
        }
      }
      //retorno el gastos de la tarjeta
      return $gastos;
  }

  /*11-05-2017 insetar factura b*/
   public function insertar_factura_b($prestamo_id,$monto_solicitado,$plus,$monto_prestamo_facturado,$interes,$iva_interes,
        $interes_mas_iva,
        $gastos_administrativos,
        $interes_tarjeta,
        $gastos_adm_mas_interes,
        $iva_gastos_adm,
        $gastos_adm_con_iva,
        $total_factura_b)
    {
      $sql="INSERT INTO `facturab`
            ( 
              `prestamo_id`,
             `monto_solicitado`,
             `plus`,
             `monto_prestamo_facturado`,
             `interes`,
             `iva_interes`,
             `interes_mas_iva`,
             `gastos_administrativos`,
             `interes_tarjeta`,
             `gastos_adm_mas_interes`,
             `iva_gastos_adm`,
             `gastos_adm_con_iva`,
             `total_factura_b`)
VALUES ('$prestamo_id',
        '$monto_solicitado',
        '$plus',
        '$monto_prestamo_facturado',
        '$interes',
        '$iva_interes',
        '$interes_mas_iva',
        '$gastos_administrativos',
        '$interes_tarjeta',
        '$gastos_adm_mas_interes',
        '$iva_gastos_adm',
        '$gastos_adm_con_iva',
        '$total_factura_b');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
      }

      ///// nuevas funciones 21052017
    public function guardarcupon($tarjeta_id,$monto,$cuotas,$fechahora,$cliente_id,$numero_cupon,$digitos)
    {
      $sql="INSERT INTO `cupondetarjeta`
              (`tarjeta_id`,
               `monto`,
               `cuotas`,
               `fechahora`,
               `cliente_id`,
               `numero_cupon`,`digitos`,
               `estado`)
      VALUES ('$tarjeta_id','$monto','$cuotas','$fechahora','$cliente_id','$numero_cupon','$digitos','nuevo');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
       $cupon_id=mysqli_insert_id(conexion::obtenerInstancia());
      return $cupon_id;
      }

    public function guardarcliente($dni,$nombre,$cuit,$domicilio,$departamento,$telefono,$fecha_ingreso,$email)
    {
      $sql="INSERT INTO `cliente`
              (`dni`,
               `nombre`,
               `cuit`,
               `domicilio`,
               `departamento`,
               `telefono`,
               `fecha_ingreso`,
               `email`)
      VALUES ('$dni',
              '$nombre',
              '$cuit',
              '$domicilio',
              '$departamento',
              '$telefono',
              '$fecha_ingreso',
              '$email');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      $cliente_id=mysqli_insert_id(conexion::obtenerInstancia());
      return $cliente_id;
      }

     
  // 24-04-2017 trear el id de una Tarjeta
  public function buscar_id_tarjeta($nombretarjeta)
  {
   $sql="SELECT id FROM tarjeta where alias_sistema='$nombretarjeta'";
   
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $id= $fila['id'];
        }
      }
      //retorno el gastos de la tarjeta
      return $id;
  }


  //18062017 insertar prestamo en la caja
  
  public function guardarPrestamoCaja($cajanueva_id,$operacion,$tipo,$monto,$fechahora,$detalle,$usuario_id)
    {
      $sql="INSERT INTO `cajadetalle`
            (`cajanueva_id`,
             `operacion`,
             `tipo`,
             `monto`,
             `fechahora`,
             `detalle`,
             `usuario_id`)
            VALUES ('$cajanueva_id',
                    '$operacion',
                    '$tipo',
                    '$monto',
                    '$fechahora',
                    '$detalle',
                    '$usuario_id');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      $cliente_id=mysqli_insert_id(conexion::obtenerInstancia());
      return $cliente_id;
      }

      
  public function buscarCajaActiva()
  {
   $consulta="SELECT id FROM cajanueva WHERE estado='Abierta'";
   $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
        $id= $fila['id'];
        }
      }
      //retorno el gastos de la tarjeta
      return $id;
  }



}
?>