﻿<?php
include("../sesion.php");
include("../menu.php");
include("caja.php");
?>
 <div class="container">
    <h2>Caja Diaria</h2>
      <p>
      <?
        $objecto = new Caja();
        $cajacerrada = caja::estadoCajas();
        foreach($cajacerrada as $item)
        {
         $cajasAbiertas=$item ['abiertas'];
        } 
        if ($cajasAbiertas==0)
        {
          ?>
           <a class="btn btn-primary" href="abrircaja.php">Abrir Caja</a> 
          <?
        }
      ?>
      </p>
    
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>N°</th>
             <th>Fecha apertura</th>
             <th>Importe</th>
             <th>Fecha cierre</th>
             <th>Saldo</th>
             <th>Estado</th>
             <th></th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objecto = new Caja();
          $usuarios = caja::lista();
          foreach($usuarios as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['fechaapertura']; ?></td>
              <td><?php echo $item ['inicio']; ?></td>
              <td><?php echo $item ['fechacierre']; ?></td>
              <td><?php echo $item ['cierre']; ?></td>
              <td><?php echo $item ['estado']; ?></td>
              <td> 
                  <?
                    if ($item ['estado']=='Abierta') {
                    ?>
                     <a class="btn btn-danger btn-sm" href="cerrarcaja.php?caja_id=<?php echo $item ['id'];?>">Cerrar Caja</a></td>
                    <?  
                    }
                     else {
                    ?>
                     <a class="btn btn-primary btn-sm" href="detalle_caja.php?caja_id=<?php echo $item ['id'];?>">Detalle de la Caja</a></td>
                    <?  
                    }
                  ?> 
                 
            </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
       </div>
         
  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>