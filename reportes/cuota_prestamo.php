<?
include_once("../bd/conexion.php");
class Cuota_Prestamo
{
  
  public function lista()
  { 
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`total_facturado` AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre,
              cliente.`nombre` AS cliente_nombre 
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                      INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)
                      LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      ORDER BY prestamo_id DESC, numero_cuota DESC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function listatarjetas()
  { 
    $consulta="SELECT id,nombre from tarjeta order by nombre desc";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


 public function insertar_cuota($prestamo_id,$fecha,$tarjeta_id,$cupon_id,$cliente_id,$facturab_id,$numero_cuota,$fecha_liquidacion,$monto_prestamo,$interes_mas_iva,$gastos_adm_con_iva,$total_facturado)
  {
    $sql="INSERT INTO `cuota_prestamo`
            (`prestamo_id`,
             `prestamo_fecha`,
             `tarjeta_id`,
             `cupon_id`,
             `cliente_id`,
             `facturab_id`,
             `numero_cuota`,
             `fecha_liquidacion`,
             `monto_prestamo`,
             `interes_mas_iva`,
             `gastos_adm_con_iva`,
             `total_facturado`)
              VALUES ('$prestamo_id',
                      '$fecha',
                      '$tarjeta_id',
                      '$cupon_id',
                      '$cliente_id',
                      '$facturab_id',
                      '$numero_cuota',
                      '$fecha_liquidacion',
                      '$monto_prestamo',
                      '$interes_mas_iva',
                      '$gastos_adm_con_iva',
                      '$total_facturado');";
                      //echo $sql; exit();
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

  //listar todo los prestamos
  public function traer_prestamos()
  {
    $consulta="SELECT
                `id`,
                `cupon_id`,
                `fechahora`
                 FROM `prestamo_tarjeta` where fechahora >= '2018-01-01 06:00:00' and estado <> 'Cancelado'  order by fechahora desc ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function traer_cupon($cupon_id)
  {
    $consulta="SELECT * FROM `cupondetarjeta` where id=$cupon_id ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function traer_dias_tarjeta($tarjeta_id)
  {
    $data[]="";
    $consulta="SELECT diaspago as cantidad_dias FROM `tarjeta` where id=$tarjeta_id";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function cuotas_dias($fecha_desde,$fecha_hasta,$tarjeta_id)
  {
    $data=array();
    /*$99consulta="SELECT * FROM `cuota_prestamo` WHERE tarjeta_id='$tarjeta_id' and `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";*/

    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`total_facturado` AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre,
              cliente.`nombre` AS cliente_nombre 
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)   
                  LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      WHERE tarjeta_id='$tarjeta_id' and `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function cuotas_dias2($fecha_desde,$fecha_hasta)
  {
    $data=array();
    /*$consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto_prestamo` AS monto_prestamo,
              cuota_prestamo.`interes_mas_iva` AS interes_mas_iva,
              cuota_prestamo.`gastos_adm_con_iva` AS gastos_adm_con_iva,
              cuota_prestamo.`total_facturado` AS total_facturado,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)   
                  LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      WHERE `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";*/
  
              $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto_prestamo` AS monto_prestamo,
              cuota_prestamo.`interes_mas_iva` AS interes_mas_iva,
              cuota_prestamo.`gastos_adm_con_iva` AS gastos_adm_con_iva,
              cuota_prestamo.`total_facturado` AS total_facturado,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
              ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                        WHERE `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE)
                               AND prestamo_tarjeta.`fechahora`>= CAST('$fecha_desde' AS DATE)
                               ORDER BY fecha_liquidacion DESC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  // consulta para sacar los totales por dia
  

  public function subtotales_por_dia($fecha_desde,$fecha_hasta)
  {
    $data=array();
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              SUM(cuota_prestamo.`total_facturado`) AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre              
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)     
                      WHERE `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE)
                      GROUP BY fecha_liquidacion ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  


  // 24102018 sacar los intereses de las facturasb
  public function datos_facturab()
  {
    $data=array();
    $consulta="SELECT
            prestamo_tarjeta.`id` AS prestamo_id,
            prestamo_tarjeta.`fechahora` AS prestamo_fecha,
            cupondetarjeta.`id` AS cupon_id,
            cupondetarjeta.`cuotas` AS cuotas,
            cliente.`id` AS cliente_id,
            tarjeta.`id` AS tarjeta_id,
            facturab.`id` AS facturab_id,
            facturab.`interes_mas_iva` AS interes_mas_iva,
            facturab.`gastos_adm_con_iva` AS gastos_adm_con_iva,
            facturab.`monto_prestamo_facturado` AS monto_prestamo,
            facturab.`total_factura_b` AS total_facturado
            FROM
                `facturab`
                INNER JOIN `prestamo_tarjeta` 
                    ON (`facturab`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                INNER JOIN `cupondetarjeta` 
                    ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
                    INNER JOIN `tarjeta` 
                    ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
                    LEFT JOIN `cliente` 
                    ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`) WHERE prestamo_tarjeta.`fechahora`>'2019-01-01' ";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }  

  // 26102018 sacar los intereses de las facturasb
  public function traer_ultima_facturab()
  {
    $id=0;
    $consulta="SELECT MAX(id) AS id FROM facturab";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
       if ($row = mysqli_fetch_row($rs)) {
          $id = trim($row[0]);
          }
    }
    return $id;
  }  


  //02-12-2018
  public function cuotas_a_cobrar($fecha_desde)
  {
    $data=array();
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto_prestamo` AS monto_prestamo,
              cuota_prestamo.`interes_mas_iva` AS interes_mas_iva,
              cuota_prestamo.`gastos_adm_con_iva` AS gastos_adm_con_iva,
              cuota_prestamo.`total_facturado` AS total_facturado,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              CAST('$fecha_desde' AS DATE) as parametro
              FROM `cuota_prestamo` 
              where cuota_prestamo.`fecha_liquidacion`>CAST('$fecha_desde' AS DATE)
              ORDER BY fecha_liquidacion ASC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function cuotas_a_cobrar_entre_fechas($fecha_desde,$fecha_hasta)
  {
    $data=array();
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto_prestamo` AS monto_prestamo,
              cuota_prestamo.`interes_mas_iva` AS interes_mas_iva,
              cuota_prestamo.`gastos_adm_con_iva` AS gastos_adm_con_iva,
              cuota_prestamo.`total_facturado` AS total_facturado,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              CAST('$fecha_desde' AS DATE) as parametro
              FROM `cuota_prestamo` 
              where cuota_prestamo.`fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE)
              ORDER BY fecha_liquidacion ASC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  // modificacion 24/12/2018 se unifican las colunmas de intereses y gastos administrativos
  public function cuotas_a_cobrar_24122018($fecha_desde)
  {
    $data=array();
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto_prestamo` AS monto_prestamo,
              cuota_prestamo.`interes_mas_iva` AS interes_mas_iva,
              cuota_prestamo.`gastos_adm_con_iva` AS gastos_adm_con_iva,
              cuota_prestamo.`total_facturado` AS total_facturado,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              CAST('$fecha_desde' AS DATE) AS parametro,
              prestamo_tarjeta.`fechahora` AS fecha_prestamo
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta`  
                 ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   WHERE (cuota_prestamo.`fecha_liquidacion`> '$fecha_desde') AND (prestamo_tarjeta.`fechahora` >= '2018-12-24')
                     ORDER BY fecha_liquidacion ASC";
                     
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


 }
?>