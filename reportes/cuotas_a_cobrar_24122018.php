<?php
/* se agrega esta modificacion a partir del 24/12/2018 donde se suman los gastos adm mas los intereses , en un iten llamado intereses*/
include("../sesion.php");
include("cuota_prestamo.php");
if( isset($_POST['fecha_desde']))
{
$fecha_desde=$_POST['fecha_desde'];
$t_monto_prestamo=0;
$t_total_facturado=0;
$t_interes_mas_iva=0;
$t_gastos_adm_con_iva=0;

$listados = cuota_prestamo::cuotas_a_cobrar_24122018($fecha_desde);
foreach($listados as $item)
  {
    $t_monto_prestamo=$t_monto_prestamo+$item['monto_prestamo'];
    $t_interes_mas_iva=$t_interes_mas_iva+$item['interes_mas_iva'];
    $t_gastos_adm_con_iva=$t_gastos_adm_con_iva+$item['gastos_adm_con_iva'];
    $t_total_facturado=$t_total_facturado+$item['total_facturado'];
  }
  ?>

  
   <table id="listado" class="table table-striped table-bordered table-hover table-condensed" style="font-size: 18px" >
          <thead>
             <tr>
             	<td>Total Monto Prestamo : </td>
             	<td><? 
                     $numero =$t_monto_prestamo;// 1002002.365;
                     echo '$ '.number_format($numero, 2, ",", ".");
               ?></td>
             </tr>
             <tr>
             	<!--td>Total Interes mas Iva : </td>
             	<td><?php
                     $numero =$t_interes_mas_iva;
                     echo '$ '.number_format($numero, 2, ",", ".");
               ?> </td>
             </tr>
             <tr>
             	<td>Total Gastos Adm con Iva : </td>
             	<td> <?
                     $numero =$t_gastos_adm_con_iva;
                     echo '$ '.number_format($numero, 2, ",", ".");
                      ?></td>
             </tr-->
            
             <td>Total Interes + Gastos : </td>
              <td><?php
                     $numero =$t_interes_mas_iva + $t_gastos_adm_con_iva;
                     echo '$ '.number_format($numero, 2, ",", ".");
               ?> </td>
             </tr>

             <tr>
             	<td>Total Liquidación : </td>
             	<td><?php
                     $numero =$t_total_facturado;
                     echo '$ '.number_format($numero, 2, ",", ".");
                ?> </td>
             </tr>
            </thead>
            </table> 

         
         <h3>Listado de todas las cuotas que forman este reporte</h3>
         <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Nº Cupon </th>
             <th>Nº Cuota</th>
             <th>Fecha Liquidación </th>
             <th>Monto Prestamo</th>
             <th>Monto Intereses </th>
             <th>Gastos Admi.</th>
             <th>Intereses + Gastos Admi.</th>
             <th>Total Facturado</th>
             </tr>
           </thead>
           <tbody>
          <?php
          $listados = cuota_prestamo::cuotas_a_cobrar_24122018($fecha_desde);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['prestamo_id']; ?></td>
              <td><?php echo $item['cupon_id']; ?></td>
              <td><?php echo $item['numero_cuota']; ?></td>
              <td><?php echo $item['fecha_liquidacion']; ?></td>
              <td><?php echo $item['monto_prestamo']; ?></td>
              <td><?php echo $item['interes_mas_iva']; ?></td>
              <td><?php echo $item['gastos_adm_con_iva']; ?></td>
               <td><?php echo $i=$item['gastos_adm_con_iva'] + $item['interes_mas_iva']; ?></td>
              <td><?php echo $item['total_facturado']; ?></td>
          </tr>
          <?php
           }
          ?>
            
          </tbody>
         </table>
  <?
} else echo 'error';
 ?>