<?php
include("../sesion.php");
include("cuota_prestamo.php");
if( isset($_POST['fecha_desde']) && !empty($_POST['fecha_hasta']) )
{
$fecha_desde=$_POST['fecha_desde'];
$fecha_hasta=$_POST['fecha_hasta'];
$t_monto_prestamo=0;
$t_total_facturado=0;
$t_interes_mas_iva=0;
$t_gastos_adm_con_iva=0;

$listados = cuota_prestamo::cuotas_dias2($fecha_desde,$fecha_hasta);
foreach($listados as $item)
  {
    $t_monto_prestamo=$t_monto_prestamo+$item['monto_prestamo'];
    $t_interes_mas_iva=$t_interes_mas_iva+$item['interes_mas_iva'];
    $t_gastos_adm_con_iva=$t_gastos_adm_con_iva+$item['gastos_adm_con_iva'];
    $t_total_facturado=$t_total_facturado+$item['total_facturado'];
  }
  ?>


   <table id="listado" class="table table-striped table-bordered table-hover table-condensed" style="font-size: 18px" >
          <thead>
             <tr>
             	<td>Total Monto Prestamo : </td>
             	<td><? 
                     $numero =$t_monto_prestamo;// 1002002.365;
                     echo '$ '.number_format($numero, 2, ",", ".");
               ?></td>
             </tr>
             <tr>
             	<td>Total Interes mas Iva : </td>
             	<td><?php
                     $numero =$t_interes_mas_iva;
                     echo '$ '.number_format($numero, 2, ",", ".");
               ?> </td>
             </tr>
             <tr>
             	<td>Total Gastos Adm con Iva : </td>
             	<td> <?
                     $numero =$t_gastos_adm_con_iva;
                     echo '$ '.number_format($numero, 2, ",", ".");
                      ?></td>
             </tr>
             <tr>
             	<td>Total Liquidación : </td>
             	<td><?php
                     $numero =$t_total_facturado;
                     echo '$ '.number_format($numero, 2, ",", ".");
                ?> </td>
             </tr>
            </thead>
            </table> 
   
  <?
} else echo 'error';
 ?>