<?php
include("../sesion.php");
include("cuota_prestamo.php");
if( isset($_POST['fecha_desde']) && !empty($_POST['fecha_hasta']) )
{
$fecha_desde=$_POST['fecha_desde'];
$fecha_hasta=$_POST['fecha_hasta'];
$tarjeta_id=$_POST['tarjeta'];
$total=0;
?>
 
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Cliente </th>
             <th>Tarjeta de Credito </th>
             <th>Nº Cupon </th>
             <th>Nº Cuota</th>
             <th>Monto</th>
             <th>Fecha Liquidacion</th>
             </tr>
           </thead>
           <tbody>
          <?php
          $listados = cuota_prestamo::cuotas_dias($fecha_desde,$fecha_hasta,$tarjeta_id);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['prestamo_id']; ?></td>
              <td><?php echo $item['cliente_nombre']; ?></td>
              <td><?php echo $item['tarjeta_nombre']; ?></td>
              <td><?php echo $item['cupon_id']; ?></td>
              <td><?php echo $item['numero_cuota']; ?></td>
              <td><?php
                       echo $item['monto'];
                       $total=$total+$item['monto'];
                    ?></td>
              <td><?php echo $item['fecha_liquidacion']; ?></td>
          </tr>
          <?php
           }
          ?>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>Total : </td>
              <td><?php echo $total; ?></td>
              <td></td>
          </tbody>
         </table>
 <?
} else echo 'error';
 ?>