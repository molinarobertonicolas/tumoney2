<?php
include("../sesion.php");
include("../menu.php");
include("cuota_prestamo.php");
?>
 <div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 col-md-8">
    <h2>Cuotas por Prestamos con Tarjeta de Credito</h2>
    <br>

      <div class="row">
        <div class="col-md-12">
        <div class="panel panel-primary">
         <div class="panel-body">
           
            <form class="form-inline" role="form">
              <div class="form-group">
               <label>Tarjeta</label>
                  <select class="form-control" name="tarjeta" id="tarjeta" tabindex="1" >
                     <option>Seleccionar ...</option>
                    <?
                    $listado = cuota_prestamo::listatarjetas();
                    foreach($listado as $item)
                    {
                    ?>
                   
                    <option value="<? echo $item['id'];?>"> <?echo $item['nombre'];?> </option>
                    <?
                     }
                    ?>
                  </select>
                
              </div>
              <div class="form-group">
                <label >Fecha Desde</label>
                <input type="date" class="form-control" id="fecha_desde"
                       placeholder="Fecha Desde" tabindex="2">
              </div>
              <div class="form-group">
                <label >Fecha Hasta</label>
                <input type="date" class="form-control" id="fecha_hasta" 
                       placeholder="Fecha Hasta" tabindex="3">
              </div>

              <button type="button"  id="buscar_fecha" class="btn btn-primary pull-right" tabindex="4"> Buscar </button>
            </form>
        </div>
      </div>
    </div>


   <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-body">
      <div id="div_dinamico">
        
      </div>
    </div>
  </div>
 </div>

  <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Cliente </th>
             <th>Tarjeta de Credito </th>
             <th>Nº Cupon </th>
             <th>Nº Cuota</th>
             <th>Monto</th>
             <th>Fecha Liquidacion</th>
             </tr>
           </thead>
           <tbody>
          <?php
          //$objecto = new PrestamoTarjeta();
          $listados = cuota_prestamo::lista();
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['prestamo_id']; ?></td>
              <td><?php echo $item['cliente_nombre']; ?></td>
              <td><?php echo $item['tarjeta_nombre']; ?></td>
              <td><?php echo $item['cupon_id']; ?></td>
              <td><?php echo $item['numero_cuota']; ?></td>
              <td><?php echo $item['monto']; ?></td>
              <td><?php echo $item['fecha_liquidacion']; ?></td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
        
 </div>
 </div> 
 </div>      

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
    $('#buscar_fecha').click(function(){
        var f_desde=$('#fecha_desde').val();
        var f_hasta=$('#fecha_hasta').val();
        var v_tarjeta=$('#tarjeta').val();
        $.post("cuotas_por_fechas.php", {fecha_desde: f_desde, fecha_hasta: f_hasta, tarjeta:v_tarjeta}, function(mensaje) {
                  $("#div_dinamico").html(mensaje);
              }); 
    });
 });
</script>
</body>
</html>