<?
include_once("../bd/conexion.php");
class Cuota_Prestamo
{
  
  public function lista()
  { 
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto` AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre,
              cliente.`nombre` AS cliente_nombre 
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                      INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)
                      LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      ORDER BY prestamo_id DESC, numero_cuota DESC";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function listatarjetas()
  { 
    $consulta="SELECT id,nombre from tarjeta order by nombre desc";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }


  public function insertar_cuota($prestamo_id,$tarjeta_id,$cupon_id,$cliente_id,$monto,$fecha_liquidacion,$numero_cuota)
  {
    $sql="INSERT INTO `cuota_prestamo`
            ( `prestamo_id`,
             `tarjeta_id`,
             `cupon_id`,
             `cliente_id`,
             `monto`,
             `fecha_liquidacion`,
             `numero_cuota`)
              VALUES ('$prestamo_id',
                      '$tarjeta_id',
                      '$cupon_id',
                      '$cliente_id',
                      '$monto',
                      '$fecha_liquidacion',
                      '$numero_cuota');";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
  }

  //listar todo los prestamos
  public function traer_prestamos()
  {
    $consulta="SELECT
                `id`,
                `cupon_id`,
                `fechahora`
                 FROM `prestamo_tarjeta` where fechahora >= '2018-01-01 06:00:00' and estado <> 'Cancelado'  order by fechahora desc ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function traer_cupon($cupon_id)
  {
    $consulta="SELECT * FROM `cupondetarjeta` where id=$cupon_id ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function traer_dias_tarjeta($tarjeta_id)
  {
    $data[]="";
    $consulta="SELECT diaspago as cantidad_dias FROM `tarjeta` where id=$tarjeta_id";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function cuotas_dias($fecha_desde,$fecha_hasta,$tarjeta_id)
  {
    $data=array();
    /*$99consulta="SELECT * FROM `cuota_prestamo` WHERE tarjeta_id='$tarjeta_id' and `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";*/

    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto` AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre,
              cliente.`nombre` AS cliente_nombre 
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)   
                  LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      WHERE tarjeta_id='$tarjeta_id' and `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function cuotas_dias2($fecha_desde,$fecha_hasta)
  {
    $data=array();
    /*$99consulta="SELECT * FROM `cuota_prestamo` WHERE tarjeta_id='$tarjeta_id' and `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";*/

    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              cuota_prestamo.`monto` AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre,
              cliente.`nombre` AS cliente_nombre 
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)   
                  LEFT JOIN `cliente` 
                      ON (`cuota_prestamo`.`cliente_id` = `cliente`.`id`)
                      WHERE `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha_liquidacion DESC";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  // consulta para sacar los totales por dia
  

  public function subtotales_por_dia($fecha_desde,$fecha_hasta)
  {
    $data=array();
    $consulta="SELECT 
              cuota_prestamo.`cupon_id` AS cupon_id,
              cuota_prestamo.`fecha_liquidacion` AS fecha_liquidacion,
              SUM(cuota_prestamo.`monto`) AS monto,
              cuota_prestamo.`numero_cuota` AS numero_cuota,
              cuota_prestamo.`prestamo_id` AS prestamo_id,
              tarjeta.`nombre` AS tarjeta_nombre              
              FROM `cuota_prestamo` 
              INNER JOIN `prestamo_tarjeta` 
                      ON (`cuota_prestamo`.`prestamo_id` = `prestamo_tarjeta`.`id`)
                   INNER JOIN `tarjeta` 
                      ON (`cuota_prestamo`.`tarjeta_id` = `tarjeta`.`id`)     
                      WHERE `fecha_liquidacion` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE)
                      GROUP BY fecha_liquidacion ";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }                  




}
?>