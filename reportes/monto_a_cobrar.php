<?php
include("../sesion.php");
include("../menu.php");
include("cuota_prestamo.php");
?>
 <div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 col-md-8">
    <h3>Monto Total a Cobrar </h3>
    <h4>Sumatoria de todas las cuotas que se cobraran despues de la fecha de inicio del reporte.</h4> 
    <br>

      <div class="row">
        <div class="col-md-12">
        <div class="panel panel-primary">
         <div class="panel-body">
           
            <form class="form-inline" role="form">
              <div class="form-group">
                <label >Fecha Inicio del Reporte</label>
                <input type="date" class="form-control" id="fecha_desde"
                       placeholder="Fecha Desde" tabindex="1">
              </div>
              <button type="button"  id="buscar_fecha" class="btn btn-primary pull-right" tabindex="3"> Buscar </button>
            </form>
        </div>
      </div>
    </div>


   <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-body">
      <div id="div_dinamico">
        
      </div>
    </div>
  </div>
 </div>
        
 </div>
 </div> 
 </div>      

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
    $('#buscar_fecha').click(function(){
        var f_desde=$('#fecha_desde').val();
        var f_hasta=$('#fecha_hasta').val();
        $.post("cuotas_a_cobrar.php", {fecha_desde: f_desde, fecha_hasta: f_hasta}, function(mensaje) {
                  $("#div_dinamico").html(mensaje);
              }); 
    });
 });
</script>
</body>
</html>