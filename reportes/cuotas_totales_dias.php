<?php
include("../sesion.php");
include("cuota_prestamo.php");
if( isset($_POST['fecha_desde']) && !empty($_POST['fecha_hasta']) )
{
$fecha_desde=$_POST['fecha_desde'];
$fecha_hasta=$_POST['fecha_hasta'];
$total=0;
?>
  <table id="listado" class="table table-striped table-bordered table-hover table-condensed" style="font-size: 18px" >
    <thead>
             <tr>
             <th>Dia</th>
             <th>Monto </th>
             </tr>
    </thead>
    <tbody>
      <?php
          $listados = cuota_prestamo::subtotales_por_dia($fecha_desde,$fecha_hasta);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td>
                <?php echo $item['fecha_liquidacion']; ?>
              </td>
              <td><?php
                       echo '$ '.number_format($item['monto'], 2, ",", ".");
                       $total=$total+$item['monto'];
                    ?></td>
          </tr>
          <?php
           }
          ?>
          <tr>
              <td>Total : </td>
              <td><?php echo '$ '.number_format($total, 2, ",", "."); ?></td>
          </tr>
          </tbody>
         </table>
 <?
} else echo 'error';
 ?>