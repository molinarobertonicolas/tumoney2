-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `american_express`
--

CREATE TABLE `american_express` (
  `id` int(2) NOT NULL,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `american_express`
--

INSERT INTO `american_express` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, '1.0528'),
(2, 3, '1.0717'),
(3, 4, '1.0907'),
(4, 5, '1.1100'),
(5, 6, '1.1295'),
(6, 7, '1.1598'),
(7, 8, '1.1812'),
(8, 9, '1.2028'),
(9, 10, '1.2247'),
(10, 11, '1.2469'),
(11, 12, '1.2693'),
(12, 13, '1.2985'),
(13, 14, '1.3320'),
(14, 15, '1.3457'),
(15, 16, '1.3696'),
(16, 17, '1.3938'),
(17, 18, '1.4182'),
(18, 19, '1.4429'),
(19, 20, '1.4671'),
(20, 21, '1.4930'),
(21, 22, '1.5185'),
(22, 23, '1.5189'),
(23, 24, '1.5700'),
(24, 25, '1.5962'),
(25, 26, '1.5934'),
(26, 27, '1.6187'),
(27, 28, '1.6442'),
(28, 29, '1.6699'),
(29, 30, '1.6958'),
(30, 31, '1.7219'),
(31, 32, '1.7482'),
(32, 33, '1.7748'),
(33, 34, '1.8015'),
(34, 35, '1.8285'),
(35, 36, '1.8556'),
(36, 37, '1.8830'),
(37, 38, '1.9105'),
(38, 39, '1.9383'),
(39, 40, '1.9662'),
(40, 41, '1.9944'),
(41, 42, '2.0227'),
(42, 43, '2.0512'),
(43, 44, '2.0799'),
(44, 45, '2.1088'),
(45, 46, '2.1379'),
(46, 47, '2.1671'),
(47, 48, '2.1965'),
(48, 49, '2.2261'),
(49, 50, '2.2559');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajadetalle`
--

CREATE TABLE `cajadetalle` (
  `id` int(8) NOT NULL,
  `cajanueva_id` int(8) DEFAULT NULL,
  `operacion` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `monto` decimal(8,2) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `detalle` varchar(60) DEFAULT NULL,
  `usuario_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cajadetalle`
--

INSERT INTO `cajadetalle` (`id`, `cajanueva_id`, `operacion`, `tipo`, `monto`, `fechahora`, `detalle`, `usuario_id`) VALUES
(10, 0, 'Prestamo', 'Egreso', '100000.00', '2021-08-24 18:44:46', 'Prestamo', 1),
(11, 0, 'Prestamo', 'Egreso', '20000.00', '2021-09-13 17:49:35', 'Prestamo', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajanueva`
--

CREATE TABLE `cajanueva` (
  `id` int(6) NOT NULL,
  `inicio` decimal(8,2) DEFAULT NULL,
  `fechaapertura` datetime DEFAULT NULL,
  `cierre` decimal(8,2) DEFAULT NULL,
  `fechacierre` datetime DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL COMMENT 'Abierto o Cerrado',
  `saldo` decimal(8,2) DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '2' COMMENT '1 centro 2 rawson'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `dni` varchar(15) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `cuit` varchar(20) DEFAULT NULL,
  `domicilio` varchar(255) DEFAULT NULL,
  `departamento` varchar(50) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `email` varchar(90) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `dni`, `nombre`, `cuit`, `domicilio`, `departamento`, `telefono`, `fecha_ingreso`, `email`, `fecha_nacimiento`, `sucursal_id`) VALUES
(1, '35049781', 'Prueba', '35049781', 'domicilio', 'SANTA LUCIA', '2645555', '2021-07-29', 'b@gmail.com', '0000-00-00', 2),
(2, '19191919', 'Segunda prueba', '19191919', 'barrio', 'CAUCETE', '(264)555-5555', '2021-08-03', 'b@gggg.com', '0000-00-00', 2),
(3, '8328515', 'Jorge ', '20265114696', 'Granaderos 942 Casa 4 Rivadavia BÂ° Sargento Cabral', 'ALBARDON', '02466738030', '2021-08-03', 'lucaspe7477@gmail.com', '0000-00-00', 2),
(4, '', 'Lucas Caceres', '', '', 'ALBARDON', '', '2021-08-03', '', '0000-00-00', 2),
(5, '38218632', 'lucas caceres', '20382186320', 'suipacha 350 sur ', 'CAPITAL', '2644711046', '2021-08-03', '', '0000-00-00', 2),
(6, '22705501', 'mauricio prueba', '20227055015', 's/d', 'ALBARDON', '2645056361', '2021-08-25', '', '2021-05-12', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranza`
--

CREATE TABLE `cobranza` (
  `id` int(6) NOT NULL,
  `prestamo_id` int(6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `aranceltarjeta` decimal(8,2) DEFAULT NULL,
  `ivaarancel` decimal(8,2) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL,
  `ivaintereses` decimal(8,2) DEFAULT NULL,
  `gastostarj1` decimal(8,2) DEFAULT NULL,
  `ivagtos1` decimal(8,2) DEFAULT NULL,
  `gastostarj2` decimal(8,2) DEFAULT NULL,
  `ivagtos2` decimal(8,2) DEFAULT NULL,
  `retiibb` decimal(8,2) DEFAULT NULL,
  `retlh` decimal(8,2) DEFAULT NULL,
  `retiva` decimal(8,2) DEFAULT NULL,
  `retgan` decimal(8,2) DEFAULT NULL,
  `acobrar` decimal(8,2) DEFAULT NULL,
  `sellos` decimal(8,2) DEFAULT NULL,
  `lotehogar` decimal(8,2) DEFAULT NULL,
  `neto` decimal(8,2) DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cobranza`
--

INSERT INTO `cobranza` (`id`, `prestamo_id`, `totalcupon`, `aranceltarjeta`, `ivaarancel`, `subtotal`, `interesestarjeta`, `ivaintereses`, `gastostarj1`, `ivagtos1`, `gastostarj2`, `ivagtos2`, `retiibb`, `retlh`, `retiva`, `retgan`, `acobrar`, `sellos`, `lotehogar`, `neto`, `sucursal_id`) VALUES
(10, 10, '225645.89', '6769.38', '1421.57', '217454.94', '46484.89', '9761.83', '0.00', '0.00', '0.00', '0.00', '5436.37', '1087.27', '6523.65', '2174.55', '145986.38', '2919.73', '583.95', '142482.70', 2),
(11, 11, '41451.89', '1243.56', '261.15', '39947.18', '5619.69', '1180.13', '0.00', '0.00', '0.00', '0.00', '998.68', '199.74', '1198.42', '399.47', '30351.05', '607.02', '121.40', '29622.63', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comollego`
--

CREATE TABLE `comollego` (
  `id` int(4) NOT NULL,
  `comollego` varchar(85) DEFAULT NULL COMMENT 'pasaba - radio - folleto- facbock',
  `fecha` date DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuota_prestamo`
--

CREATE TABLE `cuota_prestamo` (
  `id` int(11) NOT NULL,
  `prestamo_id` int(11) DEFAULT NULL,
  `prestamo_fecha` date DEFAULT NULL,
  `tarjeta_id` int(11) DEFAULT NULL,
  `cupon_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `facturab_id` int(11) DEFAULT NULL,
  `numero_cuota` int(4) DEFAULT NULL,
  `fecha_liquidacion` date DEFAULT NULL,
  `monto_prestamo` decimal(16,2) DEFAULT NULL,
  `interes_mas_iva` decimal(16,2) DEFAULT NULL,
  `gastos_adm_con_iva` decimal(16,2) DEFAULT NULL,
  `total_facturado` decimal(16,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuota_prestamo`
--

INSERT INTO `cuota_prestamo` (`id`, `prestamo_id`, `prestamo_fecha`, `tarjeta_id`, `cupon_id`, `cliente_id`, `facturab_id`, `numero_cuota`, `fecha_liquidacion`, `monto_prestamo`, `interes_mas_iva`, `gastos_adm_con_iva`, `total_facturado`) VALUES
(57, 10, '2021-08-24', 4, 10, 1, 10, 1, '2021-09-24', '15000.00', '2916.10', '6058.24', '23974.34'),
(58, 10, '2021-08-24', 4, 10, 1, 10, 2, '2021-10-25', '15000.00', '2916.10', '6058.24', '23974.34'),
(59, 10, '2021-08-24', 4, 10, 1, 10, 3, '2021-11-25', '15000.00', '2916.10', '6058.24', '23974.34'),
(60, 10, '2021-08-24', 4, 10, 1, 10, 4, '2021-12-26', '15000.00', '2916.10', '6058.24', '23974.34'),
(61, 10, '2021-08-24', 4, 10, 1, 10, 5, '2022-01-26', '15000.00', '2916.10', '6058.24', '23974.34'),
(62, 10, '2021-08-24', 4, 10, 1, 10, 6, '2022-02-26', '15000.00', '2916.10', '6058.24', '23974.34'),
(63, 10, '2021-08-24', 4, 10, 1, 10, 7, '2022-03-29', '15000.00', '2916.10', '6058.24', '23974.34'),
(64, 10, '2021-08-24', 4, 10, 1, 10, 8, '2022-04-29', '15000.00', '2916.10', '6058.24', '23974.34'),
(65, 10, '2021-08-24', 4, 10, 1, 10, 9, '2022-05-30', '15000.00', '2916.10', '6058.24', '23974.34'),
(66, 10, '2021-08-24', 4, 10, 1, 10, 10, '2022-06-30', '15000.00', '2916.10', '6058.24', '23974.34'),
(67, 11, '2021-09-13', 3, 11, 5, 11, 1, '2021-10-14', '5000.00', '972.03', '1277.83', '7249.86'),
(68, 11, '2021-09-13', 3, 11, 5, 11, 2, '2021-11-14', '5000.00', '972.03', '1277.83', '7249.86'),
(69, 11, '2021-09-13', 3, 11, 5, 11, 3, '2021-12-15', '5000.00', '972.03', '1277.83', '7249.86'),
(70, 11, '2021-09-13', 3, 11, 5, 11, 4, '2022-01-15', '5000.00', '972.03', '1277.83', '7249.86'),
(71, 11, '2021-09-13', 3, 11, 5, 11, 5, '2022-02-15', '5000.00', '972.03', '1277.83', '7249.86'),
(72, 11, '2021-09-13', 3, 11, 5, 11, 6, '2022-03-18', '5000.00', '972.03', '1277.83', '7249.86');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupondetarjeta`
--

CREATE TABLE `cupondetarjeta` (
  `id` int(10) NOT NULL,
  `tarjeta_id` int(3) DEFAULT NULL,
  `monto` decimal(8,2) DEFAULT NULL,
  `cuotas` int(3) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `cliente_id` int(7) DEFAULT NULL,
  `numero_cupon` varchar(15) DEFAULT NULL,
  `digitos` varchar(4) NOT NULL COMMENT 'cuatro ultimos digitos de la tarjeta',
  `estado` varchar(15) DEFAULT NULL COMMENT 'nuevo, con prestamo',
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cupondetarjeta`
--

INSERT INTO `cupondetarjeta` (`id`, `tarjeta_id`, `monto`, `cuotas`, `fechahora`, `cliente_id`, `numero_cupon`, `digitos`, `estado`, `sucursal_id`) VALUES
(10, 4, '239743.42', 10, '2021-08-24 18:44:24', 1, '123333', '1234', 'completo', 2),
(11, 3, '43499.15', 6, '2021-09-13 17:49:23', 5, '0645', '7544', 'completo', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleprestamo`
--

CREATE TABLE `detalleprestamo` (
  `id` int(6) NOT NULL,
  `prestamo_id` int(6) DEFAULT NULL,
  `prestamo` decimal(8,2) DEFAULT NULL COMMENT 'monto a pagar por el cliente',
  `intereses` decimal(8,2) DEFAULT NULL,
  `iva` decimal(8,2) DEFAULT NULL,
  `plus` decimal(8,2) DEFAULT NULL,
  `subtotal1` decimal(8,2) DEFAULT NULL,
  `gtosadmin` decimal(8,2) DEFAULT NULL,
  `ivagtos` decimal(8,2) DEFAULT NULL,
  `subtotal2` decimal(8,2) DEFAULT NULL,
  `cuotas` decimal(8,2) DEFAULT NULL,
  `coeficientetarjeta` decimal(8,6) DEFAULT NULL,
  `totalcupon` decimal(8,2) DEFAULT NULL,
  `interesestarjeta` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalleprestamo`
--

INSERT INTO `detalleprestamo` (`id`, `prestamo_id`, `prestamo`, `intereses`, `iva`, `plus`, `subtotal1`, `gtosadmin`, `ivagtos`, `subtotal2`, `cuotas`, `coeficientetarjeta`, `totalcupon`, `interesestarjeta`) VALUES
(10, 10, '239743.42', '24100.00', '5061.00', '50000.00', '179161.00', '3583.22', '752.48', '183496.70', '10.00', '1.229700', '239743.42', '46484.89'),
(11, 11, '43499.15', '4820.00', '1012.20', '10000.00', '35832.20', '716.64', '150.49', '36699.33', '6.00', '1.129500', '43499.15', '5619.69');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pago_pestamos_motos`
--

CREATE TABLE `detalle_pago_pestamos_motos` (
  `id` int(10) NOT NULL,
  `id_pago` int(10) DEFAULT NULL,
  `id_prestamo` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturab`
--

CREATE TABLE `facturab` (
  `id` int(8) NOT NULL,
  `prestamo_id` int(8) DEFAULT NULL,
  `monto_solicitado` decimal(10,2) DEFAULT NULL,
  `plus` decimal(10,2) DEFAULT NULL,
  `monto_prestamo_facturado` decimal(10,2) DEFAULT NULL,
  `interes` decimal(10,2) DEFAULT NULL,
  `iva_interes` decimal(10,2) DEFAULT NULL,
  `interes_mas_iva` decimal(10,2) DEFAULT NULL,
  `gastos_administrativos` decimal(10,2) DEFAULT NULL,
  `interes_tarjeta` decimal(10,2) DEFAULT NULL,
  `gastos_adm_mas_interes` decimal(10,2) DEFAULT NULL,
  `iva_gastos_adm` decimal(10,2) DEFAULT NULL,
  `gastos_adm_con_iva` decimal(10,2) DEFAULT NULL,
  `total_factura_b` decimal(10,2) DEFAULT NULL,
  `factura_numero` varchar(13) NOT NULL COMMENT 'numero de la factura del prestamo',
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturab`
--

INSERT INTO `facturab` (`id`, `prestamo_id`, `monto_solicitado`, `plus`, `monto_prestamo_facturado`, `interes`, `iva_interes`, `interes_mas_iva`, `gastos_administrativos`, `interes_tarjeta`, `gastos_adm_mas_interes`, `iva_gastos_adm`, `gastos_adm_con_iva`, `total_factura_b`, `factura_numero`, `sucursal_id`) VALUES
(10, 10, '100000.00', '50000.00', '150000.00', '24100.00', '5061.00', '29161.00', '3583.22', '46484.89', '50068.11', '10514.30', '60582.41', '239743.41', '', 2),
(11, 11, '20000.00', '10000.00', '30000.00', '4820.00', '1012.20', '5832.20', '716.64', '5619.69', '6336.33', '1330.63', '7666.96', '43499.16', '', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `detalle` varchar(100) DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `comprobante` varchar(100) DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_pres_tarj`
--

CREATE TABLE `gastos_pres_tarj` (
  `id` int(4) NOT NULL,
  `item` varchar(70) DEFAULT NULL,
  `valor` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos_pres_tarj`
--

INSERT INTO `gastos_pres_tarj` (`id`, `item`, `valor`) VALUES
(1, 'Costo Interes', '24.10'),
(2, 'Costo iva', '21.00'),
(3, 'Costo plus', '50.00'),
(4, 'Costos Gastos Administrativos', '2.00'),
(5, 'Costos Sellos', '1.50'),
(6, 'Costo iibb', '2.50'),
(7, 'Costo Lote Hogar', '20.00'),
(8, 'Costo iva 2', '3.00'),
(9, 'Costo Ganancias', '1.00'),
(10, 'Costo Sellos 2', '2.00'),
(11, 'Costo Adicional Lote Hogar', '20.00'),
(12, 'Parametro - CFT', '127.00'),
(13, 'Parametro - TNA', '90.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `usuario_id` int(2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `nombre` varchar(120) DEFAULT NULL,
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id`, `usuario_id`, `fecha`, `hora`, `ip`, `nombre`, `sucursal_id`) VALUES
(17, 1, '2021-08-24', '10:08:35', '128.201.83.161', 'rmolina', 2),
(18, 1, '2021-08-24', '17:32:41', '128.201.83.161', 'rmolina', 2),
(19, 1, '2021-08-25', '10:32:54', '186.56.175.29', 'rmolina', 2),
(20, 1, '2021-08-30', '17:28:36', '128.201.83.161', 'rmolina', 2),
(21, 1, '2021-09-03', '19:27:57', '128.201.83.161', 'rmolina', 2),
(22, 1, '2021-09-03', '19:32:38', '128.201.83.161', 'rmolina', 2),
(23, 1, '2021-09-07', '19:20:47', '128.201.83.161', 'rmolina', 2),
(24, 5, '2021-09-13', '17:30:40', '186.58.149.155', 'Jorge2017', 2),
(25, 5, '2021-09-23', '09:34:02', '186.58.251.30', 'Jorge2017', 2),
(26, 6, '2021-10-04', '18:46:38', '128.201.83.161', 'operador', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mastercard`
--

CREATE TABLE `mastercard` (
  `id` int(2) NOT NULL,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mastercard`
--

INSERT INTO `mastercard` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, '1.0528'),
(2, 3, '1.0717'),
(3, 4, '1.0907'),
(4, 5, '1.1100'),
(5, 6, '1.1295'),
(6, 7, '1.1598'),
(7, 8, '1.1812'),
(8, 9, '1.2028'),
(9, 10, '1.2247'),
(10, 11, '1.2530'),
(11, 12, '1.2755'),
(12, 13, '1.2985'),
(13, 14, '1.3320'),
(14, 15, '1.3457'),
(15, 16, '1.3696'),
(16, 17, '1.3938'),
(17, 18, '1.4182'),
(18, 19, '1.4429'),
(19, 20, '1.4671'),
(20, 21, '1.4930'),
(21, 22, '1.5185'),
(22, 23, '1.5189'),
(23, 24, '1.5700'),
(24, 25, '1.5962'),
(25, 26, '1.5934'),
(26, 27, '1.6187'),
(27, 28, '1.6442'),
(28, 29, '1.6699'),
(29, 30, '1.6958'),
(30, 31, '1.7219'),
(31, 32, '1.7482'),
(32, 33, '1.7748'),
(33, 34, '1.8015'),
(34, 35, '1.8285'),
(35, 36, '1.8556'),
(36, 37, '1.8830'),
(37, 38, '1.9105'),
(38, 39, '1.9383'),
(39, 40, '1.9662'),
(40, 41, '1.9944'),
(41, 42, '2.0227'),
(42, 43, '2.0512'),
(43, 44, '2.0799'),
(44, 45, '2.1088'),
(45, 46, '2.1379'),
(46, 47, '2.1671'),
(47, 48, '2.1965'),
(48, 49, '2.2261'),
(49, 50, '2.2559');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `naranja`
--

CREATE TABLE `naranja` (
  `id` int(2) NOT NULL,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `naranja`
--

INSERT INTO `naranja` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, '1.0528'),
(2, 3, '1.0717'),
(3, 4, '1.0907'),
(4, 5, '1.1100'),
(5, 6, '1.1295'),
(6, 7, '1.1598'),
(7, 8, '1.1812'),
(8, 9, '1.2028'),
(9, 10, '1.2247'),
(10, 11, '1.2530'),
(11, 12, '1.2755'),
(12, 13, '1.2985'),
(13, 14, '1.3320'),
(14, 15, '1.3457'),
(15, 16, '1.3696'),
(16, 17, '1.3938'),
(17, 18, '1.4182'),
(18, 19, '1.4429'),
(19, 20, '1.4671'),
(20, 21, '1.4930'),
(21, 22, '1.5185'),
(22, 23, '1.5189'),
(23, 24, '1.5700'),
(24, 25, '1.5962'),
(25, 26, '1.5934'),
(26, 27, '1.6187'),
(27, 28, '1.6442'),
(28, 29, '1.6699'),
(29, 30, '1.6958'),
(30, 31, '1.7219'),
(31, 32, '1.7482'),
(32, 33, '1.7748'),
(33, 34, '1.8015'),
(34, 35, '1.8285'),
(35, 36, '1.8556'),
(36, 37, '1.8830'),
(37, 38, '1.9105'),
(38, 39, '1.9383'),
(39, 40, '1.9662'),
(40, 41, '1.9944'),
(41, 42, '2.0227'),
(42, 43, '2.0512'),
(43, 44, '2.0799'),
(44, 45, '2.1088'),
(45, 46, '2.1379'),
(46, 47, '2.1671'),
(47, 48, '2.1965'),
(48, 49, '2.2261'),
(49, 50, '2.2559');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_prestamos_motos`
--

CREATE TABLE `pago_prestamos_motos` (
  `id` int(10) NOT NULL,
  `monto` decimal(20,2) DEFAULT NULL COMMENT 'total de los monto de los prestamos',
  `fecha_pago` date DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL COMMENT 'forma de pago'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo_tarjeta`
--

CREATE TABLE `prestamo_tarjeta` (
  `id` int(11) NOT NULL,
  `cupon_id` int(10) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `pdf_dni` varchar(70) DEFAULT NULL,
  `montoprestamo` decimal(14,2) DEFAULT NULL COMMENT 'monto prestado',
  `monto_apagar` decimal(14,2) DEFAULT NULL COMMENT 'monto a devolver por el cliente',
  `cft` int(2) NOT NULL,
  `tna` int(2) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `sucursal_id` int(4) DEFAULT '2',
  `rendido` varchar(15) DEFAULT 'NO',
  `fecha_otorgado` date DEFAULT NULL,
  `fecha_cancelado` date DEFAULT NULL,
  `documentacion` varchar(15) DEFAULT 'NO' COMMENT 'no presentada o si presentada'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prestamo_tarjeta`
--

INSERT INTO `prestamo_tarjeta` (`id`, `cupon_id`, `fechahora`, `pdf_dni`, `montoprestamo`, `monto_apagar`, `cft`, `tna`, `estado`, `sucursal_id`, `rendido`, `fecha_otorgado`, `fecha_cancelado`, `documentacion`) VALUES
(10, 10, '2021-08-24 18:44:46', 'dni_357457152u.png', '100000.00', '0.00', 127, 90, '', 0, 'Rendido', '2021-08-24', '0000-00-00', 'SI'),
(11, 11, '2021-09-13 17:49:35', 'dni_884514429.jpeg', '20000.00', '0.00', 127, 90, '', 0, 'Rendido', '2021-09-13', '0000-00-00', 'NO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(4) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `domicilio` varchar(60) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `nombre`, `domicilio`, `fecha_inicio`) VALUES
(1, 'Centro', 'Av Libertador', '2015-04-01'),
(2, 'Rawson', NULL, '2019-08-20'),
(3, 'Varias', 'Prestamos Motos', '2021-07-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE `tarjeta` (
  `id` int(3) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `alias_sistema` varchar(56) NOT NULL COMMENT 'alias del nombre para el sistema',
  `coeficiente` varchar(50) DEFAULT NULL COMMENT 'nombre de la tabla',
  `arancel` decimal(5,2) DEFAULT NULL,
  `gastos1` decimal(6,2) DEFAULT NULL,
  `gastos2` decimal(6,2) DEFAULT NULL,
  `gastos3` decimal(6,2) DEFAULT NULL,
  `diaspago` int(2) NOT NULL COMMENT 'la cantidad de dias hasta recivir el pago'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tarjeta`
--

INSERT INTO `tarjeta` (`id`, `nombre`, `alias_sistema`, `coeficiente`, `arancel`, `gastos1`, `gastos2`, `gastos3`, `diaspago`) VALUES
(2, 'Tarjeta Data', 'visa', 'visa', '3.00', '0.00', '0.00', '0.00', 31),
(3, 'Mastercard', 'mastercard', 'mastercard', '3.00', '0.00', '0.00', '0.00', 31),
(4, 'Visa', 'visa', 'visa', '3.00', '0.00', '0.00', '0.00', 31),
(5, 'American Express', 'american_express', 'american_express', '3.00', '0.00', '0.00', '0.00', 31),
(6, 'Cabal', 'cabal', 'visa', '3.00', '0.00', '0.00', '0.00', 31),
(7, 'Naranja', 'naranja', 'naranja', '3.00', '0.00', '0.00', '0.00', 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tp`
--

CREATE TABLE `tp` (
  `id` int(2) NOT NULL,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL COMMENT 'coefiente segun cuota'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tp`
--

INSERT INTO `tp` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, '1.0528'),
(2, 3, '1.0717'),
(3, 4, '1.0907'),
(4, 5, '1.1100'),
(5, 6, '1.1295'),
(6, 7, '1.1633'),
(7, 8, '1.1852'),
(8, 9, '1.2073'),
(9, 10, '1.2297'),
(10, 11, '1.2524'),
(11, 12, '1.2753'),
(12, 13, '1.2985'),
(13, 14, '1.3220'),
(14, 15, '1.3457'),
(15, 16, '1.3696'),
(16, 17, '1.3938'),
(17, 18, '1.4182'),
(18, 19, '1.4429'),
(19, 20, '1.4679'),
(20, 21, '1.4930'),
(21, 22, '1.5185'),
(22, 23, '1.5441'),
(23, 24, '1.5700'),
(24, 25, '1.5962'),
(25, 26, '1.5934'),
(26, 27, '1.6187'),
(27, 28, '1.6442'),
(28, 29, '1.6699'),
(29, 30, '1.6958'),
(30, 31, '1.7219'),
(31, 32, '1.7482'),
(32, 33, '1.7748'),
(33, 34, '1.8015'),
(34, 35, '1.8285'),
(35, 36, '1.8556'),
(36, 37, '1.8830'),
(37, 38, '1.9105'),
(38, 39, '1.9383'),
(39, 40, '1.9662'),
(40, 41, '1.9944'),
(41, 42, '2.0227'),
(42, 43, '2.0512'),
(43, 44, '2.0799'),
(44, 45, '2.1088'),
(45, 46, '2.1379'),
(46, 47, '2.1671'),
(47, 48, '2.1956'),
(48, 49, '2.2261'),
(49, 50, '2.2559');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(2) NOT NULL,
  `usuario` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` varchar(150) CHARACTER SET latin1 NOT NULL,
  `nombrereal` varchar(25) CHARACTER SET latin1 NOT NULL,
  `nivel` int(2) NOT NULL COMMENT '1-administrador 2-empleado',
  `sucursal_id` int(4) DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `nombrereal`, `nivel`, `sucursal_id`) VALUES
(1, 'rmolina', '202cb962ac59075b964b07152d234b70', 'Administrador', 1, 2),
(5, 'Jorge2017', 'd1de2abf6519f5e0e027c7cda927d2b1', 'Jorge Adm.', 1, 2),
(6, 'operador', 'a96f6caed9d005e31fa0b9972def19a1', 'Operador1', 2, 2),
(9, 'Juan', 'c1602ccfa27d591bfadd3bf9cf345bd2', 'Juan', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visa`
--

CREATE TABLE `visa` (
  `id` int(2) NOT NULL,
  `cuota` int(2) DEFAULT NULL,
  `coeficiente` decimal(8,4) DEFAULT NULL COMMENT 'coefiente segun cuota'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `visa`
--

INSERT INTO `visa` (`id`, `cuota`, `coeficiente`) VALUES
(1, 2, '1.0528'),
(2, 3, '1.0717'),
(3, 4, '1.0907'),
(4, 5, '1.1100'),
(5, 6, '1.1295'),
(6, 7, '1.1633'),
(7, 8, '1.1852'),
(8, 9, '1.2073'),
(9, 10, '1.2297'),
(10, 11, '1.2524'),
(11, 12, '1.2753'),
(12, 13, '1.2985'),
(13, 14, '1.3220'),
(14, 15, '1.3457'),
(15, 16, '1.3696'),
(16, 17, '1.3938'),
(17, 18, '1.4182'),
(18, 19, '1.4429'),
(19, 20, '1.4679'),
(20, 21, '1.4930'),
(21, 22, '1.5185'),
(22, 23, '1.5441'),
(23, 24, '1.5700'),
(24, 25, '1.5962'),
(25, 26, '1.5934'),
(26, 27, '1.6187'),
(27, 28, '1.6442'),
(28, 29, '1.6699'),
(29, 30, '1.6958'),
(30, 31, '1.7219'),
(31, 32, '1.7482'),
(32, 33, '1.7748'),
(33, 34, '1.8015'),
(34, 35, '1.8285'),
(35, 36, '1.8556'),
(36, 37, '1.8830'),
(37, 38, '1.9105'),
(38, 39, '1.9383'),
(39, 40, '1.9662'),
(40, 41, '1.9944'),
(41, 42, '2.0227'),
(42, 43, '2.0512'),
(43, 44, '2.0799'),
(44, 45, '2.1088'),
(45, 46, '2.1379'),
(46, 47, '2.1671'),
(47, 48, '2.1956'),
(48, 49, '2.2261'),
(49, 50, '2.2559');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `american_express`
--
ALTER TABLE `american_express`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cajadetalle`
--
ALTER TABLE `cajadetalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cajanueva`
--
ALTER TABLE `cajanueva`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cobranza`
--
ALTER TABLE `cobranza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comollego`
--
ALTER TABLE `comollego`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuota_prestamo`
--
ALTER TABLE `cuota_prestamo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cupondetarjeta`
--
ALTER TABLE `cupondetarjeta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalleprestamo`
--
ALTER TABLE `detalleprestamo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_pago_pestamos_motos`
--
ALTER TABLE `detalle_pago_pestamos_motos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facturab`
--
ALTER TABLE `facturab`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos_pres_tarj`
--
ALTER TABLE `gastos_pres_tarj`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mastercard`
--
ALTER TABLE `mastercard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `naranja`
--
ALTER TABLE `naranja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_prestamos_motos`
--
ALTER TABLE `pago_prestamos_motos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prestamo_tarjeta`
--
ALTER TABLE `prestamo_tarjeta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tp`
--
ALTER TABLE `tp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visa`
--
ALTER TABLE `visa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `american_express`
--
ALTER TABLE `american_express`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `cajadetalle`
--
ALTER TABLE `cajadetalle`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `cajanueva`
--
ALTER TABLE `cajanueva`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cobranza`
--
ALTER TABLE `cobranza`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comollego`
--
ALTER TABLE `comollego`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuota_prestamo`
--
ALTER TABLE `cuota_prestamo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `cupondetarjeta`
--
ALTER TABLE `cupondetarjeta`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `detalleprestamo`
--
ALTER TABLE `detalleprestamo`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `detalle_pago_pestamos_motos`
--
ALTER TABLE `detalle_pago_pestamos_motos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `facturab`
--
ALTER TABLE `facturab`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gastos_pres_tarj`
--
ALTER TABLE `gastos_pres_tarj`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `mastercard`
--
ALTER TABLE `mastercard`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `naranja`
--
ALTER TABLE `naranja`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `pago_prestamos_motos`
--
ALTER TABLE `pago_prestamos_motos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prestamo_tarjeta`
--
ALTER TABLE `prestamo_tarjeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tp`
--
ALTER TABLE `tp`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `visa`
--
ALTER TABLE `visa`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
