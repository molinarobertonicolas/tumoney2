<?php
include("../sesion.php");
include("../menu.php");
include("conciliacion.php");
include("../tarjeta/tarjeta.php");
$listaTarjeta = tarjeta::lista();



function porcentaje($cantidad,$porciento)
{
 //sin separador de miles y dos decimales separados por el .
 return number_format($cantidad*$porciento/100 ,2,".", "");
}


if( isset($_POST['selectTarjeta']) && !empty($_POST['selectTarjeta']) )
 {
  	$tarjeta = $_POST['selectTarjeta'];
    $fecha_desde = $_POST['fecha_desde'];
    $fecha_hasta = $_POST['fecha_hasta'];
    
    $objetos=tarjeta::nombreTarjeta($tarjeta);

     foreach($objetos as $item)
          {
           $nombreTarjeta=$item ['nombre'];
         }

	?>
<div class="container">
<p>Tarjeta : <? echo $nombreTarjeta;?><br>
   Fecha Desde :<? echo $fecha_desde;?>
   Fecha Hasta :<? echo $fecha_hasta;?>
</p>

	<table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Fecha</th>
             <th>N° Cupon</th>
             <th>Cuotas</th>
             <th>Monto</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $i=0;
          $totalgeneral=0; 
         $objecto = new Conciliacion();
	$datos = $objecto->listacupon($tarjeta,$fecha_desde,$fecha_hasta);
          foreach($datos as $item)
          {
          ?>
           <tr>
              <td><?php echo $i++; ?></td>
              <td><?php echo $item ['fecha']; ?></td>
              <td><?php echo $item ['numerocupon']; ?></td>
              <td><?php echo $item ['cuotas']; ?></td>
              <td><?php echo $item ['monto'];
                    $totalgeneral=$totalgeneral+ $item ['monto'];
                   
               ?></td>
              
             
         
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </br>

        <h3>Detalle Liquidación</h3>
 <div class="row">
  
 <div class="col-md-6">  
<table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
<tr>
  <td>Total a Pagar</td>
  <td><?php echo $totalgeneral; ?></td>
  
</tr>
 
<tr>
  <td>Descuento</td>
  <td><? echo $c_Descuento=porcentaje($totalgeneral,3);?></td>

</tr>
 
<tr>
  <td>Convenio</td>
  <td><? echo $c_Convenio=porcentaje($totalgeneral,0);?></td>
  
</tr>
<tr>
  <td>Bonificación</td>
   <td><? echo $c_Bonificacion=porcentaje($totalgeneral,0);?></td>
  
</tr>
<tr>
  <td>IVARI</td>
  <td><? echo $c_IVARI=porcentaje($c_Descuento,21);?></td>
</tr>
<tr>
  <td>Descuentos Varios</td>
  <td>0</td>
  
</tr>
<tr>
  <td>SubTotal</td>
  <td><? echo $subtotal=$totalgeneral-($c_Descuento+$c_Convenio+$c_Bonificacion+$c_IVARI)?></td>
  
</tr>
<tr>
  <td>Total Retiva</td>
  <td><? echo $c_Retiva=porcentaje($subtotal,3);?></td>
  
</tr>
<tr>
  <td>Total RetGan</td>
  <td><? echo $c_RetGan=porcentaje($subtotal,1);?></td>
  
</tr>
<tr>
  <td>Ing. Brutos</td>
  <td><? echo $c_ingBrutos=porcentaje($subtotal,2.5);?></td>
  
</tr>
<tr>
  <td>Lote Hogar</td>
  <td><? echo $c_loteHogar=porcentaje($subtotal,0.5);?></td>

</tr>
<tr>
  <td>Neto Liquidación</td>
  <td><? echo $neto=$subtotal-($c_Retiva+$c_RetGan+$c_ingBrutos+$c_loteHogar) ?></td>
  
</tr>
</table>

</div>
</div>

</div>



 <?
 }
 else echo 'error';
?>