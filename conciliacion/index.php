<?php
include("../sesion.php");
include("../menu.php");
include("conciliacion.php");
include("../tarjeta/tarjeta.php");
$listaTarjeta = tarjeta::lista();
?>
 <div class="container">
  <h2>Conciliaciones</h2>
  <div class="row">
  <form class="form-horizontal" role="form" method="POST" action="listadoConciliacion.php">
    <div class="col-md-8">
   <label >Tarjeta de Credito *</label>
   
    <select name="selectTarjeta" class="form-control" required>
      <option>Seleccionar ....</option>
        <?
        foreach($listaTarjeta as $item)
        {
        //while ( $item = mysql_fetch_array($tarjetas)){
        echo "<option value='".$item[id]."'> ". $item[nombre]."</option>";
        }
        ?>
       </select>
    </div>

  <div class="col-md-8">
    <label>Fecha desde</label>
    <input name="fecha_desde"  class="form-control" type="date" tabindex="1"   required />
  </div>

  <div class="col-md-8">
    <label>Fecha hasta</label>
    <input name="fecha_hasta"  class="form-control" type="date" tabindex="2" required />
  </div>

  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Aceptar</button>
  </div>
</form>    


   
 </div>
 <script src="../js/jquery-1.10.2.js"></script> 
 <script type="text/javascript">
 
 $(document).ready(function()
  {
   
    listadoTarjetas();
    $("#tarjeta_id").change(function()
     {
        var vtabla = $("#tarjeta_id").val();   
        //alert(vtabla);
        listadoCuotas(vtabla);
     })//fin
   

    // $("#generarPrestamo").click(function()
    $("#cuotas").change(function()
     {
        var vtarjeta_id = $("#tarjeta_id").val();   
        //alert(vtarjeta_id);
        var vmontoprestamo = $("#montoprestamo").val();  
        var vcoe = $("#cuotas").val(); 
        var vcantidad = $("#cuotas option:selected").html();
       // alert(vcantidad);    
        $.ajax({
          data: {"tarjeta_id" :vtarjeta_id ,"montoprestamo":vmontoprestamo ,"coeficiente":vcoe},
          type: "POST",
          url: "calculadora/generarPrestamo.php",
        })
       .done(function( data, textStatus, jqXHR ) {
           if ( console && console.log ) {
               console.log( "Generacion es correcta." );
           }
           $.each(data, function(i,datos){
                   document.getElementById("monto_apagar").value=datos.totalCupon;
                   document.getElementById("monto_cuota").value=redondeo((datos.totalCupon/vcantidad),2);
                   
                    //alert(datos.totalCupon);     
                    });
       })
       .fail(function( jqXHR, textStatus, errorThrown ) {
           if ( console && console.log ) {
               console.log( "La solicitud a fallado de Generar: " +  textStatus);
           }
      });
      });//fin buscarDNI

    
 
  }) //fin jquery   

  //17012017 listado de tarjetas para el select recibe un json
  function listadoTarjetas(){
    $.getJSON('calculadora/listaTarjetas.php',function(data){
          $.each(data, function(i,tarjeta){
            $("#tarjeta_id").append('<option value="'+tarjeta.coeficiente+'">'+tarjeta.nombre+'</option>');
          });
    });
   };

   function listadoCuotas(vtabla){
    $.getJSON('calculadora/listaCuotas.php',{ tabla: vtabla},function(data){
          $.each(data, function(i,cuotas){
            $("#cuotas").append('<option value="'+cuotas.coeficiente+'">'+cuotas.cuota+'</option>');
          });
    });
   };

   function redondeo(numero, decimales)
    {
    var flotante = parseFloat(numero);
    var resultado = Math.round(flotante*Math.pow(10,decimales))/Math.pow(10,decimales);
    return resultado;
    };
 
 







</script> 
</body>
</html>