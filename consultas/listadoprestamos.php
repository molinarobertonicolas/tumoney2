<?php
include("../sesion.php");
include("../menu.php");
include("consultas.php");
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Prestamos</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
   <!-- Bootstrap Styles-->
    <!--link href="css/bootstrapFlaty.css" rel="stylesheet" /-->
    <link href="../css/bootstrapYeti.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="../css/font-awesome.css" rel="stylesheet" />

</head>
<body>

	
	<div class="container">
		<h2>Listado de Prestamos</h2>



		<table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
			<thead>
				<tr>
					<th>Nombre</th>
					<th>D.N.I.</th>
          <th>Fecha Prestamo</th>
          <th>Monto</th>
          <th>Cuotas</th>
          <th>Numero Cupon</th>
          <th>Nombre_tarjeta</th>
					
				</tr>
			<thead>
			<tbody>
<?php

//$objecto = new Consultas();

if(isset($_GET['dni']) && !empty($_GET['dni'])) 
{
    
           $objecto = new Consultas();
          $clientes = consultas::listadoPrestamos($_GET['dni']);
          
   
}

//foreach($usuarios as $item)
 foreach($clientes as $item)
{
?>
	<tr>
		<td><?php echo $item['nombre'];?></td>
		<td><?php echo $item['dni'];?></td>
		<td><?php echo $item['fecha_prestamo_tar'];?></td>
		<td><?php echo $item['monto'];?></td>
    <td><?php echo $item['cuotas'];?></td>
     <td><?php echo $item['numero_cupon'];?></td>
     <td><?php echo $item['nombre_tarjeta'];?></td>

	</tr>
<?php } ?>
</tbody>
<tfoot></tfoot>
</table>
		<hr>
		<footer>
      <p>Sistemas <?php echo date('Y');?></p>
    </footer>

    </div>




</body>
</html>
