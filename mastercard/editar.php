<?php
include("../sesion.php");
include '../menu.php';
include("mastercard.php");

if( isset($_GET['id']) && !empty($_GET['id']) )
 {
  $id=(int)$_GET['id'];
  $registros=mastercard::obtenerId($id);
   foreach($registros as $veh)
  {
    $id = $veh['id'];
    $cuota = $veh['cuota'];
    $coeficiente = $veh['coeficiente'];
  ?>


 
 <div class="container">
 <h3>Coeficientes MasterCard</h3>
 <script src="../js/jquery.js"></script>
 <hr>
 <div class="row">
  
 <div class="col-md-8">
 <h4>Editar Coeficientes</h4> 
 <hr>
 <form class="form-horizontal" role="form" method="POST" action="editar.php">
  <input type="hidden" name="idCliente" value="<?echo $id; ?>" />
  
    <div class="col-md-8">
    <label>Cuota</label>
    <input name="cuota"  class="form-control" type="text" tabindex="1" disabled value="<?echo utf8_encode($veh['cuota']); ?>" />
  </div>


  <div class="col-md-8">
    <label >Coeficiente</label>
    <input name="coeficiente"  class="form-control" type="text" tabindex="1"  value="<?echo utf8_encode($veh['coeficiente']); ?>" maxlength="25" required />
  </div>


   <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>   



</div>



<?
}//fin del while
}// fin del if
if( isset($_POST['idCliente']) && !empty($_POST['idCliente']) )
 {
  $id= $_POST['idCliente'];
  $cuota= $_POST['cuota'];
  $coeficiente= $_POST['coeficiente'];

  $registros=Mastercard::editar($id,$cuota,$coeficiente);

  if($registros){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      //header('Location: listado.php');
      exit;
    }
    else {
    ?>
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div>
    <?
    }
    }
?>