<?php
include("../sesion.php");
include("cliente.php");
include '../menu.php';

if( isset($_GET['id']) && !empty($_GET['id']) )
 {
  $id=(int)$_GET['id'];
  $registros=cliente::obtenerId($id);
   foreach($registros as $veh)
  {
    $id = $veh['id'];
  ?>


 
 <div class="container">
 <h3>Clientes</h3>
 <script src="../js/jquery.js"></script>
 <hr>
 <div class="row">
  
 <div class="col-md-8">
 <h4>Editar Usuario</h4> 
 <hr>
 <form class="form-horizontal" role="form" method="POST" action="editar.php">
  <input type="hidden" name="idCliente" value="<?echo $id; ?>" />
  
   <div class="col-md-8">
    <label>Nombre *</label>
    <input name="nombre"  class="form-control" type="text" tabindex="1" maxlength="80" value="<?echo utf8_encode($veh['nombre']); ?>"  required autofocus/>
  </div>

  <div class="col-md-8">
    <label>D.N.I *</label>
    <input name="dni"  class="form-control" type="text" tabindex="2" maxlength="8" value="<?echo utf8_encode($veh['dni']); ?>" required />
  </div>

  <div class="col-md-8">
    <label>Domicilio *</label>
    <input name="domicilio"  class="form-control" type="text" tabindex="3" maxlength="90" value="<?echo utf8_encode($veh['domicilio']); ?>"  required />
  </div>

  <div class="col-md-8">
    <label >Departamento *</label>
    <input name="departamento"  class="form-control" type="text" tabindex="4" maxlength="50" value="<?echo utf8_encode($veh['departamento']); ?>"  required />
  </div>

  <div class="col-md-8">
    <label >Telefono *</label>
    <input type="text" class="form-control" name="telefono" maxlength="15" value="<?echo utf8_encode($veh['telefono']); ?>"  >
 </div>

 <div class="col-md-8">
    <label >Fecha de Ingreso</label>
    <input type="date" class="form-control" name="fecha_ingreso" maxlength="15" value="<?echo utf8_encode($veh['fecha_ingreso']); ?>"  >
 </div>
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>   



</div>



<?
}//fin del while
}// fin del if
if( isset($_POST['idCliente']) && !empty($_POST['idCliente']) )
 {
  $id= $_POST['idCliente'];
   $nombre = $_POST['nombre'];
  $dni= $_POST['dni'];
  $domicilio = $_POST['domicilio'];
  $departamento= $_POST['departamento'];
  $telefono= $_POST['telefono'];
  $fecha_ingreso= $_POST['fecha_ingreso'];

  $registros=cliente::editar($id,$dni,$nombre,$domicilio,$departamento,$telefono,$fecha_ingreso);

  if($registros){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>";
      //header('Location: listado.php');
      exit;
    }
    else {
    ?>
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div>
    <?
    }
    }
?>