/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 8.0.17 : Database - tumoney2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tumoney2` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `tumoney2`;

/*Table structure for table `prestamo_tarjeta` */

DROP TABLE IF EXISTS `prestamo_tarjeta`;

CREATE TABLE `prestamo_tarjeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cupon_id` int(10) DEFAULT NULL,
  `fechahora` datetime DEFAULT NULL,
  `pdf_dni` varchar(70) DEFAULT NULL,
  `montoprestamo` decimal(10,2) DEFAULT NULL COMMENT 'monto prestado',
  `monto_apagar` decimal(10,2) DEFAULT NULL COMMENT 'monto a devolver por el cliente',
  `cft` int(2) NOT NULL,
  `tna` int(2) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `sucursal_id` int(4) DEFAULT '2',
  `rendido` varchar(15) DEFAULT 'NO',
  `fecha_otorgado` date DEFAULT NULL,
  `fecha_cancelado` date DEFAULT NULL,
  `documentacion` varchar(15) DEFAULT 'NO' COMMENT 'no presentada o si presentada',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
