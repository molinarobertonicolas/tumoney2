<?
include("../sesion.php");
include("cliente.php");
include '../menu.php';
$objecto = new Cliente();
if( isset($_POST['nombre']) && !empty($_POST['nombre']) )
 {
//   $nInscripcion,$nombre,$dni,$curso,$horario,$sucursal,$email,$observacion,$otros,$idCurso
  $nombre = $_POST['nombre'];
  $dni= $_POST['dni'];
  $domicilio = $_POST['domicilio'];
  $departamento= $_POST['departamento'];
  $telefono= $_POST['telefono'];
  $fecha_ingreso= date("Y-m-d H:i:s");

  $todobien = $objecto->nuevo($dni,$nombre,$domicilio,$departamento,$telefono,$fecha_ingreso);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }     
}
else
{
?>
 <div class="container">
 <h3>Clientes</h3>
 <hr>
 <div class="row">


 <div class="col-md-6">
     
 <h4>Agregar Cliente</h4>
 <hr>
  <form method="POST" role="form" action="nuevo.php">

  <div class="col-md-8">
    <label>D.N.I *</label>
    <input name="dni" id="dni"  class="form-control" type="text" tabindex="1" maxlength="8" required autofocus />
     <div id="Info"></div>
  </div>
  
  <div class="col-md-8">
    <label>Nombre *</label>
    <input name="nombre"  class="form-control" type="text" tabindex="2" maxlength="80" required />
  </div>

  

  <div class="col-md-8">
    <label>Domicilio *</label>
    <input name="domicilio"  class="form-control" type="text" tabindex="3" maxlength="90" required />
  </div>
  
   <div class="col-md-8">
    <label >Departamento *</label>
    
        <select class="form-control" name="departamento">
          <option value="ALBARDON">ALBARDON</option>
          <option value="ANGACO">ANGACO</option>
          <option value="CALINGASTA">CALINGASTA</option>
          <option value="CAPITAL">CAPITAL</option>
         <option value="CAUCETE">CAUCETE</option>
         <option value="CHIMBAS">CHIMBAS</option>
         <option value="IGLESIA">IGLESIA</option>
         <option value="JACHAL">JACHAL</option>
         <option value="9 DE JULIO">9 DE JULIO</option>
          <option value="POCITO">POCITO</option>
           <option value="RAWSON">RAWSON</option>
            <option value="RIVADAVIA">RIVADAVIA</option>
             <option value="SAN MARTIN">SAN MARTIN</option>
              <option value="SANTA LUCIA">SANTA LUCIA</option>
               <option value="SARMIENTO">SARMIENTO</option>
                <option value="ULLUM">ULLUM</option>
                 <option value="VALLE FERTIL">VALLE FERTIL</option>
                 <option value="25 DE MAYO">25 DE MAYO</option>
                 <option value="ZONDA">ZONDA</option>
        </select>
 
     
  </div>

  <div class="col-md-8">
    <label >Telefono *</label>
    <input type="text" class="form-control" name="telefono" maxlength="15" >
 </div>
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button id='botonGuardar' type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>     
 <?
 }
 ?> 
 <script src="../js/util.js"></script> 
 <script src="../js/jquery-1.10.2.js"></script> 
 <script src="../js/jquery.maskedinput.js" type="text/javascript"></script>          
 <script type="text/javascript">

 $(document).ready(function()
  {
    $("#dni").mask("99999999");
    //validar que el cuit no este repetido
     $("#dni").blur(function(){
        //$('#Info').html('<img src="loader.gif" alt="" />').fadeOut(1000);
        var dni = $(this).val();        
        var dataString = 'dni='+dni;
        $.ajax({
            type: "POST",
            url: "validarDNI.php",
            data: dataString,
            success: function(data) {
                if (data > 0) {
                   //alert("DNI Repetido.");
                   $('#Info').fadeIn(50000).html('<div class="alert alert-danger">DNI ya existente. </div>');
                     $("#botonGuardar").css("display", "none");
                   //window.location="index.php";
                   }
            }
        });
    });           
 });
</script>
  