<?php
include("../sesion.php");
include("../menu.php");
include("facturab.php");
?>
 <div class="container">
    <h2>Listado de Facturas B</h2>

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº Prestamo</th>
             <th>Nombre</th>
             <th>CUIT</th>
             <th>Domicilio</th>
             <th>N° Cupon</th>
             <th>Digitos</th>
             <th>Préstamo</th>
             <th>Interés</th>
             <th>Gastos Administrativos</th>
             <th>Interéses + Gastos Adm.</th>
             <th>TOTAL</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objecto = new facturab();
          $datos = facturab::lista();
          foreach($datos as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['prestamo_id']; ?></td>
              
              <td><?php echo $item ['nombre']; ?></td>
               <td><?php echo $item ['cuit']; ?></td>
              <td><?php echo $item ['domicilio'].' '.$item ['departamento']; ?></td>
              <td><?php echo $item ['numero_cupon']; ?></td>
              <td><?php echo $item ['digitos']; ?></td>
              <td><?php echo $item ['monto_prestamo_facturado']; ?></td>
              <td><?php echo $item ['interes']; ?></td>

              <td><?php echo $item ['gastos_administrativos'] ; ?></td>
              <td><?php echo $nuevo=$item ['gastos_administrativos'] + $item ['interes']; ?></td>
              <td><?php echo $item ['total']; ?></td>
             
              <!--td>
                  <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>
                  <a class="btn btn-danger btn-sm" href="eliminar.php?id=<?php echo $item ['id'];?>" > Borrar</a>
              </td-->
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>