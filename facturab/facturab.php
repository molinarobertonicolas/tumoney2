<?
include_once("../bd/conexion.php");
class facturab
{
    
  public function lista()
  {
    $data=[];
   $consulta="SELECT
prestamo_tarjeta.`id` AS prestamo_id,
cliente.`nombre` AS nombre,
cliente.`cuit` AS cuit,
cliente.`domicilio` AS domicilio,
cliente.`departamento` AS departamento,
cupondetarjeta.`numero_cupon` AS numero_cupon,
cupondetarjeta.`digitos` AS digitos,
facturab.`monto_prestamo_facturado` AS monto_prestamo_facturado,
facturab.`interes_mas_iva` AS interes,
facturab.`gastos_adm_con_iva` AS gastos_administrativos,
facturab.`total_factura_b` AS total
FROM
    `prestamo_tarjeta`
    INNER JOIN `facturab` 
        ON (`prestamo_tarjeta`.`id` = `facturab`.`prestamo_id`)
    INNER JOIN `cupondetarjeta` 
        ON (`cupondetarjeta`.`id` = `prestamo_tarjeta`.`cupon_id`)
    left JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`) order by prestamo_id DESC;";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

   public function nuevo($monto_solicitado,$plus,$monto_prestamo_facturado,$interes,$iva_interes,
        $interes_mas_iva,
        $gastos_administrativos,
        $interes_tarjeta,
        $gastos_adm_mas_interes,
        $iva_gastos_adm,
        $gastos_adm_con_iva,
        $total_factura_b)
   	{
  	  $sql="INSERT INTO `facturab`
            (`monto_solicitado`,
             `plus`,
             `monto_prestamo_facturado`,
             `interes`,
             `iva_interes`,
             `interes_mas_iva`,
             `gastos_administrativos`,
             `interes_tarjeta`,
             `gastos_adm_mas_interes`,
             `iva_gastos_adm`,
             `gastos_adm_con_iva`,
             `total_factura_b`)
VALUES ('$monto_solicitado',
        '$plus',
        '$monto_prestamo_facturado',
        '$interes',
        '$iva_interes',
        '$interes_mas_iva',
        '$gastos_administrativos',
        '$interes_tarjeta',
        '$gastos_adm_mas_interes',
        '$iva_gastos_adm',
        '$gastos_adm_con_iva',
        '$total_factura_b');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
    	}

    public function obtenerId($id)
  	{
  	 $sql="SELECT * FROM cliente where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }

    public function eliminar($id)
	  {
     $sql="DELETE FROM cliente WHERE id ='$id'";
     $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
     return $rs;
    }

 public function traer_facturab($id)
    {
     $sql="SELECT * FROM facturab where prestamo_id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      } 
    
   }
?>