<?php
include("../sesion.php");
include("../menu.php");
include("caja.php");
?>
 <div class="container">
    <h2>Listado de Caja Diaria</h2>

    <p> <a class="btn btn-primary" href="nuevo.php">Adicionar Nuevo</a> </p>

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Id</th>
             <th>Monto</th>
             <th>Fecha</th>
             <th>Operacion</th>
             <th>Usuario</th>
             <th>Observacion</th>
             <th>Funciones</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $objecto = new Caja();
          $usuarios = caja::lista();
          foreach($usuarios as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['monto']; ?></td>
              <td><?php echo $item ['fechahora']; ?></td>
              <td><?php echo $item ['operacion']; ?></td>
              <td><?php echo $item ['usuario']; ?></td>
              <td><?php echo $item ['observacion']; ?></td>
              <td>
                  <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>
                  <a class="btn btn-danger btn-sm" href="eliminar.php?id=<?php echo $item ['id'];?>" > Borrar</a>
              </td>
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
      $('#agregar').click(function(){
        $.ajax({
            url: 'nuevo.php',
            success: function(data) {
                $('#div_dinamico').html(data);
            }
        });
    });

    //editar
    $("a[id^='editar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data){

            if (data)
            {
             //$('#div_dinamico').hide();
             $('#div_dinamico').html(data);
            }
        }
        })//fin ajax
        });//fin

    //eliminar
     $("a[id^='borrar']").click(function(evento)
       {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'eliminar.php',
          data: { id: vid},
          success: function(data){
            if (data)
            {
              alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
        });//fin


 });
</script>
</body>
</html>