<?php
include("../sesion.php");
include("../menu.php");
include("estadisticas.php");
?>
 <div class="container">

        <div class="row">
        <div class="col-md-12">
           <h3>Mostrar Totales por fechas de la Encuesta.</h3>
        <div class="panel panel-primary">
         <div class="panel-body">
           
            <form class="form-inline" role="form">
              <div class="form-group">
                <label >Fecha Inicio del Reporte</label>
                <input type="date" class="form-control" id="fecha_desde"
                       placeholder="Fecha Desde" tabindex="1" autofocus>

              </div>

                <div class="form-group">
                <label >Fecha Fin del Reporte</label>
                <input type="date" class="form-control" id="fecha_hasta"
                       placeholder="Fecha Desde" tabindex="2">
                       
              </div>
              <button type="button"  id="buscar_fecha" class="btn btn-primary pull-right" tabindex="3"> Buscar </button>
            </form>
        </div>
      </div>
    </div>


   <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-body">
      <div id="div_dinamico">
        
      </div>
    </div>
  </div>
 </div>
        
 </div>
 








  <h2>Encuesta Como llegue?</h2>
  <?php
      $objecto = new Estadisticas();
      $listado=$objecto->comollego();
     
      $folleto=0;
       $radio=0;
       $facebook=0;
       $recomendacion=0;
       $pasaba=0;
      foreach($listado as $item)
      {

       $tipo=$item['comollego'];


       switch ( $tipo ) 
        {
        case "Pasaba":
              $pasaba++;
              break;
        case "Folleto":
              $folleto++;
              break;
        case "Radio":
              $radio++;
              break;
        case "Facebook":
              $facebook++;
              break;
        case "Recomendacion":
              $recomendacion++; 
              break;                       
        }

       }
  ?>  

  <ul>
    <li><p> Pasaba : <?php echo $pasaba; ?>     </p> </li> 
    <li><p> Folleto : <?php echo $folleto; ?>     </p> </li>  
    <li><p> Radio : <?php echo $radio; ?>     </p> </li>  
    <li><p> Facebook : <?php echo $facebook; ?>     </p> </li>  
    <li><p> Recomendacion : <?php echo $recomendacion; ?>     </p> </li>     
  </ul>

 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
     // llamada ajax
    $('#buscar_fecha').click(function(){
        var f_desde=$('#fecha_desde').val();
        var f_hasta=$('#fecha_hasta').val();
        $.post("informe_comollego_entre_fechas.php", {fecha_desde: f_desde, fecha_hasta: f_hasta}, function(mensaje) {
                  $("#div_dinamico").html(mensaje);
              }); 
    });
 });
</script>
</body>
</html>