<?
include("../sesion.php");
include("gastos.php");
include '../menu.php';

if( isset($_GET['id']) && !empty($_GET['id']) )
{
  $id=(int)$_GET['id'];
  $registros=gastos::obtenerId($id);
   foreach($registros as $veh)
  {
    $id = $veh['id'];
  ?>
   <div class="container">
 <div class="row">
 <div class="col-md-8">
     
 <h4>Editar comprobante de Gasto</h4>
 <hr>
  <form action="editar.php" method="post" enctype="multipart/form-data">

    <input name="MAX_FILE_SIZE" value="20200000" type="hidden">
    <input name="action" value="upload" type="hidden">
    <input type="hidden" name="idgasto" value="<?echo $id; ?>" />

  <div class="col-md-8">
    <label>Detalle</label>
    <input name="detalle" class="form-control" type="text" tabindex="1" required value="<?echo utf8_encode($veh['detalle']); ?>" />
  </div>
  
  <div class="col-md-8">
    <label>Monto</label>
    <input name="monto" class="form-control" type="number" step="any" tabindex="2" maxlength="12" required  value="<?echo utf8_encode($veh['monto']); ?>" />
  </div>

  <div class="col-md-8">
    <label>Fecha</label>
    <input name="fecha" class="form-control" type="date" tabindex="3" required value="<?echo utf8_encode($veh['fecha']); ?>" />
  </div>

  <div class="col-md-8">
    <label> Comprobante</label>

     <img src="comprobantes/<?php echo $veh ['comprobante']; ?>" width="250" height="250" class="img-thumbnail" >

    <input name="foto" value="<?echo $veh['comprobante']; ?>" type="hidden"/>
     <label>Nuevo Comprobante</label>
    <input name="foto2" class="form-control" type="file" />
  
  </div>
 
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>
</div>

<?
}//fin del while
}// fin del if
if( isset($_POST['idgasto']) && !empty($_POST['idgasto']) )
 {     

  $objecto = new Gastos();
  $idgasto = $_POST['idgasto'];
  $detalle = $_POST['detalle'];
  $monto= $_POST['monto'];
  $fecha = $_POST['fecha'];
  //$fecha= date("Y-m-d");
  //26-09-2018 subir archivo
      $status = "";
      if ($_POST["action"] == "upload")
      {
         // obtenemos los datos del archivo
          $tamano = $_FILES["foto2"]['size'];
    $tipo = $_FILES["foto2"]['type'];
    $archivo = $_FILES["foto2"]['name'];

         if ($archivo != "")
         {
           // guardamos el archivo a la carpeta files
           $destino="comprobantes/".$archivo;

           if (copy($_FILES['foto2']['tmp_name'],$destino))
           {
              // renombrar archivo subido
                $long = 6; // la longitud del rand 
                $nuevo_rand = ''; 
                for($i=0;$i<=$long;$i++){ 
                $nuevo_rand .= rand(0,9); 
                }
                //subcadena
                $subcadena = substr ($archivo,-5);

              // nombre modificado del archivo
              $nuevo_name='comp_'.$nuevo_rand.$subcadena; 
              rename("comprobantes/".$archivo,"comprobantes/".$nuevo_name);
              //fin del renombrar
              $status = "Archivo subido: <b>".$nuevo_name."</b>";
             }
             else {
                     $status  = "Error al Copiar el Archivo";
                     echo '<script> console.log("Error al Copiar el Archivo") </script>';
                       exit;
                      }
        }//fin del if del subir archivo
         else {  
                 $nuevo_name=$_POST['foto'];//$status = "Achivo esta vacio";}
               }

      }//fin del subir archivo

  $todobien = $objecto->editar($nuevo_name,$idgasto);
 if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }  
    
}
?>