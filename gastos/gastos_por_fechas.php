<?php
include("../sesion.php");
include("gastos.php");
if( isset($_POST['fecha_desde']) && !empty($_POST['fecha_hasta']) )
{

$fecha_desde=$_POST['fecha_desde'];
$fecha_hasta=$_POST['fecha_hasta'];

$total=0;
?>
 
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Fecha</th>
             <th>Detalle</th>
             <th>Monto</th>
             <th>Comprobante</th>
             </tr>
           </thead>
           <tbody>
          <?php
          $listados = gastos::gastos_fechas($fecha_desde,$fecha_hasta);
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item['fecha']; ?></td>
              <td><?php echo $item['detalle']; ?></td>
              <td><?php  echo $item['monto'];
                       $total=$total+$item['monto']; ?></td>
              <td> <a href="comprobantes/<?php echo $item ['comprobante']; ?>" target="_blank"><?php echo $item ['comprobante']; ?></a></td>
          </tr>
          <?php
           }
          ?>
              <td></td>
              <td>Total :  </td>
              <td><strong><?php echo '$ '.$total; ?> </strong></td>
              <td></td>
              <td></td>
          </tbody>
         </table>
 <?
} else echo 'error';
 ?>