<?
include_once("../bd/conexion.php");
class Gastos
{
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT * FROM gastos order by id desc";
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function nuevo($detalle,$monto,$fecha,$comprobante)
  {
  	  $sql="INSERT INTO `gastos`
            (`detalle`,`monto`,`fecha`,`comprobante`)
           VALUES ('$detalle','$monto','$fecha','$comprobante');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
  }

  public function guardarGastoCaja($cajanueva_id,$operacion,$tipo,$monto,$fechahora,$detalle,$usuario_id)
    {
      $sql="INSERT INTO `cajadetalle`
            (`cajanueva_id`,
             `operacion`,
             `tipo`,
             `monto`,
             `fechahora`,
             `detalle`,
             `usuario_id`)
            VALUES ('$cajanueva_id',
                    '$operacion',
                    '$tipo',
                    '$monto',
                    '$fechahora',
                    '$detalle',
                    '$usuario_id');";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    return $rs;
    }

    public function caja_habilitada()
    {
      $sql="SELECT `id` FROM `cajanueva` WHERE estado='Abierta';";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      
      return $data;
    }

    public function gastos_fechas($fecha_desde,$fecha_hasta)
    {
    $data=array();
    
    $consulta="SELECT * 
               FROM `gastos`
               WHERE `fecha` BETWEEN CAST('$fecha_desde' AS DATE) AND CAST('$fecha_hasta' AS DATE) ORDER BY fecha DESC";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  public function obtenerId($id)
    {
     $sql="SELECT * FROM gastos where id='$id'";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      if(mysqli_num_rows($rs) >0)
      {
        while($fila = mysqli_fetch_assoc($rs))
        {
          $data[] = $fila;
        }
      }
      return $data;
      }
  
  public function editar($comprobante,$id)
    {
     $sql="UPDATE `gastos`
          SET 
            `comprobante` = '$comprobante'
          WHERE `id` = '$id';";
      $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
      return $rs;
  }


	
}
?>