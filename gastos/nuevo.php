<?
include("../sesion.php");
include("gastos.php");
include '../menu.php';
$objecto = new Gastos();
if( isset($_POST['detalle']) && !empty($_POST['detalle']) )
 {
//   $nInscripcion,$nombre,$dni,$curso,$horario,$sucursal,$email,$observacion,$otros,$idCurso
  $detalle = $_POST['detalle'];
  $monto= $_POST['monto'];
  $fecha = $_POST['fecha'];
  //$fecha= date("Y-m-d");
  //26-09-2018 subir archivo
      $status = "";
      if ($_POST["action"] == "upload")
      {
         // obtenemos los datos del archivo
         $tamano = $_FILES["archivo"]['size'];
         $tipo = $_FILES["archivo"]['type'];
         $archivo = $_FILES["archivo"]['name'];

         if ($archivo != "")
         {
           // guardamos el archivo a la carpeta files
           $destino="comprobantes/".$archivo;

           if (copy($_FILES['archivo']['tmp_name'],$destino))
           {
              // renombrar archivo subido
                $long = 6; // la longitud del rand 
                $nuevo_rand = ''; 
                for($i=0;$i<=$long;$i++){ 
                $nuevo_rand .= rand(0,9); 
                }
                //subcadena
                $subcadena = substr ($archivo,-5);

              // nombre modificado del archivo
              $nuevo_name='comp_'.$nuevo_rand.$subcadena; 
              rename("comprobantes/".$archivo,"comprobantes/".$nuevo_name);
              //fin del renombrar
              $status = "Archivo subido: <b>".$nuevo_name."</b>";
             }
             else {
                     $status  = "Error al Copiar el Archivo";
                     echo '<script> console.log("Error al Copiar el Archivo") </script>';
                       exit;
                      }
        }//fin del if del subir archivo
        else
        {
           $status  = "Achivo esta vacio";
            echo '<script> console.log("Archivo esta vacio.") </script>';
            exit;
        }

      }//fin del subir archivo

  $todobien = $objecto->nuevo($detalle,$monto,$fecha,$nuevo_name);
  if(!$todobien){
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }   
    // insertar en caja detalle
    // buscar caja habilitada
    $cajanueva=$objecto->caja_habilitada();
     foreach($cajanueva as $item)
      {$cajanueva_id=$item['id'];}

     $todobien = $objecto->guardarGastoCaja($cajanueva_id,"Gasto","Egreso",$monto,$fecha,$detalle,$usuario);
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
    ?>      
         <div class="alert alert-block alert-error fade in" style="max-width: 220px; margin: 0px auto 20px;">
         <button data-dismiss="alert" class="close" type="button">×</button>
         Lo sentimos, no se pudo guardar ...
         </div> 
    <?
    }  
}
else
{
?>
 <div class="container">
 <div class="row">
 <div class="col-md-8">
     
 <h4>Agregar Gasto</h4>
 <hr>
  <form action="nuevo.php" method="post" enctype="multipart/form-data">

    <input name="MAX_FILE_SIZE" value="20200000" type="hidden">
    <input name="action" value="upload" type="hidden">

  <div class="col-md-8">
    <label>Detalle</label>
    <input name="detalle" class="form-control" type="text" tabindex="1" required autofocus />
  </div>
  
  <div class="col-md-8">
    <label>Monto</label>
    <input name="monto" class="form-control" type="number" step="any" tabindex="2" maxlength="12" required />
  </div>

  <div class="col-md-8">
    <label>Fecha</label>
    <input name="fecha" class="form-control" type="date" tabindex="3" required />
  </div>

  <div class="col-md-8">
    <label> Comprobante</label>
    <input name="archivo" class="form-control" type="file" tabindex="4" maxlength="150"  required />
  </div>
 
  
  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>     
 <?
 }
 ?>