<?php
include("../sesion.php");
include("../menu.php");
include("gastos.php");
?>
 <div class="container">
    <h3>Gastos </h3>
     <p>
        <a class="btn btn-primary" href="nuevo.php">Adicionar Nuevo</a>
        <a class="btn btn-primary" href="reporte_gastos.php">Reporte Gastos</a>
     </p>
    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Detalle</th>
             <th>Monto </th>
             <th>Fecha</th>
             <th>Comprobante</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $clientes = gastos::lista();
          foreach($clientes as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['detalle']; ?></td>
              <td><?php echo $item ['monto']; ?></td>
              <td><?php echo $item ['fecha']; ?></td>
              <td>
                <a href="comprobantes/<?php echo $item ['comprobante']; ?>" target="_blank">
                <img src="comprobantes/<?php echo $item ['comprobante']; ?>" alt="" height="100" width="100">
                  <?php echo $item ['comprobante']; ?></a>
              
                  <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>
                  <!--a class="btn btn-danger btn-sm" href="eliminar.php?id=<?php echo $item ['id'];?>" > Borrar</a>
              </td-->
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>