<?php
include_once("../bd/conexion.php");
class Documentacion
{
  
  public function lista()
  {
    $data=[];
    $consulta="SELECT
    prestamo_tarjeta.`estado` AS estado, 
    prestamo_tarjeta.`id` AS id, 
    prestamo_tarjeta.`fechahora` AS fecha,
    cliente.`nombre` AS cliente,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`id` AS id_cupon,
    cupondetarjeta.`numero_cupon` AS numero_cupon,
    cupondetarjeta.`cuotas` AS cuotas,
    cupondetarjeta.`cuotas` AS cuotas,
    prestamo_tarjeta.`montoprestamo` AS monto_prestado,
    prestamo_tarjeta.`monto_apagar` AS apagar,
    prestamo_tarjeta.`pdf_dni` AS documento, 
     prestamo_tarjeta.`documentacion` AS documentacion
    FROM
    `prestamo_tarjeta`
    INNER JOIN `cupondetarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
      INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
        left JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`)
         order by prestamo_tarjeta.`id` desc";

    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
   
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  //28-07-2021 modificar el estado de la documentacion
  public function editarDocumentacion($id,$documentacion,$fecha_pago_estimada)
  {
    $sql="UPDATE `prestamo_tarjeta`
          SET 
            `documentacion` = '$documentacion',
            `fecha_pago_estimada` = '$fecha_pago_estimada'
          WHERE `id` = '$id'";
    $rs = mysqli_query(conexion::obtenerInstancia(), $sql);
    if(!$rs)
    {
      return 'error actualizar'.$id;
    }
    return 'Se actualizo correctamente.';
  }

  public function obtenerId($id)
  {
    $data=[];
    $consulta="SELECT
    prestamo_tarjeta.`estado` AS estado, 
    prestamo_tarjeta.`id` AS id, 
    prestamo_tarjeta.`fechahora` AS fecha,
    cliente.`nombre` AS cliente,
    tarjeta.`nombre` AS tarjeta,
    cupondetarjeta.`id` AS id_cupon,
    cupondetarjeta.`numero_cupon` AS numero_cupon,
    cupondetarjeta.`cuotas` AS cuotas,
    cupondetarjeta.`cuotas` AS cuotas,
    prestamo_tarjeta.`montoprestamo` AS monto_prestado,
    prestamo_tarjeta.`monto_apagar` AS apagar,
    prestamo_tarjeta.`pdf_dni` AS documento, 
     prestamo_tarjeta.`documentacion` AS documentacion
    FROM
    `prestamo_tarjeta`
    INNER JOIN `cupondetarjeta` 
        ON (`prestamo_tarjeta`.`cupon_id` = `cupondetarjeta`.`id`)
      INNER JOIN `tarjeta` 
        ON (`cupondetarjeta`.`tarjeta_id` = `tarjeta`.`id`)
        left JOIN `cliente` 
        ON (`cupondetarjeta`.`cliente_id` = `cliente`.`id`)
         where prestamo_tarjeta.`id`=$id";
          
    $rs = mysqli_query(conexion::obtenerInstancia(), $consulta);
    if(mysqli_num_rows($rs) >0)
    {
      while($fila = mysqli_fetch_assoc($rs))
      {
        $data[] = $fila;
      }
    }
    return $data;
  }

  
}
?>