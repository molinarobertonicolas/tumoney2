<?php
include("../sesion.php");
include("../menu.php");
include("documentacion.php");
if( isset($_GET['id']) && !empty($_GET['id']) )
{
  $id=(int)$_GET['id'];
  $doc = new documentacion();
  $registros=$doc->obtenerId($id);
  foreach($registros as $veh)
  {
    $id = $veh['id'];
  ?>
 <div class="container">
  <h2>Documentación - Prestamos </h2>
 <div class="row">
 <div class="col-md-8">
     
 <h4>Editar </h4>
 <hr>
 <form action="editar.php" method="post" >
  <input type="hidden" name="id" value="<?echo $id; ?>" />
  <div class="col-md-8">
    <label>N°: <?echo $veh['id']; ?></label>
    <label>Cliente: <?echo $veh['cliente']; ?></label>
    <label>Fecha: <?echo $veh['fecha']; ?></label>
  </div>

  <div class="col-md-8">
    <label>Documentación</label>
     <select name="documentacion" class="form-control" name="estado">
       <option >Seleccione.......</option> 
      <option value="Aprobada">Aprobada</option> 
  </select>
  </div>

  <div class="col-md-8">
    <label>Fecha Pago Estimada</label>
    <input name="fecha_pago_estimada" class="form-control" type="date"  />
  </div>

  <div class="col-md-8">
  <hr>
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="location.href='index.php';"><i class="fa fa-times"></i> Cancelar</button>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
  </div>
</form>
</div>

<?php

}//fin del while
}// fin del if

if( isset($_POST['id']) && !empty($_POST['id']) )
 {     
  $objeto = new documentacion();
  $id = $_POST['id'];
  $documentacion = $_POST['documentacion'];
  $fecha_pago_estimada = $_POST['fecha_pago_estimada'];
  $todobien = $objeto->editarDocumentacion($id,$documentacion,$fecha_pago_estimada);
  
  if($todobien){
      echo "<script language=Javascript> location.href=\"index.php\"; </script>"; 
      //header('Location: listado.php');
      exit;
    } 
    else {
          echo 'Error: Editar Documentacion. ';
    } 
    
}
?>
 