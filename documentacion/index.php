<?php
include("../sesion.php");
include("../menu.php");
include("documentacion.php");
?>
 <div class="container">
    <h2>Documentación - Prestamos </h2>

    <table id="listado" class="table table-striped table-bordered table-hover table-condensed" >
          <thead>
             <tr>
             <th>Nº</th>
             <th>Cliente </th>
             <th>Tarjeta de Credito </th>
             <th>Monto Prestado</th>
             <th>N° Cupon</th>
             <th>Cuotas</th>
             <th>Fecha</th>
             <th>DNI/PDF</th>
              <th>Documentación</th>
              <th>Contrato</th>
             </tr>
           <thead>
           <tbody>
          <?php
          $doc = new documentacion();
          $listados = $doc->lista();
          foreach($listados as $item)
          {
          ?>
           <tr>
              <td><?php echo $item ['id']; ?></td>
              <td><?php echo $item ['cliente']; ?></td>
              <td><?php echo $item ['tarjeta']; ?></td>
              <td><?php echo $item ['monto_prestado']; ?></td>
              <td><?php echo $item ['numero_cupon']; ?></td>
              <td><?php echo $item ['cuotas']; ?></td>
              <td><?php echo $item ['fecha']; ?></td>
              <td>

              <? echo '<a href="../dni/'.$item ['documento'].'" target="_blank" >'.$item ['documento'].'</a>'; ?>
              </td>
              <td>
              <?php 

              if ( $item ['documentacion'] == 'NO')
              {
              ?>
               
                <a class="btn btn-primary btn-sm" href="editar.php?id=<?php echo $item ['id'];?>">Editar</a>

              <?php
               }
               else {
              ?>
              Aprobada
              <?php
            }
            ?>
              </td>

              <td>
               <a href="imprimir.php?cupon_id=<?php echo $item ['id_cupon'];?>" target="_blank" > Contrato</a>
              </td>
             
          </tr>
          <?php
           }
          ?>
          </tbody>
         </table>
         </div>
         </div>
  </div>
 </div>
 </div>  

  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>

  <script type="text/javascript">
 $(document).ready(function()
  {
    //editar
    $("a[id^='editar']").click(function(evento)
    {
        evento.preventDefault();
        vid = this.id.substr(6,4);
        $.ajax({
          type: "POST",
          cache: false,
          async: false,
          url: 'editar.php',
          data: { id: vid},
          success: function(data)
          {
            if (data)
            {
               alert(data);
               location.reload(true);
            }
        }
        })//fin ajax
    });//fin
 });
</script>
</body>
</html>